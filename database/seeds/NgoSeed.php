<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Ngo;
use App\SelfHelpGroup;
use App\District;
class NgoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      factory(App\Ngo::class, 500)->create()->each(function ($ngo) {
            // Seed the relation with one address
            $ngo_sector = factory(App\NgoActiveSector::class,2)->make();
            $ngo->ngo_sector()->saveMany($ngo_sector);
            $self_help_group = factory(App\SelfHelpGroup::class,2)->make();
            $ngo->ngo_self_help_group()->saveMany($self_help_group);
        });
        $ngos = Ngo::all();
        foreach($ngos as $ngo)
        {
          $ngo = Ngo::find($ngo->id);
          $districts = District::where('state_id',$ngo->state_id)->pluck('id');
          $first_element = $districts->first();
          $last_element = $districts->last();
          //return $last_element;
          $ngo->district_id = rand($first_element,$last_element);
          $ngo->update();
        }
        $groups = SelfHelpGroup::all();
        foreach($groups as $group)
        {
          $group = SelfHelpGroup::find($group->id);
          $districts = District::where('state_id',$group->state_id)->pluck('id');
          $first_element = $districts->first();
          $last_element = $districts->last();
          $group->district_id = rand($first_element,$last_element);
          $group->update();
        }
    }
}
