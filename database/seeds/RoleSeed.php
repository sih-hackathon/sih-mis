<?php

use Illuminate\Database\Seeder;
use App\Role;
class RoleSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table((new Role)->getTable())->truncate();

        Role::insert([
            [
                'id'    => 1,
                'title' => 'Super Administrator',
            ],
            [
                'id'    => 2,
                'title' => 'RMK Administrator',
            ],
            [
                'id'    => 3,
                'title' => 'NGO Administrator',
            ],
            [
                'id'    => 4,
                'title' => 'SHG Administrator',
            ],
            [
                'id'    => 5,
                'title' => 'Entrepreneur',
            ],
        ]);
    }
}
