<?php

use Illuminate\Database\Seeder;
use App\District;
class DistrictSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table((new District)->getTable())->truncate();

      District::insert([
          [
              'id'    => 1,
              'state_id' => 1,
              'title' => 'Anantapur',
          ],
          [
              'id'    => 2,
              'state_id' => 1,
              'title' => 'Chittoor',
          ],
          [
              'id'    => 3,
              'state_id' => 1,
              'title' => 'East Godavari',
          ],
          [
              'id'    => 4,
              'state_id' => 1,
              'title' => 'Guntur',
          ],
          [
              'id'    => 5,
              'state_id' => 1,
              'title' => 'Kadapa',
          ],
          [
              'id'    => 6,
              'state_id' => 1,
              'title' => 'Krishna',
          ],
          [
              'id'    => 7,
              'state_id' => 1,
              'title' => 'Kurnool',
          ],
          [
              'id'    => 8,
              'state_id' => 1,
              'title' => 'Nellore',
          ],
          [
              'id'    => 9,
              'state_id' => 1,
              'title' => 'Prakasam',
          ],
          [
              'id'    => 10,
              'state_id' => 1,
              'title' => 'Srikakulam',
          ],
          [
              'id'    => 11,
              'state_id' => 1,
              'title' => 'Visakhapatnam',
          ],
          [
              'id'    => 12,
              'state_id' => 1,
              'title' => 'Vizianagaram',
          ],
          [
              'id'    => 13,
              'state_id' => 1,
              'title' => 'West Godavari',
          ],
          [
              'id'    => 14,
              'state_id' => 2,
              'title' => 'Anjaw',
          ],
          [
              'id'    => 15,
              'state_id' => 2,
              'title' => 'Central Siang',
          ],
          [
              'id'    => 16,
              'state_id' => 2,
              'title' => 'Changlang',
          ],
          [
              'id'    => 17,
              'state_id' => 2,
              'title' => 'Dibang Valley',
          ],
          [
              'id'    => 18,
              'state_id' => 2,
              'title' => 'East Kameng',
          ],
          [
              'id'    => 19,
              'state_id' => 2,
              'title' => 'East Siang',
          ],
          [
              'id'    => 20,
              'state_id' => 2,
              'title' => 'Kamle',
          ],
          [
              'id'    => 21,
              'state_id' => 2,
              'title' => 'Kra Daadi',
          ],
          [
              'id'    => 22,
              'state_id' => 2,
              'title' => 'Kurung Kumey',
          ],
          [
              'id'    => 23,
              'state_id' => 2,
              'title' => 'Lepa Rada',
          ],
          [
              'id'    => 24,
              'state_id' => 2,
              'title' => 'Lohit',
          ],
          [
              'id'    => 25,
              'state_id' => 2,
              'title' => 'Longding',
          ],
          [
              'id'    => 26,
              'state_id' => 2,
              'title' => 'Lower Dibang Valley',
          ],
          [
              'id'    => 27,
              'state_id' => 2,
              'title' => 'Lower Siang',
          ],
          [
              'id'    => 28,
              'state_id' => 2,
              'title' => 'Lower Subansir',
          ],
          [
              'id'    => 29,
              'state_id' => 2,
              'title' => 'Namsai',
          ],
          [
              'id'    => 30,
              'state_id' => 2,
              'title' => 'Pakke Kessang',
          ],
          [
              'id'    => 31,
              'state_id' => 2,
              'title' => 'Papum Pare',
          ],
          [
              'id'    => 32,
              'state_id' => 2,
              'title' => 'Shi Yomi',
          ],
          [
              'id'    => 33,
              'state_id' => 2,
              'title' => 'Tawang',
          ],
          [
              'id'    => 34,
              'state_id' => 2,
              'title' => 'Tirap',
          ],
          [
              'id'    => 35,
              'state_id' => 2,
              'title' => 'Upper Siang',
          ],
          [
              'id'    => 36,
              'state_id' => 2,
              'title' => 'Upper Subansir',
          ],
          [
              'id'    => 37,
              'state_id' => 2,
              'title' => 'West Kameng',
          ],
          [
              'id'    => 38,
              'state_id' => 2,
              'title' => 'West Siang',
          ],
          [
              'id'    => 39,
              'state_id' => 3,
              'title' => 'Baksa',
          ],
          [
              'id'    => 40,
              'state_id' => 3,
              'title' => 'Barpeta',
          ],
          [
              'id'    => 41,
              'state_id' => 3,
              'title' => 'Biswanath',
          ],
          [
              'id'    => 42,
              'state_id' => 3,
              'title' => 'Bongaigaon',
          ],
          [
              'id'    => 43,
              'state_id' => 3,
              'title' => 'Cachar',
          ],
          [
              'id'    => 44,
              'state_id' => 3,
              'title' => 'Charaideo',
          ],
          [
              'id'    => 45,
              'state_id' => 3,
              'title' => 'Chirang',
          ],
          [
              'id'    => 46,
              'state_id' => 3,
              'title' => 'Darrang',
          ],
          [
              'id'    => 47,
              'state_id' => 3,
              'title' => 'Dhemaji',
          ],
          [
              'id'    => 48,
              'state_id' => 3,
              'title' => 'Dhubri',
          ],
          [
              'id'    => 49,
              'state_id' => 3,
              'title' => 'Dibrugarh',
          ],
          [
              'id'    => 50,
              'state_id' => 3,
              'title' => 'Dima Hasao',
          ],
          [
              'id'    => 51,
              'state_id' => 3,
              'title' => 'Goalpara',
          ],
          [
              'id'    => 52,
              'state_id' => 3,
              'title' => 'Golaghat',
          ],
          [
              'id'    => 53,
              'state_id' => 3,
              'title' => 'Hailakandi',
          ],
          [
              'id'    => 54,
              'state_id' => 3,
              'title' => 'Hojai',
          ],
          [
              'id'    => 55,
              'state_id' => 3,
              'title' => 'Jorhat',
          ],
          [
              'id'    => 56,
              'state_id' => 3,
              'title' => 'Kamrup',
          ],
          [
              'id'    => 57,
              'state_id' => 3,
              'title' => 'Kamrup Metropolitan',
          ],
          [
              'id'    => 58,
              'state_id' => 3,
              'title' => 'Karbi Anglong',
          ],
          [
              'id'    => 59,
              'state_id' => 3,
              'title' => 'Karimganj',
          ],
          [
              'id'    => 60,
              'state_id' => 3,
              'title' => 'Kokrajhar',
          ],
          [
              'id'    => 61,
              'state_id' => 3,
              'title' => 'Lakhimpur',
          ],
          [
              'id'    => 62,
              'state_id' => 3,
              'title' => 'Majuli',
          ],
          [
              'id'    => 63,
              'state_id' => 3,
              'title' => 'Morigaon',
          ],
          [
              'id'    => 64,
              'state_id' => 3,
              'title' => 'Nagaon',
          ],
          [
              'id'    => 65,
              'state_id' => 3,
              'title' => 'Nalbari',
          ],
          [
              'id'    => 66,
              'state_id' => 3,
              'title' => 'Sivasagar',
          ],
          [
              'id'    => 67,
              'state_id' => 3,
              'title' => 'Sonitpur',
          ],
          [
              'id'    => 68,
              'state_id' => 3,
              'title' => 'South Salmara-Mankachar',
          ],
          [
              'id'    => 69,
              'state_id' => 3,
              'title' => 'Tinsukia',
          ],
          [
              'id'    => 70,
              'state_id' => 3,
              'title' => 'Udalguri',
          ],
          [
              'id'    => 71,
              'state_id' => 3,
              'title' => 'West Karbi Anglong',
          ],
          [
              'id'    => 72,
              'state_id' => 4,
              'title' => 'Araria',
          ],
          [
              'id'    => 73,
              'state_id' => 4,
              'title' => 'Arwal',
          ],
          [
              'id'    => 74,
              'state_id' => 4,
              'title' => 'Aurangabad',
          ],
          [
              'id'    => 75,
              'state_id' => 4,
              'title' => 'Banka',
          ],
          [
              'id'    => 76,
              'state_id' => 4,
              'title' => 'Begusarai',
          ],
          [
              'id'    => 77,
              'state_id' => 4,
              'title' => 'Bhagalpur',
          ],
          [
              'id'    => 78,
              'state_id' => 4,
              'title' => 'Bhojpur',
          ],
          [
              'id'    => 79,
              'state_id' => 4,
              'title' => 'Buxar',
          ],
          [
              'id'    => 80,
              'state_id' => 4,
              'title' => 'Darbhanga',
          ],
          [
              'id'    => 81,
              'state_id' => 4,
              'title' => 'East Champaran',
          ],
          [
              'id'    => 82,
              'state_id' => 4,
              'title' => 'Gaya',
          ],
          [
              'id'    => 83,
              'state_id' => 4,
              'title' => 'Gopalganj',
          ],
          [
              'id'    => 84,
              'state_id' => 4,
              'title' => 'Jamui',
          ],
          [
              'id'    => 85,
              'state_id' => 4,
              'title' => 'Jehanabad',
          ],
          [
              'id'    => 86,
              'state_id' => 4,
              'title' => 'Kaimur',
          ],
          [
              'id'    => 87,
              'state_id' => 4,
              'title' => 'Katihar',
          ],
          [
              'id'    => 88,
              'state_id' => 4,
              'title' => 'Khagaria',
          ],
          [
              'id'    => 89,
              'state_id' => 4,
              'title' => 'Kishanganj',
          ],
          [
              'id'    => 90,
              'state_id' => 4,
              'title' => ' Lakhisarai',
          ],
          [
              'id'    => 91,
              'state_id' => 4,
              'title' => 'Madhepura',
          ],
          [
              'id'    => 92,
              'state_id' => 4,
              'title' => 'Madhubani',
          ],
          [
              'id'    => 93,
              'state_id' => 4,
              'title' => 'Munger',
          ],
          [
              'id'    => 94,
              'state_id' => 4,
              'title' => 'Muzaffarpur',
          ],
          [
              'id'    => 95,
              'state_id' => 4,
              'title' => 'Nalanda',
          ],
          [
              'id'    => 96,
              'state_id' => 4,
              'title' => 'Nawada',
          ],
          [
              'id'    => 97,
              'state_id' => 4,
              'title' => 'Patna',
          ],
          [
              'id'    => 98,
              'state_id' => 4,
              'title' => 'Purnia',
          ],
          [
              'id'    => 99,
              'state_id' => 4,
              'title' => 'Purnia',
          ],
          [
              'id'    => 100,
              'state_id' => 4,
              'title' => 'Saharsa',
          ],
          [
              'id'    => 101,
              'state_id' => 4,
              'title' => 'Samastipur',
          ],
          [
              'id'    => 102,
              'state_id' => 4,
              'title' => 'Saran',
          ],
          [
              'id'    => 103,
              'state_id' => 4,
              'title' => 'Sheikhpura',
          ],
          [
              'id'    => 104,
              'state_id' => 4,
              'title' => 'Sheohar',
          ],
          [
              'id'    => 105,
              'state_id' => 4,
              'title' => 'Sitamarhi',
          ],
          [
              'id'    => 106,
              'state_id' => 4,
              'title' => 'Siwan',
          ],
          [
              'id'    => 107,
              'state_id' => 4,
              'title' => 'Supaul',
          ],
          [
              'id'    => 108,
              'state_id' => 4,
              'title' => 'Vaishali',
          ],
          [
              'id'    => 109,
              'state_id' => 4,
              'title' => 'West Champaran',
          ],
          [
              'id'    => 110,
              'state_id' => 5,
              'title' => 'Balod',
          ],
          [
              'id'    => 111,
              'state_id' => 5,
              'title' => 'Baloda Bazar',
          ],
          [
              'id'    => 112,
              'state_id' => 5,
              'title' => 'Balrampur',
          ],
          [
              'id'    => 113,
              'state_id' => 5,
              'title' => 'Bastar',
          ],
          [
              'id'    => 114,
              'state_id' => 5,
              'title' => 'Bemetara',
          ],
          [
              'id'    => 115,
              'state_id' => 5,
              'title' => 'Bijapur',
          ],
          [
              'id'    => 116,
              'state_id' => 5,
              'title' => 'Bilaspur',
          ],
          [
              'id'    => 117,
              'state_id' => 5,
              'title' => 'Dantewada',
          ],
          [
              'id'    => 118,
              'state_id' => 5,
              'title' => 'Dhamtari',
          ],
          [
              'id'    => 119,
              'state_id' => 5,
              'title' => 'Durg',
          ],
          [
              'id'    => 120,
              'state_id' => 5,
              'title' => 'Gariaband',
          ],
          [
              'id'    => 121,
              'state_id' => 5,
              'title' => 'Gaurela Pendra Marwahi',
          ],
          [
              'id'    => 122,
              'state_id' => 5,
              'title' => 'Janjgir Champa',
          ],
          [
              'id'    => 123,
              'state_id' => 5,
              'title' => 'Jashpur',
          ],
          [
              'id'    => 124,
              'state_id' => 5,
              'title' => 'Kabirdham',
          ],
          [
              'id'    => 125,
              'state_id' => 5,
              'title' => 'Kanker',
          ],
          [
              'id'    => 126,
              'state_id' => 5,
              'title' => 'Kondagaon',
          ],
          [
              'id'    => 127,
              'state_id' => 5,
              'title' => 'Korba',
          ],
          [
              'id'    => 128,
              'state_id' => 5,
              'title' => 'Koriya',
          ],
          [
              'id'    => 129,
              'state_id' => 5,
              'title' => 'Mahasamund',
          ],
          [
              'id'    => 130,
              'state_id' => 5,
              'title' => 'Mungeli',
          ],
          [
              'id'    => 131,
              'state_id' => 5,
              'title' => 'Narayanpur',
          ],
          [
              'id'    => 132,
              'state_id' => 5,
              'title' => 'Raigarh',
          ],
          [
              'id'    => 133,
              'state_id' => 5,
              'title' => 'Raipur',
          ],
          [
              'id'    => 134,
              'state_id' => 5,
              'title' => 'Rajnandgaon',
          ],
          [
              'id'    => 135,
              'state_id' => 5,
              'title' => 'Sukma',
          ],
          [
              'id'    => 136,
              'state_id' => 5,
              'title' => 'Surajpur',
          ],
          [
              'id'    => 137,
              'state_id' => 5,
              'title' => 'Surguja',
          ],
          [
              'id'    => 138,
              'state_id' => 6,
              'title' => 'North Goa',
          ],
          [
              'id'    => 139,
              'state_id' => 6,
              'title' => 'South Goa',
          ],
          [
              'id'    => 140,
              'state_id' => 7,
              'title' => 'Ahmedabad',
          ],
          [
              'id'    => 141,
              'state_id' => 7,
              'title' => 'Amreli',
          ],
          [
              'id'    => 142,
              'state_id' => 7,
              'title' => 'Anand',
          ],
          [
              'id'    => 143,
              'state_id' => 7,
              'title' => 'Aravalli',
          ],
          [
              'id'    => 144,
              'state_id' => 7,
              'title' => 'Banaskantha',
          ],
          [
              'id'    => 145,
              'state_id' => 7,
              'title' => 'Bharuch',
          ],
          [
              'id'    => 146,
              'state_id' => 7,
              'title' => 'Bhavnagar',
          ],
          [
              'id'    => 147,
              'state_id' => 7,
              'title' => 'Botad',
          ],
          [
              'id'    => 148,
              'state_id' => 7,
              'title' => 'Chhota Udaipur',
          ],
          [
              'id'    => 149,
              'state_id' => 7,
              'title' => 'Dahod',
          ],
          [
              'id'    => 150,
              'state_id' => 7,
              'title' => 'Dang',
          ],

          [
              'id'    => 151,
              'state_id' => 7,
              'title' => 'Devbhoomi Dwarka',
          ],
          [
              'id'    => 152,
              'state_id' => 7,
              'title' => 'Gandhinagar',
          ],
          [
              'id'    => 153,
              'state_id' => 7,
              'title' => 'Gir Somnath',
          ],
          [
              'id'    => 154,
              'state_id' =>7,
              'title' => 'Jamnagar',
          ],
          [
              'id'    => 155,
              'state_id' => 7,
              'title' => 'Junagadh',
          ],
          [
              'id'    => 156,
              'state_id' => 7,
              'title' => 'Kheda',
          ],
          [
              'id'    => 157,
              'state_id' => 7,
              'title' => 'Kutch',
          ],
          [
              'id'    => 158,
              'state_id' => 7,
              'title' => 'Mahisagar',
          ],
          [
              'id'    => 159,
              'state_id' => 7,
              'title' => 'Mehsana',
          ],
          [
              'id'    => 160,
              'state_id' => 7,
              'title' => 'Morbi',
          ],
          [
              'id'    => 161,
              'state_id' => 7,
              'title' => 'Narmada',
          ],
          [
              'id'    => 162,
              'state_id' => 7,
              'title' => 'Navsari',
          ],
          [
              'id'    => 163,
              'state_id' => 7,
              'title' => 'West Panchmahal',
          ],
          [
              'id'    => 164,
              'state_id' => 7,
              'title' => 'Patan',
          ],
          [
              'id'    => 165,
              'state_id' => 7,
              'title' => 'Porbandar',
          ],
          [
              'id'    => 166,
              'state_id' => 7,
              'title' => 'Rajkot',
          ],
          [
              'id'    => 167,
              'state_id' => 7,
              'title' => 'Sabarkantha',
          ],
          [
              'id'    => 168,
              'state_id' => 7,
              'title' => 'Surat',
          ],
          [
              'id'    => 169,
              'state_id' => 7,
              'title' => 'Surendranagar',
          ],
          [
              'id'    => 170,
              'state_id' => 7,
              'title' => 'Tapi',
          ],
          [
              'id'    => 171,
              'state_id' => 7,
              'title' => 'Vadodara',
          ],
          [
              'id'    => 172,
              'state_id' => 7,
              'title' => 'Valsad',
          ],
          [
              'id'    => 173,
              'state_id' => 8,
              'title' => 'Ambala',
          ],
          [
              'id'    => 174,
              'state_id' => 8,
              'title' => 'Bhiwani',
          ],
          [
              'id'    => 175,
              'state_id' => 8,
              'title' => 'Charkhi Dadri',
          ],
          [
              'id'    => 176,
              'state_id' => 8,
              'title' => 'Faridabad',
          ],
          [
              'id'    => 177,
              'state_id' => 8,
              'title' => 'Fatehabad',
          ],
          [
              'id'    => 178,
              'state_id' => 8,
              'title' => 'Gurugram',
          ],
          [
              'id'    => 179,
              'state_id' => 8,
              'title' => 'Hisar',
          ],
          [
              'id'    => 180,
              'state_id' => 8,
              'title' => 'Jhajjar',
          ],
          [
              'id'    => 181,
              'state_id' => 8,
              'title' => 'Jind',
          ],
          [
              'id'    => 182,
              'state_id' => 8,
              'title' => 'Kaithal',
          ],
          [
              'id'    => 183,
              'state_id' => 8,
              'title' => 'Karnal',
          ],
          [
              'id'    => 184,
              'state_id' => 8,
              'title' => 'Kurukshetra',
          ],
          [
              'id'    => 185,
              'state_id' => 8,
              'title' => 'Mahendragarh',
          ],
          [
              'id'    => 186,
              'state_id' => 8,
              'title' => 'Mewat',
          ],
          [
              'id'    => 187,
              'state_id' => 8,
              'title' => 'Palwal',
          ],
          [
              'id'    => 188,
              'state_id' => 8,
              'title' => 'Panchkula',
          ],
          [
              'id'    => 189,
              'state_id' => 8,
              'title' => 'Panipat',
          ],
          [
              'id'    => 190,
              'state_id' => 8,
              'title' => 'Rewari',
          ],
          [
              'id'    => 191,
              'state_id' => 8,
              'title' => 'Rohtak',
          ],
          [
              'id'    => 192,
              'state_id' => 8,
              'title' => 'Sirsa',
          ],
          [
              'id'    => 193,
              'state_id' => 8,
              'title' => 'Sonipat',
          ],
          [
              'id'    => 194,
              'state_id' => 8,
              'title' => 'Yamunanagar',
          ],
          [
              'id'    => 195,
              'state_id' => 9,
              'title' => 'Bilaspur',
          ],
          [
              'id'    => 196,
              'state_id' => 9,
              'title' => 'Chamba',
          ],
          [
              'id'    => 197,
              'state_id' => 9,
              'title' => 'Hamirpur',
          ],
          [
              'id'    => 198,
              'state_id' => 9,
              'title' => 'Kangra',
          ],
          [
              'id'    => 199,
              'state_id' => 9,
              'title' => 'Kinnaur',
          ],
          [
              'id'    => 200,
              'state_id' => 9,
              'title' => 'Kullu',
          ],
          [
              'id'    => 201,
              'state_id' => 9,
              'title' => 'Lahaul Spiti',
          ],
          [
              'id'    => 202,
              'state_id' => 9,
              'title' => 'Mandi',
          ],
          [
              'id'    => 203,
              'state_id' => 9,
              'title' => 'Shimla',
          ],
          [
              'id'    => 204,
              'state_id' => 9,
              'title' => 'Sirmaur',
          ],
          [
              'id'    => 205,
              'state_id' => 9,
              'title' => 'Solan',
          ],
          [
              'id'    => 206,
              'state_id' => 9,
              'title' => 'Una',
          ],
          [
              'id'    => 207,
              'state_id' => 10,
              'title' => 'Anantnag',
          ],
          [
              'id'    => 208,
              'state_id' => 10,
              'title' => 'Bandipora',
          ],
          [
              'id'    => 209,
              'state_id' => 10,
              'title' => 'Baramulla',
          ],
          [
              'id'    => 210,
              'state_id' => 10,
              'title' => 'Budgam',
          ],
          [
              'id'    => 211,
              'state_id' => 10,
              'title' => 'Doda',
          ],
          [
              'id'    => 212,
              'state_id' => 10,
              'title' => 'Ganderbal',
          ],
          [
              'id'    => 213,
              'state_id' => 10,
              'title' => 'Jammu',
          ],
          [
              'id'    => 214,
              'state_id' => 10,
              'title' => 'Kathua',
          ],
          [
              'id'    => 215,
              'state_id' => 10,
              'title' => 'Kishtwar',
          ],
          [
              'id'    => 216,
              'state_id' => 10,
              'title' => 'Kulgam',
          ],
          [
              'id'    => 217,
              'state_id' => 10,
              'title' => 'Kupwara',
          ],
          [
              'id'    => 218,
              'state_id' => 10,
              'title' => 'Poonch',
          ],
          [
              'id'    => 219,
              'state_id' => 10,
              'title' => 'Pulwama',
          ],
          [
              'id'    => 220,
              'state_id' => 10,
              'title' => 'Rajouri',
          ],
          [
              'id'    => 221,
              'state_id' => 10,
              'title' => 'Ramban',
          ],
          [
              'id'    => 222,
              'state_id' => 10,
              'title' => 'Reasi',
          ],
          [
              'id'    => 223,
              'state_id' => 10,
              'title' => 'Samba',
          ],
          [
              'id'    => 224,
              'state_id' => 10,
              'title' => 'Shopian',
          ],
          [
              'id'    => 225,
              'state_id' => 10,
              'title' => 'Srinagar',
          ],
          [
              'id'    => 226,
              'state_id' => 10,
              'title' => 'Udhampur',
          ],
          [
              'id'    => 227,
              'state_id' => 11,
              'title' => 'Bokaro',
          ],
          [
              'id'    => 228,
              'state_id' => 11,
              'title' => 'Chatra',
          ],
          [
              'id'    => 229,
              'state_id' => 11,
              'title' => 'Deoghar',
          ],
          [
              'id'    => 230,
              'state_id' => 11,
              'title' => 'Dhanbad',
          ],
          [
              'id'    => 231,
              'state_id' => 11,
              'title' => 'Dumka',
          ],
          [
              'id'    => 232,
              'state_id' => 11,
              'title' => 'East Singhbhum',
          ],
          [
              'id'    => 233,
              'state_id' => 11,
              'title' => 'Garhwa',
          ],
          [
              'id'    => 234,
              'state_id' => 11,
              'title' => 'Giridih',
          ],
          [
              'id'    => 235,
              'state_id' => 11,
              'title' => 'Godda',
          ],
          [
              'id'    => 236,
              'state_id' => 11,
              'title' => 'Gumla',
          ],
          [
              'id'    => 237,
              'state_id' => 11,
              'title' => 'Hazaribagh',
          ],
          [
              'id'    => 238,
              'state_id' => 11,
              'title' => 'Jamtara',
          ],
          [
              'id'    => 239,
              'state_id' => 11,
              'title' => 'Khunti',
          ],
          [
              'id'    => 240,
              'state_id' => 11,
              'title' => 'Koderma',
          ],
          [
              'id'    => 241,
              'state_id' => 11,
              'title' => 'Latehar',
          ],
          [
              'id'    => 242,
              'state_id' => 11,
              'title' => 'Lohardaga',
          ],
          [
              'id'    => 243,
              'state_id' => 11,
              'title' => 'Pakur',
          ],
          [
              'id'    => 244,
              'state_id' => 11,
              'title' => 'Palamu',
          ],
          [
              'id'    => 245,
              'state_id' => 11,
              'title' => 'Ramgarh',
          ],
          [
              'id'    => 246,
              'state_id' => 11,
              'title' => 'Ranchi',
          ],
          [
              'id'    => 247,
              'state_id' => 11,
              'title' => 'Sahebganj',
          ],
          [
              'id'    => 248,
              'state_id' => 11,
              'title' => 'Seraikela Kharsawan',
          ],
          [
              'id'    => 249,
              'state_id' => 11,
              'title' => 'Simdega',
          ],
          [
              'id'    => 250,
              'state_id' => 11,
              'title' => 'West Singhbhum',
          ],
          [
              'id'    => 251,
              'state_id' => 12,
              'title' => 'Bagalkot',
          ],
          [
              'id'    => 252,
              'state_id' => 12,
              'title' => 'Bangalore Rural',
          ],
          [
              'id'    => 253,
              'state_id' => 12,
              'title' => 'Bangalore Urban',
          ],
          [
              'id'    => 254,
              'state_id' => 12,
              'title' => 'Belgaum',
          ],
          [
              'id'    => 255,
              'state_id' => 12,
              'title' => 'Bellary',
          ],
          [
              'id'    => 256,
              'state_id' => 12,
              'title' => 'Bidar',
          ],
          [
              'id'    => 257,
              'state_id' => 12,
              'title' => 'Chamarajanagar',
          ],
          [
              'id'    => 258,
              'state_id' => 12,
              'title' => 'Chikkaballapur',
          ],
          [
              'id'    => 259,
              'state_id' => 12,
              'title' => 'Chikkamagaluru',
          ],
          [
              'id'    => 260,
              'state_id' => 12,
              'title' => 'Chitradurga',
          ],
          [
              'id'    => 261,
              'state_id' => 12,
              'title' => 'Dakshina Kannada',
          ],
          [
              'id'    => 262,
              'state_id' => 12,
              'title' => 'Davanagere',
          ],
          [
              'id'    => 263,
              'state_id' => 12,
              'title' => 'Dharwad',
          ],
          [
              'id'    => 264,
              'state_id' => 12,
              'title' => 'Gadag',
          ],
          [
              'id'    => 265,
              'state_id' => 12,
              'title' => 'Gulbarga',
          ],
          [
              'id'    => 266,
              'state_id' => 12,
              'title' => 'Hassan',
          ],
          [
              'id'    => 267,
              'state_id' => 12,
              'title' => 'Haveri',
          ],
          [
              'id'    => 268,
              'state_id' => 12,
              'title' => 'Kodagu',
          ],
          [
              'id'    => 269,
              'state_id' => 12,
              'title' => 'Kolar',
          ],
          [
              'id'    => 270,
              'state_id' => 12,
              'title' => 'Koppal',
          ],
          [
              'id'    => 271,
              'state_id' => 12,
              'title' => 'Mandya',
          ],
          [
              'id'    => 272,
              'state_id' => 12,
              'title' => 'Mysore',
          ],
          [
              'id'    => 273,
              'state_id' => 12,
              'title' => 'Raichur',
          ],
          [
              'id'    => 274,
              'state_id' => 12,
              'title' => 'Ramanagara',
          ],
          [
              'id'    => 275,
              'state_id' => 12,
              'title' => 'Shimoga',
          ],
          [
              'id'    => 276,
              'state_id' => 12,
              'title' => 'Tumkur',
          ],
          [
              'id'    => 277,
              'state_id' => 12,
              'title' => 'Udupi',
          ],
          [
              'id'    => 278,
              'state_id' => 12,
              'title' => 'Uttara Kannada',
          ],
          [
              'id'    => 279,
              'state_id' => 12,
              'title' => 'Vijayapura',
          ],
          [
              'id'    => 280,
              'state_id' => 12,
              'title' => 'Yadgir',
          ],
          [
              'id'    => 281,
              'state_id' => 13,
              'title' => 'Alappuzha',
          ],
          [
              'id'    => 282,
              'state_id' => 13,
              'title' => 'Ernakulam',
          ],
          [
              'id'    => 283,
              'state_id' => 13,
              'title' => 'Idukki',
          ],
          [
              'id'    => 284,
              'state_id' => 13,
              'title' => 'Kannur',
          ],
          [
              'id'    => 285,
              'state_id' => 13,
              'title' => 'Kasaragod',
          ],
          [
              'id'    => 286,
              'state_id' => 13,
              'title' => 'Kollam',
          ],
          [
              'id'    => 287,
              'state_id' => 13,
              'title' => 'Kottayam',
          ],
          [
              'id'    => 288,
              'state_id' => 13,
              'title' => 'Kozhikode',
          ],
          [
              'id'    => 289,
              'state_id' => 13,
              'title' => 'Malappuram',
          ],
          [
              'id'    => 290,
              'state_id' => 13,
              'title' => 'Palakkad',
          ],
          [
              'id'    => 291,
              'state_id' => 13,
              'title' => 'Pathanamthitta',
          ],
          [
              'id'    => 292,
              'state_id' => 13,
              'title' => 'Thiruvananthapuram',
          ],
          [
              'id'    => 293,
              'state_id' => 13,
              'title' => 'Thrissur',
          ],
          [
              'id'    => 294,
              'state_id' => 13,
              'title' => 'Wayanad',
          ],
          [
              'id'    => 295,
              'state_id' => 14,
              'title' => 'Agar Malwa',
          ],
          [
              'id'    => 296,
              'state_id' => 14,
              'title' => 'Alirajpur',
          ],
          [
              'id'    => 297,
              'state_id' => 14,
              'title' => 'Anuppur',
          ],
          [
              'id'    => 298,
              'state_id' => 14,
              'title' => 'Ashoknagar',
          ],
          [
              'id'    => 299,
              'state_id' => 14,
              'title' => 'Balaghat',
          ],
          [
              'id'    => 300,
              'state_id' => 14,
              'title' => 'Barwani',
          ],
          [
              'id'    => 301,
              'state_id' => 14,
              'title' => 'Betul',
          ],
          [
              'id'    => 302,
              'state_id' => 14,
              'title' => 'Bhind',
          ],
          [
              'id'    => 303,
              'state_id' => 14,
              'title' => 'Bhopal',
          ],
          [
              'id'    => 304,
              'state_id' => 14,
              'title' => 'Burhanpur',
          ],
          [
              'id'    => 305,
              'state_id' => 14,
              'title' => 'Chhatarpur',
          ],
          [
              'id'    => 306,
              'state_id' => 14,
              'title' => 'Chhindwara',
          ],
          [
              'id'    => 307,
              'state_id' => 14,
              'title' => 'Damoh',
          ],
          [
              'id'    => 308,
              'state_id' => 14,
              'title' => 'Datia',
          ],
          [
              'id'    => 309,
              'state_id' => 14,
              'title' => 'Dewas',
          ],
          [
              'id'    => 310,
              'state_id' => 14,
              'title' => 'Dhar',
          ],
          [
              'id'    => 311,
              'state_id' => 14,
              'title' => 'Dindori',
          ],
          [
              'id'    => 312,
              'state_id' => 14,
              'title' => 'Guna',
          ],
          [
              'id'    => 313,
              'state_id' => 14,
              'title' => 'Gwalior',
          ],
          [
              'id'    => 314,
              'state_id' => 14,
              'title' => 'Harda',
          ],
          [
              'id'    => 315,
              'state_id' => 14,
              'title' => 'Hoshangabad',
          ],
          [
              'id'    => 316,
              'state_id' => 14,
              'title' => 'Indore',
          ],
          [
              'id'    => 317,
              'state_id' => 14,
              'title' => 'Jabalpur',
          ],
          [
              'id'    => 318,
              'state_id' => 14,
              'title' => 'Jhabua',
          ],
          [
              'id'    => 319,
              'state_id' => 14,
              'title' => 'Katni',
          ],
          [
              'id'    => 320,
              'state_id' => 14,
              'title' => 'Khandwa',
          ],
          [
              'id'    => 321,
              'state_id' => 14,
              'title' => 'Khargone',
          ],
          [
              'id'    => 322,
              'state_id' => 14,
              'title' => 'Mandla',
          ],
          [
              'id'    => 323,
              'state_id' => 14,
              'title' => 'Mandsaur',
          ],
          [
              'id'    => 324,
              'state_id' => 14,
              'title' => 'Morena',
          ],
          [
              'id'    => 325,
              'state_id' => 14,
              'title' => 'Narsinghpur',
          ],
          [
              'id'    => 326,
              'state_id' => 14,
              'title' => 'Neemuch',
          ],
          [
              'id'    => 327,
              'state_id' => 14,
              'title' => 'Niwari',
          ],
          [
              'id'    => 328,
              'state_id' => 14,
              'title' => 'Panna',
          ],
          [
              'id'    => 329,
              'state_id' => 14,
              'title' => 'Raisen',
          ],
          [
              'id'    => 330,
              'state_id' => 14,
              'title' => 'Rajgarh',
          ],
          [
              'id'    => 331,
              'state_id' => 14,
              'title' => 'Ratlam',
          ],
          [
              'id'    => 332,
              'state_id' => 14,
              'title' => 'Rewa',
          ],
          [
              'id'    => 333,
              'state_id' => 14,
              'title' => 'Sagar',
          ],
          [
              'id'    => 334,
              'state_id' => 14,
              'title' => 'Satna',
          ],
          [
              'id'    => 335,
              'state_id' => 14,
              'title' => 'USehore',
          ],
          [
              'id'    => 336,
              'state_id' => 14,
              'title' => 'Seoni',
          ],
          [
              'id'    => 337,
              'state_id' => 14,
              'title' => 'Shahdol',
          ],
          [
              'id'    => 338,
              'state_id' => 14,
              'title' => 'Shajapur',
          ],
          [
              'id'    => 339,
              'state_id' => 14,
              'title' => 'Sheopur',
          ],
          [
              'id'    => 340,
              'state_id' => 14,
              'title' => 'Shivpuri',
          ],
          [
              'id'    => 341,
              'state_id' => 14,
              'title' => 'Sidhi',
          ],
          [
              'id'    => 342,
              'state_id' => 14,
              'title' => 'Singrauli',
          ],
          [
              'id'    => 343,
              'state_id' => 14,
              'title' => 'Tikamgarh',
          ],
          [
              'id'    => 344,
              'state_id' => 14,
              'title' => 'Ujjain',
          ],
          [
              'id'    => 345,
              'state_id' => 14,
              'title' => 'Umaria',
          ],
          [
              'id'    => 346,
              'state_id' => 14,
              'title' => 'Vidisha',
          ],
          [
              'id'    => 347,
              'state_id' => 15,
              'title' => 'Ahmednagar',
          ],
          [
              'id'    => 348,
              'state_id' => 15,
              'title' => 'Akola',
          ],
          [
              'id'    => 349,
              'state_id' => 15,
              'title' => 'Amravati',
          ],
          [
              'id'    => 350,
              'state_id' => 15,
              'title' => 'Aurangabad',
          ],
          [
              'id'    => 351,
              'state_id' => 15,
              'title' => 'Beed',
          ],
          [
              'id'    => 352,
              'state_id' => 15,
              'title' => 'Bhandara',
          ],
          [
              'id'    => 353,
              'state_id' => 15,
              'title' => 'Buldhana',
          ],
          [
              'id'    => 354,
              'state_id' => 15,
              'title' => 'Chandrapur',
          ],
          [
              'id'    => 355,
              'state_id' => 15,
              'title' => 'Dhule',
          ],
          [
              'id'    => 356,
              'state_id' => 15,
              'title' => 'Gadchiroli',
          ],
          [
              'id'    => 357,
              'state_id' => 15,
              'title' => 'Gondia',
          ],
          [
              'id'    => 358,
              'state_id' => 15,
              'title' => 'Hingoli',
          ],
          [
              'id'    => 359,
              'state_id' => 15,
              'title' => 'Jalgaon',
          ],
          [
              'id'    => 360,
              'state_id' => 15,
              'title' => 'Jalna',
          ],
          [
              'id'    => 361,
              'state_id' => 15,
              'title' => 'Kolhapur',
          ],
          [
              'id'    => 362,
              'state_id' => 15,
              'title' => 'Latur',
          ],
          [
              'id'    => 363,
              'state_id' => 15,
              'title' => 'Mumbai City',
          ],
          [
              'id'    => 364,
              'state_id' => 15,
              'title' => 'Mumbai Suburban',
          ],
          [
              'id'    => 365,
              'state_id' => 15,
              'title' => 'Nagpur',
          ],
          [
              'id'    => 366,
              'state_id' => 15,
              'title' => 'Nanded',
          ],
          [
              'id'    => 367,
              'state_id' => 15,
              'title' => 'Nandurbar',
          ],
          [
              'id'    => 368,
              'state_id' => 15,
              'title' => 'Nashik',
          ],
          [
              'id'    => 369,
              'state_id' => 15,
              'title' => 'Osmanabad',
          ],
          [
              'id'    => 370,
              'state_id' => 15,
              'title' => 'Palghar',
          ],
          [
              'id'    => 371,
              'state_id' => 15,
              'title' => 'Parbhani',
          ],
          [
              'id'    => 372,
              'state_id' => 15,
              'title' => 'Pune',
          ],
          [
              'id'    => 373,
              'state_id' => 15,
              'title' => 'Raigad',
          ],
          [
              'id'    => 374,
              'state_id' => 15,
              'title' => 'Ratnagiri',
          ],
          [
              'id'    => 375,
              'state_id' => 15,
              'title' => 'Sangli',
          ],
          [
              'id'    => 376,
              'state_id' => 15,
              'title' => 'Satara',
          ],
          [
              'id'    => 377,
              'state_id' => 15,
              'title' => 'Sindhudurg',
          ],
          [
              'id'    => 378,
              'state_id' => 15,
              'title' => 'Solapur',
          ],
          [
              'id'    => 379,
              'state_id' => 15,
              'title' => 'Thane',
          ],
          [
              'id'    => 380,
              'state_id' => 15,
              'title' => 'Wardha',
          ],
          [
              'id'    => 381,
              'state_id' => 15,
              'title' => 'Washim',
          ],
          [
              'id'    => 382,
              'state_id' => 15,
              'title' => 'Yavatmal',
          ],
          [
              'id'    => 383,
              'state_id' => 16,
              'title' => 'Bishnupur',
          ],
          [
              'id'    => 384,
              'state_id' => 16,
              'title' => 'Chandel',
          ],
          [
              'id'    => 385,
              'state_id' => 16,
              'title' => 'Churachandpur',
          ],
          [
              'id'    => 386,
              'state_id' => 16,
              'title' => 'Imphal East',
          ],
          [
              'id'    => 387,
              'state_id' => 16,
              'title' => 'Imphal West',
          ],
          [
              'id'    => 388,
              'state_id' => 16,
              'title' => 'Jiribam',
          ],
          [
              'id'    => 389,
              'state_id' => 16,
              'title' => 'Kakching',
          ],
          [
              'id'    => 390,
              'state_id' => 16,
              'title' => 'Kamjong',
          ],
          [
              'id'    => 391,
              'state_id' => 16,
              'title' => 'Kangpokpi',
          ],
          [
              'id'    => 392,
              'state_id' => 16,
              'title' => 'Noney',
          ],
          [
              'id'    => 393,
              'state_id' => 16,
              'title' => 'Pherzawl',
          ],
          [
              'id'    => 394,
              'state_id' => 16,
              'title' => 'Senapati',
          ],
          [
              'id'    => 395,
              'state_id' => 16,
              'title' => 'Tamenglong',
          ],
          [
              'id'    => 396,
              'state_id' => 16,
              'title' => 'Tengnoupal',
          ],
          [
              'id'    => 397,
              'state_id' => 16,
              'title' => 'Thoubal',
          ],
          [
              'id'    => 398,
              'state_id' => 16,
              'title' => 'Ukhrul',
          ],
          [
              'id'    => 399,
              'state_id' => 17,
              'title' => 'East Garo Hills',
          ],
          [
              'id'    => 400,
              'state_id' => 17,
              'title' => 'East Jaintia Hills',
          ],
          [
              'id'    => 401,
              'state_id' => 17,
              'title' => 'East Khasi Hills',
          ],
          [
              'id'    => 402,
              'state_id' => 17,
              'title' => 'North Garo Hills',
          ],
          [
              'id'    => 403,
              'state_id' => 17,
              'title' => 'Ri Bhoi',
          ],
          [
              'id'    => 404,
              'state_id' => 17,
              'title' => 'South Garo Hills',
          ],
          [
              'id'    => 405,
              'state_id' => 17,
              'title' => 'South West Garo Hills',
          ],
          [
              'id'    => 406,
              'state_id' => 17,
              'title' => 'South West Khasi Hills',
          ],
          [
              'id'    => 407,
              'state_id' => 17,
              'title' => 'West Garo Hills',
          ],
          [
              'id'    => 408,
              'state_id' => 17,
              'title' => 'West Jaintia Hills',
          ],
          [
              'id'    => 409,
              'state_id' => 17,
              'title' => 'West Khasi Hills',
          ],
          [
              'id'    => 410,
              'state_id' => 18,
              'title' => 'Aizawl',
          ],
          [
              'id'    => 411,
              'state_id' => 18,
              'title' => 'Champhai',
          ],
          [
              'id'    => 412,
              'state_id' => 18,
              'title' => 'Kolasib',
          ],
          [
              'id'    => 413,
              'state_id' => 18,
              'title' => 'Lawngtlai',
          ],
          [
              'id'    => 414,
              'state_id' => 18,
              'title' => 'Lunglei',
          ],
          [
              'id'    => 415,
              'state_id' => 18,
              'title' => 'Mamit',
          ],
          [
              'id'    => 416,
              'state_id' => 18,
              'title' => 'Saiha',
          ],
          [
              'id'    => 417,
              'state_id' => 18,
              'title' => 'Serchhip',
          ],
          [
              'id'    => 418,
              'state_id' => 19,
              'title' => 'Mon',
          ],
          [
              'id'    => 419,
              'state_id' => 19,
              'title' => 'Dimapur',
          ],
          [
              'id'    => 420,
              'state_id' => 19,
              'title' => 'Kiphire',
          ],
          [
              'id'    => 421,
              'state_id' => 19,
              'title' => 'Kohima',
          ],
          [
              'id'    => 422,
              'state_id' => 19,
              'title' => 'Longleng',
          ],
          [
              'id'    => 423,
              'state_id' => 19,
              'title' => 'Mokokchung',
          ],
          [
              'id'    => 424,
              'state_id' => 19,
              'title' => 'Noklak',
          ],
          [
              'id'    => 425,
              'state_id' => 19,
              'title' => 'Peren',
          ],
          [
              'id'    => 426,
              'state_id' => 19,
              'title' => 'Phek',
          ],
          [
              'id'    => 427,
              'state_id' => 19,
              'title' => 'Tuensang',
          ],
          [
              'id'    => 428,
              'state_id' => 19,
              'title' => 'Wokha',
          ],
          [
              'id'    => 429,
              'state_id' => 19,
              'title' => 'Zunheboto',
          ],
          [
              'id'    => 430,
              'state_id' => 20,
              'title' => 'Angul',
          ],
          [
              'id'    => 431,
              'state_id' => 20,
              'title' => 'Balangir',
          ],
          [
              'id'    => 432,
              'state_id' => 20,
              'title' => 'Balangir',
          ],
          [
              'id'    => 433,
              'state_id' => 20,
              'title' => 'Bargarh',
          ],
          [
              'id'    => 434,
              'state_id' => 20,
              'title' => 'Bhadrak',
          ],
          [
              'id'    => 435,
              'state_id' => 20,
              'title' => 'Boudh',
          ],
          [
              'id'    => 436,
              'state_id' => 20,
              'title' => 'Cuttack',
          ],
          [
              'id'    => 437,
              'state_id' => 20,
              'title' => 'Debagarh',
          ],
          [
              'id'    => 438,
              'state_id' => 20,
              'title' => 'Dhenkanal',
          ],
          [
              'id'    => 439,
              'state_id' => 20,
              'title' => 'Gajapati',
          ],
          [
              'id'    => 440,
              'state_id' => 20,
              'title' => 'Ganjam',
          ],
          [
              'id'    => 441,
              'state_id' => 20,
              'title' => 'Jagatsinghpur',
          ],
          [
              'id'    => 442,
              'state_id' => 20,
              'title' => 'Jajpur',
          ],
          [
              'id'    => 443,
              'state_id' => 20,
              'title' => 'Jharsuguda',
          ],
          [
              'id'    => 444,
              'state_id' => 20,
              'title' => 'Kalahandi',
          ],
          [
              'id'    => 445,
              'state_id' => 20,
              'title' => 'Kandhamal',
          ],
          [
              'id'    => 446,
              'state_id' => 20,
              'title' => 'Kendrapara',
          ],
          [
              'id'    => 447,
              'state_id' => 20,
              'title' => 'Kendujhar',
          ],
          [
              'id'    => 448,
              'state_id' => 20,
              'title' => 'Khordha',
          ],
          [
              'id'    => 449,
              'state_id' => 20,
              'title' => 'Koraput',
          ],
          [
              'id'    => 450,
              'state_id' => 20,
              'title' => 'Malkangiri',
          ],
          [
              'id'    => 451,
              'state_id' => 20,
              'title' => 'Mayurbhanj',
          ],
          [
              'id'    => 452,
              'state_id' => 20,
              'title' => 'Nabarangpur',
          ],
          [
              'id'    => 453,
              'state_id' => 20,
              'title' => 'Nayagarh',
          ],
          [
              'id'    => 454,
              'state_id' => 20,
              'title' => 'Nuapada',
          ],
          [
              'id'    => 455,
              'state_id' => 20,
              'title' => 'Puri',
          ],
          [
              'id'    => 456,
              'state_id' => 20,
              'title' => 'Rayagada',
          ],
          [
              'id'    => 457,
              'state_id' => 20,
              'title' => 'Sambalpur',
          ],
          [
              'id'    => 458,
              'state_id' => 20,
              'title' => 'Subarnapur',
          ],
          [
              'id'    => 459,
              'state_id' => 20,
              'title' => 'Sundergarh',
          ],
          [
              'id'    => 460,
              'state_id' => 21,
              'title' => 'Amritsar',
          ],
          [
              'id'    => 461,
              'state_id' => 21,
              'title' => 'Barnala',
          ],
          [
              'id'    => 462,
              'state_id' => 21,
              'title' => 'Bathinda',
          ],
          [
              'id'    => 463,
              'state_id' => 21,
              'title' => 'Faridkot',
          ],
          [
              'id'    => 464,
              'state_id' => 21,
              'title' => 'Fatehgarh Sahib',
          ],
          [
              'id'    => 465,
              'state_id' => 21,
              'title' => 'Fazilka',
          ],
          [
              'id'    => 466,
              'state_id' => 21,
              'title' => 'Firozpur',
          ],
          [
              'id'    => 467,
              'state_id' => 21,
              'title' => 'Gurdaspur',
          ],
          [
              'id'    => 468,
              'state_id' => 21,
              'title' => 'Hoshiarpur',
          ],
          [
              'id'    => 469,
              'state_id' => 21,
              'title' => 'Jalandhar',
          ],
          [
              'id'    => 470,
              'state_id' => 21,
              'title' => 'Kapurthala',
          ],
          [
              'id'    => 471,
              'state_id' => 21,
              'title' => 'Ludhiana',
          ],
          [
              'id'    => 472,
              'state_id' => 21,
              'title' => 'Mansa',
          ],
          [
              'id'    => 473,
              'state_id' => 21,
              'title' => 'Moga',
          ],
          [
              'id'    => 474,
              'state_id' => 21,
              'title' => 'Mohali',
          ],
          [
              'id'    => 475,
              'state_id' => 21,
              'title' => 'Muktsar',
          ],
          [
              'id'    => 476,
              'state_id' => 21,
              'title' => 'Pathankot',
          ],
          [
              'id'    => 477,
              'state_id' => 21,
              'title' => 'Patiala',
          ],
          [
              'id'    => 478,
              'state_id' => 21,
              'title' => 'Rupnagar',
          ],
          [
              'id'    => 479,
              'state_id' => 21,
              'title' => 'Sangrur',
          ],
          [
              'id'    => 480,
              'state_id' => 21,
              'title' => 'Shaheed Bhagat Singh Nagar',
          ],
          [
              'id'    => 481,
              'state_id' => 21,
              'title' => 'Tarn Taran',
          ],
          [
              'id'    => 482,
              'state_id' => 22,
              'title' => 'Ajmer',
          ],
          [
              'id'    => 483,
              'state_id' => 22,
              'title' => 'Alwar',
          ],
          [
              'id'    => 484,
              'state_id' => 22,
              'title' => 'Banswara',
          ],
          [
              'id'    => 485,
              'state_id' => 22,
              'title' => 'Baran',
          ],
          [
              'id'    => 486,
              'state_id' => 22,
              'title' => 'Barmer',
          ],
          [
              'id'    => 487,
              'state_id' => 22,
              'title' => 'Bharatpur',
          ],
          [
              'id'    => 488,
              'state_id' => 22,
              'title' => 'Bhilwara',
          ],
          [
              'id'    => 489,
              'state_id' => 22,
              'title' => 'Bikaner',
          ],
          [
              'id'    => 490,
              'state_id' => 22,
              'title' => 'Bundi',
          ],
          [
              'id'    => 491,
              'state_id' => 22,
              'title' => 'Chittorgarh',
          ],
          [
              'id'    => 492,
              'state_id' => 22,
              'title' => 'Churu',
          ],
          [
              'id'    => 493,
              'state_id' => 22,
              'title' => 'Dausa',
          ],
          [
              'id'    => 494,
              'state_id' => 22,
              'title' => 'Dholpur',
          ],
          [
              'id'    => 495,
              'state_id' => 22,
              'title' => 'Dungarpur',
          ],
          [
              'id'    => 496,
              'state_id' => 22,
              'title' => 'Hanumangarh',
          ],
          [
              'id'    => 497,
              'state_id' => 22,
              'title' => 'Jaipur',
          ],
          [
              'id'    => 498,
              'state_id' => 22,
              'title' => 'Jaisalmer',
          ],
          [
              'id'    => 499,
              'state_id' => 22,
              'title' => 'Jalore',
          ],
          [
              'id'    => 500,
              'state_id' => 22,
              'title' => 'Jhalawar',
          ],
          [
              'id'    => 501,
              'state_id' => 22,
              'title' => 'Jhunjhunu',
          ],
          [
              'id'    => 502,
              'state_id' => 22,
              'title' => 'Jodhpur',
          ],
          [
              'id'    => 503,
              'state_id' => 22,
              'title' => 'Karauli',
          ],
          [
              'id'    => 504,
              'state_id' => 22,
              'title' => 'Kota',
          ],
          [
              'id'    => 505,
              'state_id' => 22,
              'title' => 'Nagaur',
          ],
          [
              'id'    => 506,
              'state_id' => 22,
              'title' => 'Pali',
          ],
          [
              'id'    => 507,
              'state_id' => 22,
              'title' => 'Pratapgarh',
          ],
          [
              'id'    => 508,
              'state_id' => 22,
              'title' => 'Rajsamand',
          ],
          [
              'id'    => 509,
              'state_id' => 22,
              'title' => 'Sawai Madhopur',
          ],
          [
              'id'    => 510,
              'state_id' => 22,
              'title' => 'Sikar',
          ],
          [
              'id'    => 511,
              'state_id' => 22,
              'title' => 'Sirohi',
          ],
          [
              'id'    => 512,
              'state_id' => 22,
              'title' => 'Sri Ganganagar',
          ],
          [
              'id'    => 513,
              'state_id' => 22,
              'title' => 'Tonk',
          ],
          [
              'id'    => 514,
              'state_id' => 22,
              'title' => 'Udaipur',
          ],
          [
              'id'    => 515,
              'state_id' => 23,
              'title' => 'East Sikkim',
          ],
          [
              'id'    => 516,
              'state_id' => 23,
              'title' => 'North Sikkim',
          ],
          [
              'id'    => 517,
              'state_id' => 23,
              'title' => 'South Sikkim',
          ],
          [
              'id'    => 518,
              'state_id' => 23,
              'title' => 'West Sikkim',
          ],
          [
              'id'    => 519,
              'state_id' => 24,
              'title' => 'Ariyalur',
          ],
          [
              'id'    => 520,
              'state_id' => 24,
              'title' => 'Chengalpattu',
          ],
          [
              'id'    => 521,
              'state_id' => 24,
              'title' => 'Chennai',
          ],
          [
              'id'    => 522,
              'state_id' => 24,
              'title' => 'Coimbatore',
          ],
          [
              'id'    => 523,
              'state_id' => 24,
              'title' => 'Cuddalore',
          ],
          [
              'id'    => 524,
              'state_id' => 24,
              'title' => 'Dharmapuri',
          ],
          [
              'id'    => 525,
              'state_id' => 24,
              'title' => 'Dindigul',
          ],
          [
              'id'    => 526,
              'state_id' => 24,
              'title' => 'Erode',
          ],
          [
              'id'    => 527,
              'state_id' => 24,
              'title' => 'Kallakurichi',
          ],
          [
              'id'    => 528,
              'state_id' => 24,
              'title' => 'Kanchipuram',
          ],
          [
              'id'    => 529,
              'state_id' => 24,
              'title' => 'Kanyakumari',
          ],
          [
              'id'    => 530,
              'state_id' => 24,
              'title' => 'Karur',
          ],
          [
              'id'    => 531,
              'state_id' => 24,
              'title' => 'Krishnagiri',
          ],
          [
              'id'    => 532,
              'state_id' => 24,
              'title' => 'Madurai',
          ],
          [
              'id'    => 533,
              'state_id' => 24,
              'title' => 'Nagapattinam',
          ],
          [
              'id'    => 534,
              'state_id' => 24,
              'title' => 'Namakkal',
          ],
          [
              'id'    => 535,
              'state_id' => 24,
              'title' => 'Nilgiris',
          ],
          [
              'id'    => 536,
              'state_id' => 24,
              'title' => 'Perambalur',
          ],
          [
              'id'    => 537,
              'state_id' => 24,
              'title' => 'Pudukkottai',
          ],
          [
              'id'    => 538,
              'state_id' => 24,
              'title' => 'Ramanathapuram',
          ],
          [
              'id'    => 539,
              'state_id' => 24,
              'title' => 'Ranipet',
          ],
          [
              'id'    => 540,
              'state_id' => 24,
              'title' => ' Salem',
          ],
          [
              'id'    => 541,
              'state_id' => 24,
              'title' => 'Sivaganga',
          ],
          [
              'id'    => 542,
              'state_id' => 24,
              'title' => 'Tenkasi',
          ],
          [
              'id'    => 543,
              'state_id' => 24,
              'title' => 'Thanjavur',
          ],
          [
              'id'    => 544,
              'state_id' => 24,
              'title' => 'Theni',
          ],
          [
              'id'    => 545,
              'state_id' => 24,
              'title' => 'Thoothukudi',
          ],
          [
              'id'    => 546,
              'state_id' => 24,
              'title' => 'Tiruchirappalli',
          ],
          [
              'id'    => 547,
              'state_id' => 24,
              'title' => 'Tirunelveli',
          ],
          [
              'id'    => 548,
              'state_id' => 24,
              'title' => 'Tirupattur',
          ],
          [
              'id'    => 549,
              'state_id' => 24,
              'title' => 'Tiruppur',
          ],
          [
              'id'    => 550,
              'state_id' => 24,
              'title' => 'Tiruvallur',
          ],
          [
              'id'    => 551,
              'state_id' => 24,
              'title' => 'Tiruvannamalai',
          ],
          [
              'id'    => 552,
              'state_id' => 24,
              'title' => 'Tiruvarur',
          ],
          [
              'id'    => 553,
              'state_id' => 24,
              'title' => 'Vellore',
          ],
          [
              'id'    => 554,
              'state_id' => 24,
              'title' => 'Viluppuram',
          ],
          [
              'id'    => 555,
              'state_id' => 24,
              'title' => 'Virudhunagar',
          ],
          [
              'id'    => 556,
              'state_id' => 25,
              'title' => 'Adilabad',
          ],
          [
              'id'    => 557,
              'state_id' => 25,
              'title' => 'Bhadradri Kothagudem',
          ],
          [
              'id'    => 558,
              'state_id' => 25,
              'title' => 'Hyderabad',
          ],
          [
              'id'    => 559,
              'state_id' => 25,
              'title' => 'Jagtial',
          ],
          [
              'id'    => 560,
              'state_id' => 25,
              'title' => 'Jangaon',
          ],
          [
              'id'    => 561,
              'state_id' => 25,
              'title' => 'Jayashankar',
          ],
          [
              'id'    => 562,
              'state_id' => 25,
              'title' => 'Jogulamba',
          ],
          [
              'id'    => 563,
              'state_id' => 25,
              'title' => 'Kamareddy',
          ],
          [
              'id'    => 564,
              'state_id' => 25,
              'title' => 'Karimnagar',
          ],
          [
              'id'    => 565,
              'state_id' => 25,
              'title' => 'Khammam',
          ],
          [
              'id'    => 566,
              'state_id' => 25,
              'title' => 'Komaram Bheem',
          ],
          [
              'id'    => 567,
              'state_id' => 25,
              'title' => 'Mahabubabad',
          ],
          [
              'id'    => 568,
              'state_id' => 25,
              'title' => 'Mahbubnagar',
          ],
          [
              'id'    => 569,
              'state_id' => 25,
              'title' => 'Mancherial',
          ],
          [
              'id'    => 570,
              'state_id' => 25,
              'title' => 'Medak',
          ],
          [
              'id'    => 571,
              'state_id' => 25,
              'title' => 'Medchal',
          ],
          [
              'id'    => 572,
              'state_id' => 25,
              'title' => 'Mulugu',
          ],
          [
              'id'    => 573,
              'state_id' => 25,
              'title' => 'Nagarkurnool',
          ],
          [
              'id'    => 574,
              'state_id' => 25,
              'title' => 'Nalgonda',
          ],
          [
              'id'    => 575,
              'state_id' => 25,
              'title' => 'Narayanpet',
          ],
          [
              'id'    => 576,
              'state_id' => 25,
              'title' => 'Nirmal',
          ],
          [
              'id'    => 577,
              'state_id' => 25,
              'title' => 'Nizamabad',
          ],
          [
              'id'    => 578,
              'state_id' => 25,
              'title' => 'Peddapalli',
          ],
          [
              'id'    => 579,
              'state_id' => 25,
              'title' => 'Rajanna Sircilla',
          ],
          [
              'id'    => 580,
              'state_id' => 25,
              'title' => 'Ranga Reddy',
          ],
          [
              'id'    => 581,
              'state_id' => 25,
              'title' => 'Sangareddy',
          ],
          [
              'id'    => 582,
              'state_id' => 25,
              'title' => 'Siddipet',
          ],
          [
              'id'    => 583,
              'state_id' => 25,
              'title' => 'Suryapet',
          ],
          [
              'id'    => 584,
              'state_id' => 25,
              'title' => 'Vikarabad',
          ],
          [
              'id'    => 585,
              'state_id' => 25,
              'title' => 'Wanaparthy',
          ],
          [
              'id'    => 586,
              'state_id' => 25,
              'title' => 'Warangal Rural',
          ],
          [
              'id'    => 587,
              'state_id' => 25,
              'title' => 'Warangal Urban',
          ],
          [
              'id'    => 588,
              'state_id' => 25,
              'title' => 'Yadadri Bhuvanagiri',
          ],
          [
              'id'    => 589,
              'state_id' => 26,
              'title' => 'Dhalai',
          ],
          [
              'id'    => 590,
              'state_id' => 26,
              'title' => 'Gomati',
          ],
          [
              'id'    => 591,
              'state_id' => 26,
              'title' => 'Khowai',
          ],
          [
              'id'    => 592,
              'state_id' => 26,
              'title' => 'North Tripura',
          ],
          [
              'id'    => 593,
              'state_id' => 26,
              'title' => 'Sepahijala',
          ],
          [
              'id'    => 594,
              'state_id' => 26,
              'title' => 'South Tripura',
          ],
          [
              'id'    => 595,
              'state_id' => 26,
              'title' => 'Unakoti',
          ],
          [
              'id'    => 596,
              'state_id' => 26,
              'title' => 'West Tripura',
          ],
          [
              'id'    => 597,
              'state_id' => 27,
              'title' => 'Agra',
          ],
          [
              'id'    => 598,
              'state_id' => 27,
              'title' => 'Aligarh',
          ],
          [
              'id'    => 599,
              'state_id' => 27,
              'title' => 'Ambedkar Nagar',
          ],
          [
              'id'    => 600,
              'state_id' => 27,
              'title' => 'Amethi',
          ],
          [
              'id'    => 601,
              'state_id' => 27,
              'title' => 'Amroha',
          ],
          [
              'id'    => 602,
              'state_id' => 27,
              'title' => 'Auraiya',
          ],
          [
              'id'    => 603,
              'state_id' => 27,
              'title' => 'Ayodhya',
          ],
          [
              'id'    => 604,
              'state_id' => 27,
              'title' => 'Azamgarh',
          ],
          [
              'id'    => 605,
              'state_id' => 27,
              'title' => 'Baghpat',
          ],
          [
              'id'    => 606,
              'state_id' => 27,
              'title' => 'Bahraich',
          ],
          [
              'id'    => 607,
              'state_id' => 27,
              'title' => 'Ballia',
          ],
          [
              'id'    => 608,
              'state_id' => 27,
              'title' => 'Balrampur',
          ],
          [
              'id'    => 609,
              'state_id' => 27,
              'title' => 'Banda',
          ],
          [
              'id'    => 610,
              'state_id' => 27,
              'title' => 'Barabanki',
          ],
          [
              'id'    => 611,
              'state_id' => 27,
              'title' => 'Bareilly',
          ],
          [
              'id'    => 612,
              'state_id' => 27,
              'title' => 'Basti',
          ],
          [
              'id'    => 613,
              'state_id' => 27,
              'title' => 'Bhadohi',
          ],
          [
              'id'    => 614,
              'state_id' => 27,
              'title' => 'Bijnor',
          ],
          [
              'id'    => 615,
              'state_id' => 27,
              'title' => 'Budaun',
          ],
          [
              'id'    => 616,
              'state_id' => 27,
              'title' => 'Bulandshahr',
          ],
          [
              'id'    => 617,
              'state_id' => 27,
              'title' => 'Chandauli',
          ],
          [
              'id'    => 618,
              'state_id' => 27,
              'title' => 'Chitrakoot',
          ],
          [
              'id'    => 619,
              'state_id' => 27,
              'title' => 'Deoria',
          ],
          [
              'id'    => 620,
              'state_id' => 27,
              'title' => 'Etah',
          ],
          [
              'id'    => 621,
              'state_id' => 27,
              'title' => 'Etawah',
          ],
          [
              'id'    => 622,
              'state_id' => 27,
              'title' => 'Farrukhabad',
          ],
          [
              'id'    => 623,
              'state_id' => 27,
              'title' => 'Fatehpur',
          ],
          [
              'id'    => 624,
              'state_id' => 27,
              'title' => 'Firozabad',
          ],
          [
              'id'    => 625,
              'state_id' => 27,
              'title' => 'Gautam Buddha Nagar',
          ],
          [
              'id'    => 626,
              'state_id' => 27,
              'title' => 'Ghaziabad',
          ],
          [
              'id'    => 627,
              'state_id' => 27,
              'title' => 'Ghazipur',
          ],
          [
              'id'    => 628,
              'state_id' => 27,
              'title' => 'Gonda',
          ],
          [
              'id'    => 629,
              'state_id' => 27,
              'title' => 'Gorakhpur',
          ],
          [
              'id'    => 630,
              'state_id' => 27,
              'title' => 'Hamirpur',
          ],
          [
              'id'    => 631,
              'state_id' => 27,
              'title' => 'Hapur',
          ],
          [
              'id'    => 632,
              'state_id' => 27,
              'title' => 'Hardoi',
          ],
          [
              'id'    => 633,
              'state_id' => 27,
              'title' => 'Hathras',
          ],
          [
              'id'    => 634,
              'state_id' => 27,
              'title' => 'Jalaun',
          ],
          [
              'id'    => 635,
              'state_id' => 27,
              'title' => 'Jaunpur',
          ],
          [
              'id'    => 636,
              'state_id' => 27,
              'title' => 'Jhansi',
          ],
          [
              'id'    => 637,
              'state_id' => 27,
              'title' => 'Kannauj',
          ],
          [
              'id'    => 638,
              'state_id' => 27,
              'title' => 'Kanpur Dehat',
          ],
          [
              'id'    => 639,
              'state_id' => 27,
              'title' => 'Kanpur Nagar',
          ],
          [
              'id'    => 640,
              'state_id' => 27,
              'title' => 'Kasganj',
          ],
          [
              'id'    => 641,
              'state_id' => 27,
              'title' => 'Kaushambi',
          ],
          [
              'id'    => 642,
              'state_id' => 27,
              'title' => 'Kheri',
          ],
          [
              'id'    => 643,
              'state_id' => 27,
              'title' => 'Kushinagar',
          ],
          [
              'id'    => 644,
              'state_id' => 27,
              'title' => 'Lalitpur',
          ],
          [
              'id'    => 645,
              'state_id' => 27,
              'title' => 'Lucknow',
          ],
          [
              'id'    => 646,
              'state_id' => 27,
              'title' => 'Maharajganj',
          ],
          [
              'id'    => 647,
              'state_id' => 27,
              'title' => 'Mahoba',
          ],
          [
              'id'    => 648,
              'state_id' => 27,
              'title' => 'Mainpuri',
          ],
          [
              'id'    => 649,
              'state_id' => 27,
              'title' => 'Mathura',
          ],
          [
              'id'    => 650,
              'state_id' => 27,
              'title' => 'Mau',
          ],
          [
              'id'    => 651,
              'state_id' => 27,
              'title' => 'Meerut',
          ],
          [
              'id'    => 652,
              'state_id' => 27,
              'title' => 'Mirzapur',
          ],
          [
              'id'    => 653,
              'state_id' => 27,
              'title' => 'Moradabad',
          ],
          [
              'id'    => 654,
              'state_id' => 27,
              'title' => 'Muzaffarnagar',
          ],
          [
              'id'    => 655,
              'state_id' => 27,
              'title' => 'Pilibhit',
          ],
          [
              'id'    => 656,
              'state_id' => 27,
              'title' => 'Pratapgarh',
          ],
          [
              'id'    => 657,
              'state_id' => 27,
              'title' => 'Prayagraj',
          ],
          [
              'id'    => 658,
              'state_id' => 27,
              'title' => 'Raebareli',
          ],
          [
              'id'    => 659,
              'state_id' => 27,
              'title' => 'Rampur',
          ],
          [
              'id'    => 660,
              'state_id' => 27,
              'title' => 'Saharanpur',
          ],
          [
              'id'    => 661,
              'state_id' => 27,
              'title' => 'Sambhal',
          ],
          [
              'id'    => 662,
              'state_id' => 27,
              'title' => 'Sant Kabir Nagar',
          ],
          [
              'id'    => 663,
              'state_id' => 27,
              'title' => 'Shahjahanpur',
          ],
          [
              'id'    => 664,
              'state_id' => 27,
              'title' => 'Shamli',
          ],
          [
              'id'    => 665,
              'state_id' => 27,
              'title' => 'Shravasti',
          ],
          [
              'id'    => 666,
              'state_id' => 27,
              'title' => 'Siddharthnagar',
          ],
          [
              'id'    => 667,
              'state_id' => 27,
              'title' => 'Sitapur',
          ],
          [
              'id'    => 668,
              'state_id' => 27,
              'title' => 'Sonbhadra',
          ],
          [
              'id'    => 669,
              'state_id' => 27,
              'title' => 'Sultanpur',
          ],
          [
              'id'    => 670,
              'state_id' => 27,
              'title' => 'Unnao',
          ],
          [
              'id'    => 671,
              'state_id' => 27,
              'title' => 'Varanasi',
          ],
          [
              'id'    => 672,
              'state_id' => 28,
              'title' => 'Almora',
          ],
          [
              'id'    => 673,
              'state_id' => 28,
              'title' => 'Bageshwar',
          ],
          [
              'id'    => 674,
              'state_id' => 28,
              'title' => 'Chamoli',
          ],
          [
              'id'    => 675,
              'state_id' => 28,
              'title' => 'Champawat',
          ],
          [
              'id'    => 676,
              'state_id' => 28,
              'title' => 'Dehradun',
          ],
          [
              'id'    => 677,
              'state_id' => 28,
              'title' => 'Haridwar',
          ],
          [
              'id'    => 678,
              'state_id' => 28,
              'title' => 'Nainital',
          ],
          [
              'id'    => 679,
              'state_id' => 28,
              'title' => 'Pauri',
          ],
          [
              'id'    => 680,
              'state_id' => 28,
              'title' => 'Pithoragarh',
          ],
          [
              'id'    => 681,
              'state_id' => 28,
              'title' => 'Rudraprayag',
          ],
          [
              'id'    => 682,
              'state_id' => 28,
              'title' => 'Tehri',
          ],
          [
              'id'    => 683,
              'state_id' => 28,
              'title' => 'Udham Singh Nagar',
          ],
          [
              'id'    => 684,
              'state_id' => 28,
              'title' => 'Uttarkashi',
          ],
          [
              'id'    => 685,
              'state_id' => 29,
              'title' => 'Alipurduar',
          ],
          [
              'id'    => 686,
              'state_id' => 29,
              'title' => 'Bankura',
          ],
          [
              'id'    => 687,
              'state_id' => 29,
              'title' => 'Birbhum',
          ],
          [
              'id'    => 688,
              'state_id' => 29,
              'title' => 'Cooch Behar',
          ],
          [
              'id'    => 689,
              'state_id' => 29,
              'title' => 'Dakshin Dinajpur',
          ],
          [
              'id'    => 690,
              'state_id' => 29,
              'title' => ' Darjeeling',
          ],
          [
              'id'    => 691,
              'state_id' => 29,
              'title' => 'Hooghly',
          ],
          [
              'id'    => 692,
              'state_id' => 29,
              'title' => 'Howrah',
          ],
          [
              'id'    => 693,
              'state_id' => 29,
              'title' => 'Jalpaiguri',
          ],
          [
              'id'    => 694,
              'state_id' => 29,
              'title' => 'Jhargram',
          ],
          [
              'id'    => 695,
              'state_id' => 29,
              'title' => 'Kalimpong',
          ],
          [
              'id'    => 696,
              'state_id' => 29,
              'title' => 'Kolkata',
          ],
          [
              'id'    => 697,
              'state_id' => 29,
              'title' => 'Malda',
          ],
          [
              'id'    => 698,
              'state_id' => 29,
              'title' => 'Murshidabad',
          ],
          [
              'id'    => 699,
              'state_id' => 29,
              'title' => 'Nadia',
          ],
          [
              'id'    => 700,
              'state_id' => 29,
              'title' => 'North 24 Parganas',
          ],
          [
              'id'    => 701,
              'state_id' => 29,
              'title' => 'Paschim Bardhaman',
          ],
          [
              'id'    => 702,
              'state_id' => 29,
              'title' => 'Paschim Medinipur',
          ],
          [
              'id'    => 703,
              'state_id' => 29,
              'title' => 'Purba Bardhaman',
          ],
          [
              'id'    => 704,
              'state_id' => 29,
              'title' => 'Purba Medinipur',
          ],
          [
              'id'    => 705,
              'state_id' => 29,
              'title' => 'Purulia',
          ],
          [
              'id'    => 706,
              'state_id' => 29,
              'title' => 'South 24 Parganas',
          ],
          [
              'id'    => 707,
              'state_id' => 29,
              'title' => 'Uttar Dinajpur',
          ],
          [
              'id'    => 708,
              'state_id' => 30,
              'title' => 'Nicobar',
          ],
          [
              'id'    => 709,
              'state_id' => 30,
              'title' => 'North Middle Andaman',
          ],
          [
              'id'    => 710,
              'state_id' => 30,
              'title' => 'South Andaman',
          ],
          [
              'id'    => 711,
              'state_id' => 31,
              'title' => 'Chandigarh',
          ],
          [
              'id'    => 712,
              'state_id' => 32,
              'title' => 'Dadra Nagar Haveli',
          ],
          [
              'id'    => 713,
              'state_id' => 32,
              'title' => 'Daman',
          ],
          [
              'id'    => 714,
              'state_id' => 32,
              'title' => 'Diu',
          ],
          [
              'id'    => 715,
              'state_id' => 33,
              'title' => 'Central Delhi',
          ],
          [
              'id'    => 716,
              'state_id' => 33,
              'title' => 'East Delhi',
          ],
          [
              'id'    => 717,
              'state_id' => 33,
              'title' => 'New Delhi',
          ],
          [
              'id'    => 718,
              'state_id' => 33,
              'title' => 'North Delhi',
          ],
          [
              'id'    => 719,
              'state_id' => 33,
              'title' => 'North East Delhi',
          ],
          [
              'id'    => 720,
              'state_id' => 33,
              'title' => 'North West Delhi',
          ],
          [
              'id'    => 721,
              'state_id' => 33,
              'title' => 'Shahdara',
          ],
          [
              'id'    => 722,
              'state_id' => 33,
              'title' => 'South Delhi',
          ],
          [
              'id'    => 723,
              'state_id' => 33,
              'title' => 'South East Delhi',
          ],
          [
              'id'    => 724,
              'state_id' => 33,
              'title' => 'South West Delhi',
          ],
          [
              'id'    => 725,
              'state_id' => 33,
              'title' => 'West Delhi',
          ],
          [
              'id'    => 726,
              'state_id' => 34,
              'title' => 'Lakshadweep',
          ],
          [
              'id'    => 727,
              'state_id' => 35,
              'title' => 'Kargil',
          ],
          [
              'id'    => 728,
              'state_id' => 35,
              'title' => 'Leh',
          ],
          [
              'id'    => 729,
              'state_id' => 36,
              'title' => 'Karaikal',
          ],
          [
              'id'    => 730,
              'state_id' => 36,
              'title' => 'Mahe',
          ],
          [
              'id'    => 731,
              'state_id' => 36,
              'title' => 'Puducherry',
          ],
          [
              'id'    => 732,
              'state_id' => 36,
              'title' => 'Yanam',
          ],
        ]);

    }
}
