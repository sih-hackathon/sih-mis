<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Faker\Factory as Faker;
use App\Loan;
use App\UserDetail;
class LoanSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create();
    	foreach (range(1508,6508) as $index)
      {
        DB::table('loans')->insert([
          'user_id' => $index,
          'loan_scheme_id' => $faker->numberBetween(1,6),
          'loan_amount'=> $faker->randomNumber(6, true),
          'interest_rate'=>$faker->numberBetween(6,10),
          'date_of_disbursement'=> Carbon::now()->format('Y-m-d'),
        ]);
      }
      $loans = Loan::all();
      foreach($loans as $loan)
      {
        $loan = Loan::find($loan->id);
        $user = UserDetail::where('user_id',$loan->user_id)->first();
        $loan->ngo_id = $user->ngo_id;
        $loan->self_help_group_id = $user->self_help_group_id;
        $loan->update();
      }
    }
}
