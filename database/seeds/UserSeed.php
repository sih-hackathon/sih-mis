<?php

use Illuminate\Database\Seeder;
use App\User;
use Faker\Factory as Faker;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table((new User)->getTable())->truncate();
      User::insert([
          [
              'id'    => 1,
              'name' => 'Super Administrator',
              'email'=> 'super-admin@test.com',
              'password'=> Hash::make('password'),
              'phone_number'=>'9867645546',
              'role_id'=>1,
          ],
          [
            'id'    => 2,
            'name' => 'RMK Administrator',
            'email'=> 'rmk-admin@test.com',
            'password'=> Hash::make('password'),
            'phone_number'=>'9653316033',
            'role_id'=>2,
          ],
          [
            'id'    => 3,
            'name' => 'NGO Administrator',
            'email'=> 'ngo-admin@test.com',
            'password'=> Hash::make('password'),
            'phone_number'=>'9757022785',
            'role_id'=>3,
          ],
          [
            'id'    => 4,
            'name' => 'SHG Administrator',
            'email'=> 'shg-admin@test.com',
            'password'=> Hash::make('password'),
            'phone_number'=>'900425406',
            'role_id'=>4,
          ],
          [
            'id'    => 5,
            'name' => 'Entrepreneur',
            'email'=> 'entrepreneur@test.com',
            'password'=> Hash::make('password'),
            'phone_number'=>'9422995629',
            'role_id'=>5,
          ],
      ]);
      $faker = Faker::create();
    	foreach (range(1,501) as $index)
      {
        DB::table('users')->insert([
          'name' => $faker->name,
          'email' => $faker->unique()->email,
          'password' => bcrypt('password'),
          'role_id'=>3,
        ]);
      }
      foreach (range(1,1001) as $index)
      {
        DB::table('users')->insert([
          'name' => $faker->name,
          'email' => $faker->unique()->email,
          'password' => bcrypt('password'),
          'role_id'=>4,
        ]);
      }
      foreach(range(1,5001) as $index)
      {
        DB::table('users')->insert([
          'name' => $faker->name,
          'email' => $faker->unique()->email,
          'password' => bcrypt('password'),
          'role_id'=>5,
        ]);
      }
    }
}
