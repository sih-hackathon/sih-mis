<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Role;
use Illuminate\Database\Eloquent\Model;
class DatabaseSeeder extends Seeder
{
    public function run()
    {
      Model::unguard();
      DB::statement('SET FOREIGN_KEY_CHECKS=0;');
      $this->call(RoleSeed::class);
      $this->call(UserSeed::class);
      $this->call(StateSeed::class);
      $this->call(DistrictSeed::class);
      $this->call(NgoSectorSeed::class);
      $this->call(ReligionSeed::class);
      $this->call(CasteSeed::class);
      $this->call(LiteracyLevelSeed::class);
      $this->call(LoanSchemeSeed::class);
      $this->call(IncomeLevelSeed::class);
//      $this->call(NgoAdminSeed::class);
      $this->call(NgoSeed::class);
      $this->call(UserDetailSeed::class);
      $this->call(LoanSeed::class);
      $this->call(RepaymentSeed::class);
      DB::statement('SET FOREIGN_KEY_CHECKS=1;');
      Model::reguard();
    }
}
