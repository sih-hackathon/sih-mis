<?php

use Illuminate\Database\Seeder;
use App\IncomeLevel;

class IncomeLevelSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table((new IncomeLevel)->getTable())->truncate();

      IncomeLevel::insert([
          [
              'id'             => 1,
              'title'          => 'Less than 50,000/year',
              'description'    => 'Income Level Category for those earning less than 50,000 per year',
          ],
          [
              'id'             => 2,
              'title'          => '50,000 - 1,00,000',
              'description'    => 'Income Level Category for those earning between 50,000 and 1,00,000 per year',
          ],
          [
              'id'             => 3,
              'title'          => '1,00,000 - 2,00,000',
              'description'    => 'Income Level Category for those earning between 1,00,000 and 2,00,000 per year',
          ],
          [
              'id'             => 4,
              'title'          => '2,00,000 - 3,00,000',
              'description'    => 'Income Level Category for those earning between 2,00,000 and 3,00,000 per year',
          ],
          [
              'id'             => 5,
              'title'          => '3,00,000 - 4,00,000',
              'description'    => 'Income Level Category for those earning between 3,00,000 and 4,00,000 per year',
          ],
          [
              'id'             => 6,
              'title'          => '4,00,000 - 5,00,000',
              'description'    => 'Income Level Category for those earning between 4,00,000 and 5,00,000 per year',
          ],
          [
              'id'             => 7,
              'title'          => 'More than 5,00,000/year',
              'description'    => 'Income Level Category for those earning more than 5,00,000 per year',
          ],

      ]);

    }
}
