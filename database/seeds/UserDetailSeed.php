<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use App\UserDetail;
use App\SelfHelpGroup;
use App\District;
class UserDetailSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = \Faker\Factory::create('en_IN');
      foreach (range(1508,6508) as $index)
      {
        DB::table('user_details')->insert([
          'user_id' => $index,
          'ngo_id' => $faker->numberBetween(1,500),
          'self_help_group_id' => $faker->numberBetween(1,1000),
          'state_id' => $faker->numberBetween(1,36),
          'district_id' => $faker->numberBetween(1,732),
          'income_level_id'=> $faker->numberBetween(1,7),
          'literacy_level_id'=> $faker->numberBetween(1,8),
          'religion_id'=> $faker->numberBetween(1,8),
          'caste_id'=> $faker->numberBetween(1,6),
          'age'=> $faker->numberBetween(18,70),
          'address'=>$faker->address,
          'no_of_family_members' =>  $faker->numberBetween(1,6)
        ]);
      }
      $users = UserDetail::all();
      foreach($users as $user)
      {
        $user = UserDetail::find($user->id);
        $districts = District::where('state_id',$user->state_id)->pluck('id');
        $shgs = SelfHelpGroup::where('ngo_id',$user->ngo_id)->pluck('id');
        $first_element = $districts->first();
        $last_element = $districts->last();
        $user->district_id = rand($first_element,$last_element);
        $user->self_help_group_id = $shgs->random();
        $user->update();
      }
    }
}
