<?php

use Illuminate\Database\Seeder;
use App\Caste;

class CasteSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table((new Caste)->getTable())->truncate();

      Caste::insert([
          [
              'id'             => 1,
              'title'          => 'General',
              'description'    => 'Caste Category for General Category who do not come under any caste',
          ],
          [
              'id'             => 2,
              'title'          => 'OBC',
              'description'    => 'Caste Category for OBC',
          ],
          [
              'id'             => 3,
              'title'          => 'SC',
              'description'    => 'Caste Category for Scheduled Castes',
          ],
          [
              'id'             => 4,
              'title'          => 'ST',
              'description'    => 'Caste Category for Scheduled Tribes',
          ],
          [
              'id'             => 5,
              'title'          => 'SBC',
              'description'    => 'Caste Category for Special Backward Classes',
          ],
          [
              'id'             => 6,
              'title'          => 'EBC',
              'description'    => 'Caste Category for Economically Backward Classes',
          ],
        ]);

    }
}
