<?php

use Illuminate\Database\Seeder;
use App\Religion;
class ReligionSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table((new Religion)->getTable())->truncate();

      Religion::insert([
          [
              'id'             => 1,
              'title'          => 'Hinduism',
              'description'    => 'Religion Category for Hindu Religion',
          ],
          [
              'id'             => 2,
              'title'          => 'Islam',
              'description'    => 'Relgion Category for Muslim Religion',
          ],
          [
              'id'             => 3,
              'title'          => 'Christianity',
              'description'    => 'Religion Category for Christian Relgion',
          ],
          [
              'id'             => 4,
              'title'          => 'Sikhism',
              'description'    => 'Religion Category for Sikh Religion',
          ],
          [
              'id'             => 5,
              'title'          => 'Zoroastrianism',
              'description'    => 'Religion Category for Zoroastrian Religion',
          ],
          [
              'id'             => 6,
              'title'          => 'Buddhism',
              'description'    => 'Religion Category for Buddhist Religion',
          ],
          [
              'id'             => 7,
              'title'          => 'Jainism',
              'description'    => 'Religion Category for Jain Religion',
          ],
          [
              'id'             => 8,
              'title'          => 'Others/Religion not specified',
              'description'    => 'Religion Category for remaining religion/ for those who do not want to specify religion',
          ],
        ]);

    }
}
