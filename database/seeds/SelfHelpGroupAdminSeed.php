<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class SelfHelpGroupAdminSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create();
    	foreach (range(1,1001) as $index)
      {
        DB::table('users')->insert([
          'name' => $faker->name,
          'email' => $faker->unique()->email,
          'password' => bcrypt('password'),
          'role_id'=>4,
        ]);
      }
    }
}
