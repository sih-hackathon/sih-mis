<?php

use Illuminate\Database\Seeder;
use App\LoanScheme;

class LoanSchemeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table((new LoanScheme)->getTable())->truncate();
      LoanScheme::insert([
        [
            'id'             => 1,
            'title'          => 'Loan Promotion Scheme',
            'description'    => 'This scheme is for providing loans to new and smaller organizations with experience of at least 6 months in thrift & credit. The organization can avail a maximum loan up to Rs. 10 lakhs.',
            'no_of_installments'=>30,
            'gestation_period_in_months'=>6,
        ],
        [
            'id'             => 2,
            'title'          => 'Main Loan Scheme',
            'description'    => 'This scheme is for providing loans to organizations having minimum 3 years’ experience in thrift & credit activities. The organization can avail a maximum loan upto 2 crores for 1 state. An organization can avail loan under the scheme for a maximum number of 3 states at a time. If the organization avails loan for more than 1 state, then the maximum loan amount can be upto Rs.6 crores.',
            'no_of_installments'=>30,
            'gestation_period_in_months'=>6,
        ],
        [
            'id'             => 3,
            'title'          => 'Gold Credit Scheme',
            'description'    => 'This scheme is for providing bulk loans to medium and large NGOs. This scheme is meant for organization which has in the past availed loan from RMK and have not defaulted in repayment. The organization can avail a maximum loan upto Rs 5 crores.',
            'no_of_installments'=>30,
            'gestation_period_in_months'=>6,
        ],
        [
            'id'             => 4,
            'title'          => 'Housing Loan Scheme',
            'description'    => 'This scheme is for providing loan for construction/ repair and maintenance of houses to women who are members of Self Help Group/ Joint Liability Groups (JLGs). This loan is provided through IMOs/NGOs/VOs. The organization can avail a maximum loan upto Rs 1,00,000/- per beneficiary for construction of low cost house. The organization has to mortgage the immovable property created out of RMK\'s loan by deposit of title deed (i.e. equitable mortgage). The NGO/IMO/VO may hold the deeds in trust for RMK and details of the same be captured in RMK database. An NGO/IMO/ VO can avail a maximum loan upto Rs. 6 crores for a maximum of 3 states at a time.',
            'no_of_installments'=>30,
            'gestation_period_in_months'=>12,
        ],
        [
            'id'             => 5,
            'title'          => 'Working Capital Loan (WCL) Scheme',
            'description'    => 'This scheme is for providing working capital term loan to the intermediary organization for backward and forward marketing linkage of product of Women SHGs/Individuals and group entrepreneurs, namely technology transfer, education and skill up gradation. The applicant has to submit a detailed project proposal to RMK.',
            'no_of_installments'=>30,
            'gestation_period_in_months'=>6,
        ],
        [
            'id'             => 6,
            'title'          => 'Repeat Loan Scheme',
            'description'    => 'For a repeat loan, the NGO/IMO must have promptly repaid 80% of the previous loan without any break or delay in repayment. All other criteria for assessment, approval etc. will be the same as that of availing a fresh loan under (i) to (iv) Lending Schemes of RMK.',
            'no_of_installments'=>30,
            'gestation_period_in_months'=>6,
        ],
  ]);

    }
}
