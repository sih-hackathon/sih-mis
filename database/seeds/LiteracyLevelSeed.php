<?php

use Illuminate\Database\Seeder;
use App\LiteracyLevel;

class LiteracyLevelSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table((new LiteracyLevel)->getTable())->truncate();

    LiteracyLevel::insert([
        [
            'id'             => 1,
            'title'          => 'No Formal Education',
            'description'    => 'Literacy Level Category for those who have never attended school.',
        ],
        [
            'id'             => 2,
            'title'          => 'Less than 5th Pass',
            'description'    => 'Literacy Level Category for those who have attended only Primary School',
        ],
        [
            'id'             => 3,
            'title'          => 'Less than 8th Pass',
            'description'    => 'Literacy Level Category for those who have attended only upto Secondary School',
        ],
        [
            'id'             => 4,
            'title'          => 'Less than 10th Pass',
            'description'    => 'Literacy Level Category for those who have attended only upto Higher Secondary School',
        ],
        [
            'id'             => 5,
            'title'          => '10th Pass',
            'description'    => 'Literacy Level Category for those who have completed their 10th Standard Schooling',
        ],
        [
            'id'             => 6,
            'title'          => 'Graduation',
            'description'    => 'Literacy Level Category for those who have completed their Graduation',
        ],
        [
            'id'             => 7,
            'title'          => 'Post Graduation',
            'description'    => 'Literacy Level Category for those who have completed their Post Graduation',
        ],
        [
            'id'             => 8,
            'title'          => 'PhD',
            'description'    => 'Literacy Level Category for those who have completed their PhD',
        ],
    ]);

    }
}
