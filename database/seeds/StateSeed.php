<?php

use Illuminate\Database\Seeder;
use App\State;

class StateSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table((new State)->getTable())->truncate();

      State::insert([
          [
              'id'    => 1,
              'title' => 'Andhra Pradesh',
          ],
          [
              'id'    => 2,
              'title' => 'Arunachal Pradesh',
          ],
          [
              'id'    => 3,
              'title' => 'Assam',
          ],
          [
              'id'    => 4,
              'title' => 'Bihar',
          ],
          [
              'id'    => 5,
              'title' => 'Chhattisgarh',
          ],
          [
              'id'    => 6,
              'title' => 'Goa',
          ],
          [
              'id'    => 7,
              'title' => 'Gujarat',
          ],
          [
              'id'    => 8,
              'title' => 'Haryana',
          ],
          [
              'id'    => 9,
              'title' => 'Himachal Pradesh',
          ],
          [
              'id'    => 10,
              'title' => 'Jammu & Kashmir',
          ],
          [
              'id'    => 11,
              'title' => 'Jharkhand',
          ],
          [
              'id'    => 12,
              'title' => 'Karnataka',
          ],
          [
              'id'    => 13,
              'title' => 'Kerala',
          ],
          [
              'id'    => 14,
              'title' => 'Madhya Pradesh',
          ],
          [
              'id'    => 15,
              'title' => 'Maharashtra',
          ],
          [
              'id'    => 16,
              'title' => 'Manipur',
          ],
          [
              'id'    => 17,
              'title' => 'Meghalaya',
          ],
          [
              'id'    => 18,
              'title' => 'Mizoram',
          ],
          [
              'id'    => 19,
              'title' => 'Nagaland',
          ],
          [
              'id'    => 20,
              'title' => 'Odisha',
          ],
          [
              'id'    => 21,
              'title' => 'Punjab',
          ],
          [
              'id'    => 22,
              'title' => 'Rajasthan',
          ],
          [
              'id'    => 23,
              'title' => 'Sikkim',
          ],
          [
              'id'    => 24,
              'title' => 'Tamil Nadu',
          ],
          [
              'id'    => 25,
              'title' => 'Telangana',
          ],
          [
              'id'    => 26,
              'title' => 'Tripura',
          ],
          [
              'id'    => 27,
              'title' => 'Uttar Pradesh',
          ],
          [
              'id'    => 28,
              'title' => 'Uttarakhand',
          ],
          [
              'id'    => 29,
              'title' => 'West Bengal',
          ],
          [
              'id'    => 30,
              'title' => 'Andaman & Nicobar',
          ],
          [
              'id'    => 31,
              'title' => 'Chandigarh',
          ],
          [
              'id'    => 32,
              'title' => 'Dadra and Nagar Haveli and Daman and Diu',
          ],
          [
              'id'    => 33,
              'title' => 'Delhi',
          ],
          [
              'id'    => 34,
              'title' => 'Lakshadweep',
          ],
          [
              'id'    => 35,
              'title' => 'Ladakh',
          ],
          [
              'id'    => 36,
              'title' => 'Puducherry',
          ],
      ]);

    }
}
