<?php

use Illuminate\Database\Seeder;
use App\Loan;
use App\Repayment;
use App\LoanScheme;
use Carbon\Carbon;
class RepaymentSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $loans = Loan::all();
      ini_set('max_execution_time', 0);
      foreach($loans as $loan)
      {
        $loan_scheme = LoanScheme::where('id',$loan->loan_scheme_id)->first();
        $no_of_installments = $loan_scheme->no_of_installments;
        $gestation_period = $loan_scheme->gestation_period_in_months;
        $start_date = Carbon::parse($loan->date_of_disbursement)->addMonthsNoOverflow($gestation_period);
        $total_amount = $loan->loan_amount;
        $loan->pre_gestation_loan_amount = $total_amount;
        $interest = 0;
        $interest_array = [];
        // Calculate the Total amount increased due to the gestation period
        for($i = 1; $i <= $gestation_period;$i++)
        {
          $interest = (($total_amount*($loan->interest_rate/100))/12);
          $total_amount = $total_amount + $interest;
        }
        $loan->post_gestation_loan_amount = $total_amount;
        $loan->update();
        $repayment = [];
        $new_interest = 0;
        $new_total_amount = $total_amount;
        $P = $total_amount;
        $R = (($loan->interest_rate/100)/12);
        $N = $no_of_installments;
        $first_comp =(1+$R);
        $first_calculation = pow($first_comp,$N);
        $second_comp = (1+$R);
        $second_calculation = pow($second_comp,$N);
        $remaining = $first_calculation/(($second_calculation)-1);
        $monthly_amount = $P * $R * $remaining;
        $first_interest = (($total_amount*($loan->interest_rate/100))/12);
        $first_principal = $monthly_amount - $first_interest;
        $new_total_amount = $total_amount;
        $reducing_array = [];
        for($i = 1; $i <= $no_of_installments;$i++)
        {
          if($i == 1)
          {
            $interest = (($new_total_amount * ($loan->interest_rate/100))/12);
            $principal = $monthly_amount - $interest;
            $repayment = new Repayment;
            $repayment->loan_id = $loan->id;
            $repayment->user_id = $loan->user_id;
            $repayment->ngo_id = $loan->ngo_id;
            $repayment->self_help_group_id = $loan->self_help_group_id;
            $repayment->title = "Installment No. ".$i;
            $repayment->installment_no = $i;
            $repayment->principal = $first_principal;
            $repayment->interest = $first_interest;
            $repayment->amount = $first_principal + $first_interest;
            $repayment->repayment_status = rand(1,4);
            $repayment->due_date = Carbon::parse($loan->date_of_disbursement)->addMonthsNoOverflow($i+$gestation_period);
            $repayment->save();
            $principal = $repayment->principal;
            $due_date = $repayment->due_date;
          }
          elseif($i != 1)
          {
            $new_total_amount = $new_total_amount - $principal;
            $interest = (($new_total_amount * ($loan->interest_rate/100))/12);
            $principal = $monthly_amount - $interest;
            $repayment = new Repayment;
            $repayment->loan_id = $loan->id;
            $repayment->user_id = $loan->user_id;
            $repayment->ngo_id = $loan->ngo_id;
            $repayment->self_help_group_id = $loan->self_help_group_id;
            $repayment->title = "Installment No. ".$i;
            $repayment->installment_no = $i;
            $repayment->principal = $principal;
            $repayment->interest = $interest;
            $repayment->amount = $principal + $interest;
            $repayment->repayment_status = rand(1,4);
            $repayment->due_date = Carbon::parse($due_date)->addMonthsNoOverflow(1);
            $repayment->save();
            $due_date = $repayment->due_date;
          }
        }
      }
    }
}
