<?php

use Illuminate\Database\Seeder;
use App\NgoSector;

class NgoSectorSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table((new NgoSector)->getTable())->truncate();

      NgoSector::insert([
          [
              'title'          => 'Animal Husbandry, Dairying & Fisheries',

          ],
          [
              'title'          => 'Aged/Elderly',

          ],
          [
              'title'          => 'Agriculture',

          ],
          [
              'title'          => 'Art & Culture',

          ],
          [
              'title'          => 'Biotechnology',

          ],
          [
              'title'          => 'Children',

          ],
          [
              'title'          => 'Civic Issues',

          ],
          [
              'title'          => 'Differently Abled',

          ],
          [
              'title'          => 'Disaster Management ',

          ],
          [
              'title'          => 'Dalit Upliftment',

          ],
          [
              'title'          => 'Drinking Water',

          ],
          [
              'title'          => 'Education & Literacy',

          ],
          [
              'title'          => 'Environment & Forests',

          ],
          [
              'title'          => 'Food Processing',

          ],
          [
              'title'          => 'Health & Family Welfare',

          ],
          [
              'title'          => 'HIV/AIDS',

          ],
          [
              'title'          => 'Housing',

          ],
          [
              'title'          => 'Human Rights',

          ],
          [
              'description'    => 'Information & Communication Technology',

          ],
          [
              'title'          => 'Legal Awareness & Aid',

          ],
          [
              'title'          => 'Labour & Employment',

          ],
          [
              'title'          => 'Land Resources',

          ],
          [
              'title'          => 'Micro Finance',

          ],
          [
              'title'          => 'Minority Issues',

          ],
          [
              'title'          => 'Micro Small & Medium Enterprises',

          ],
          [
              'title'          => 'New & Renewable Energy',

          ],
          [
              'title'          => 'Nutrition',

          ],
          [
              'title'          => 'Panchayati Raj',

          ],
          [
              'title'          => "Prisoner's Issues",

          ],
          [
              'title'          => 'Right To Information & Advocacy',

          ],
          [
              'title'          => 'Rural Development & Poverty Alleviation',

          ],
          [
              'title'          => 'Scientific & Industrial Research',

          ],
          [
              'title'          => 'Science & Technology',

          ],
          [
              'title'          => 'Sports',

          ],
          [
              'title'          => 'Tribal Affairs',

          ],
          [
              'title'          => 'Tourism',

          ],
          [
              'title'          => 'Urban Development & Poverty Alleviation',

          ],
          [
              'title'          => 'Vocational Training',

          ],
          [
              'title'          => 'Water Resources',

          ],
          [
              'title'          => "Women's Development & Empowerment",

          ],
          [
              'title'          => 'Youth Affairs',

          ],
          [
              'title'          => 'Any Other',

          ],
        ]);
    }
}
