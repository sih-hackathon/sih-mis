<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Ngo;
use App\NgoActiveSector;
use App\SelfHelpGroup;
use Faker\Generator as Faker;

$factory->define(Ngo::class, function (Faker $faker) {
    return [
      'name' => $faker->company,
      'state_id' => $faker->numberBetween(1,36),
      'district_id' => $faker->numberBetween(1,732),
      'admin_id' => $faker->unique()->numberBetween(6,506),
      'darpan_unique_id' => $faker->unique()->randomNumber(6),
      'achievements'=> $faker->sentence,
    ];
});

$factory->define(NgoActiveSector::class, function (Faker $faker) {
    return [
      'sector_id' => $faker->numberBetween(1,42),
    ];
});
$factory->define(SelfHelpGroup::class, function (Faker $faker) {
    return [
      'name' => $faker->company,
      'state_id' => $faker->numberBetween(1,36),
      'district_id' => $faker->numberBetween(1,732),
      'admin_id' => $faker->unique()->numberBetween(507,1507),
      'address' => $faker->address,
    ];
});
