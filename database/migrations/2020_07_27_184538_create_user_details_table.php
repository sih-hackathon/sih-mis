<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('ngo_id')->nullable();
            $table->unsignedBigInteger('state_id')->nullable();
            $table->unsignedBigInteger('district_id')->nullable();
            $table->unsignedBigInteger('income_level_id')->nullable();
            $table->unsignedBigInteger('literacy_level_id')->nullable();
            $table->unsignedBigInteger('religion_id')->nullable();
            $table->unsignedBigInteger('caste_id')->nullable();
            $table->unsignedBigInteger('self_help_group_id')->nullable();
            $table->text('economic_activity')->nullable();
            $table->text('aadhar_card_no')->nullable();
            $table->text('bank_account_no')->nullable();
            $table->text('ifsc_code')->nullable();
            $table->text('age')->nullable();
            $table->text('address')->nullable();
            $table->text('savings')->nullable();
            $table->text('no_of_family_members')->nullable();
            $table->text('mobile_number')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('ngo_id')->references('id')->on('ngos')->onDelete('cascade');
            $table->foreign('state_id')->references('id')->on('states')->onDelete('cascade');
            $table->foreign('district_id')->references('id')->on('districts')->onDelete('cascade');
            $table->foreign('income_level_id')->references('id')->on('income_levels')->onDelete('cascade');
            $table->foreign('literacy_level_id')->references('id')->on('literacy_levels')->onDelete('cascade');
            $table->foreign('religion_id')->references('id')->on('religions')->onDelete('cascade');
            $table->foreign('caste_id')->references('id')->on('castes')->onDelete('cascade');
            $table->foreign('self_help_group_id')->references('id')->on('self_help_groups')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}
