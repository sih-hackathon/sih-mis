<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repayments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('loan_id')->nullable();
            $table->unsignedBigInteger('ngo_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('self_help_group_id')->nullable();
            $table->text('title')->nullable();
            $table->text('installment_no')->nullable();
            $table->decimal('amount',12,4)->nullable();
            $table->decimal('principal',12,4)->nullable();
            $table->decimal('interest',12,4)->nullable();
            $table->decimal('paid_amount',12,4)->nullable();
            $table->text('due_date')->nullable();
            $table->text('paid_on')->nullable();
            // New Repayment Status 4 = before time, 3 = on time, 2 = Delayed , 1 = not paid
            $table->text('repayment_status')->nullable();
            $table->text('method_of_payment')->nullable();
            $table->text('payment_file_pdf_url')->nullable();
            $table->text('transaction_no')->nullable();
            $table->foreign('loan_id')->references('id')->on('loans');
            $table->foreign('ngo_id')->references('id')->on('ngos');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('self_help_group_id')->references('id')->on('self_help_groups');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repayments');
    }
}
