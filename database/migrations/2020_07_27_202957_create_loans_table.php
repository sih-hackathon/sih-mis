<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('ngo_id')->nullable();
            $table->unsignedBigInteger('self_help_group_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('loan_scheme_id')->unsigned()->nullable();
            $table->decimal('loan_amount',12,4)->nullable();
            $table->decimal('interest_rate',12,4)->nullable();
            $table->text('date_of_disbursement')->nullable();
            $table->text('repayment_from_date')->nullable();
            $table->text('total_repayment_amount')->nullable();
            $table->text('pre_gestation_loan_amount')->nullable();
            $table->text('post_gestation_loan_amount')->nullable();
            $table->text('total_repaid_amount')->nullable();
            $table->text('due_date')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('ngo_id')->references('id')->on('ngos')->onDelete('cascade');
            $table->foreign('self_help_group_id')->references('id')->on('self_help_groups');
            $table->foreign('loan_scheme_id')->references('id')->on('loan_schemes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
