<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNgoActiveSectorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ngo_active_sectors', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->unsignedBigInteger('ngo_id')->nullable();
          $table->foreign('ngo_id')->references('id')->on('ngos');
          $table->unsignedBigInteger('sector_id')->nullable();
          $table->foreign('sector_id')->references('id')->on('ngo_sectors');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ngo_active_sectors');
    }
}
