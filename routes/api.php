<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('auth')->group(function () {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::get('refresh', 'AuthController@refresh');

    Route::group(['middleware' => 'auth:api'], function(){
        Route::get('user', 'AuthController@user');
        Route::post('logout', 'AuthController@logout');
    });
});


Route::get('charts/{state_id}/ngo-performance-state-wise',['uses'=>'ChartController@get_ngo_performance_state_wise','as'=>'ngo-performance-state-wise']);

Route::get('charts/{district_id}/ngo-performance-district-wise',['uses'=>'ChartController@get_ngo_performance_district_wise','as'=>'ngo-performance-district-wise']);

Route::get('reports/all-states',['uses'=>'ReportController@all_states_all_time']);
Route::get('reports/{state_id}/all-districts',['uses'=>'ReportController@all_districts_particular_state_all_time']);
Route::get('reports/all-income-level',['uses'=>'ReportController@all_income_levels_all_time']);
Route::get('reports/all-literacy-level',['uses'=>'ReportController@all_literacy_levels_all_time']);
Route::get('reports/all-caste',['uses'=>'ReportController@all_castes_all_time']);
Route::get('reports/all-religion',['uses'=>'ReportController@all_religions_all_time']);
Route::get('reports/{state_id}/all-ngos',['uses'=>'ReportController@all_ngos_particular_state_all_time']);
Route::group(['middleware' => 'auth:api'], function(){
    // Roles
    Route::get('roles',['uses'=>'RoleController@index','as'=>'roles.index']);
    Route::post('roles/store',['uses'=>'RoleController@store','as'=>'roles.store']);
    Route::get('roles/{id}/edit',['uses'=>'RoleController@edit','as'=>'roles.edit']);
    Route::put('roles/{id}/update',['uses'=>'RoleController@update','as'=>'roles.update']);
    Route::get('roles/{id}/view',['uses'=>'RoleController@show','as'=>'roles.view']);

    // Users
    Route::get('users', ['uses'=>'UserController@index','as'=>'users.index']);
    Route::post('users/store',['uses'=>'UserController@store','as'=>'users.store']);
    Route::get('users/{id}/edit',['uses'=>'UserController@edit','as'=>'users.edit']);
    Route::put('users/{id}/update',['uses'=>'UserController@update','as'=>'users.update']);
    Route::get('users/{id}/view', ['uses'=>'UserController@show','as'=>'users.view']);
    Route::post('/users/import','UserController@import');

    //Get all users with role as NGO admin
    Route::get('users/ngoAdmin',['uses'=>'UserController@get_ngo_user','as'=>'users.ngoAdmin']);

    // Get all users with role as SHG Admin
    Route::get('users/shgAdmin',['uses'=>'UserController@get_shg_user','as'=>'users.get_shg_user']);

    // States
    Route::get('states',['uses'=>'StateController@index','as'=>'states.index']);
    Route::post('states/store',['uses'=>'StateController@store','as'=>'states.store']);
    Route::get('states/{id}/edit',['uses'=>'StateController@edit','as'=>'states.edit']);
    Route::put('states/{id}/update',['uses'=>'StateController@update','as'=>'states.update']);
    Route::get('states/{id}/view',['uses'=>'StateController@show','as'=>'states.view']);

    // IncomeLevels
    Route::get('income-levels',['uses'=>'IncomeLevelController@index','as'=>'income-levels.index']);
    Route::post('income-levels/store',['uses'=>'IncomeLevelController@store','as'=>'income-levels.store']);
    Route::get('income-levels/{id}/edit',['uses'=>'IncomeLevelController@edit','as'=>'income-levels.edit']);
    Route::put('income-levels/{id}/update',['uses'=>'IncomeLevelController@update','as'=>'income-levels.update']);
    Route::get('income-levels/{id}/view',['uses'=>'IncomeLevelController@show','as'=>'income-levels.view']);

    //Loan Schemes
    Route::get('loan-schemes', ['uses'=>'LoanSchemeController@index','as'=>'loan-schemes.index']);
    Route::post('loan-schemes/store',['uses'=>'LoanSchemeController@store','as'=>'loan-schemes.store']);
    Route::get('loan-schemes/{id}/edit',['uses'=>'LoanSchemeController@edit','as'=>'loan-schemes.edit']);
    Route::put('loan-schemes/{id}/update',['uses'=>'LoanSchemeController@update','as'=>'loan-schemes.update']);
    Route::get('loan-schemes/{id}/view',['uses'=>'LoanSchemeController@show','as'=>'loan-schemes.view']);

    //Religion
    Route::get('religions', ['uses'=>'ReligionController@index','as'=>'religions.index']);
    Route::post('religions/store',['uses'=>'ReligionController@store','as'=>'religions.store']);
    Route::get('religions/{id}/edit',['uses'=>'ReligionController@edit','as'=>'religions.edit']);
    Route::put('religions/{id}/update',['uses'=>'ReligionController@update','as'=>'religions.update']);
    Route::get('religions/{id}/view',['uses'=>'ReligionController@show','as'=>'religions.view']);
    //Caste
    Route::get('castes', ['uses'=>'CasteController@index','as'=>'castes.index']);
    Route::post('castes/store',['uses'=>'CasteController@store','as'=>'castes.store']);
    Route::get('castes/{id}/edit',['uses'=>'CasteController@edit','as'=>'castes.edit']);
    Route::put('castes/{id}/update',['uses'=>'CasteController@update','as'=>'castes.update']);
    Route::get('castes/{id}/view',['uses'=>'CasteController@show','as'=>'castes.view']);

    //Literacy Levels
    Route::get('literacy-levels', ['uses'=>'LiteracyLevelController@index','as'=>'literacy-levels.index']);
    Route::post('literacy-levels/store',['uses'=>'LiteracyLevelController@store','as'=>'literacy-levels.store']);
    Route::get('literacy-levels/{id}/edit',['uses'=>'LiteracyLevelController@edit','as'=>'literacy-levels.edit']);
    Route::put('literacy-levels/{id}/update',['uses'=>'LiteracyLevelController@update','as'=>'literacy-levels.update']);
    Route::get('literacy-levels/{id}/view',['uses'=>'LiteracyLevelController@show','as'=>'literacy-levels.view']);


    // Districts
    Route::get('districts',['uses'=>'DistrictController@index','as'=>'districts.index']);
    Route::post('districts/store',['uses'=>'DistrictController@store','as'=>'districts.store']);
    Route::get('districts/{id}/edit',['uses'=>'DistrictController@edit','as'=>'districts.edit']);
    Route::put('districts/{id}/update',['uses'=>'DistrictController@update','as'=>'districts.update']);
    Route::get('districts/{id}/view',['uses'=>'DistrictController@show','as'=>'districts.view']);

    //Get districts of given state id
    Route::get('states/{state_id}/districts',['uses'=>'DistrictController@get_districts','as'=>'states.districts']);

    // Get ALl Sectors for a particular NGO
    Route::get('ngos/{ngo_id}/sectors',['uses'=>'NgoActiveSectorController@getNgoSectors','as'=>'ngos.get_sectors']);

    // Get All Shgs for a particular NGO (Admin Role)
    Route::get('ngos/{ngo_id}/self-help-groups',['uses'=>'SelfHelpGroupController@getShgsForAdmin','as'=>'ngos.get_shgs_for_admin']);

    // Get All Shgs for a particular NGO (NGO Role)
    Route::get('ngos/self-help-groups',['uses'=>'SelfHelpGroupController@getShgsForNgoAdmins','as'=>'ngos.get_shgs_for_ngo_admin']);

    // Get All Women Entrepreneurs for a particular Self Help Group
    Route::get('users/{shg_id}/women-entrepreneurs',['uses'=>'UserController@getEntrepreneursForShg','as'=>'users.get_entrepreneurs_for_shg']);

    // Ngo Sectors
    Route::get('ngo-sectors',['uses'=>'NgoSectorController@index','as'=>'ngo-sectors.index']);
    Route::post('ngo-sectors/store',['uses'=>'NgoSectorController@store','as'=>'ngo-sectors.store']);
    Route::get('ngo-sectors/{id}/edit',['uses'=>'NgoSectorController@edit','as'=>'ngo-sectors.edit']);
    Route::put('ngo-sectors/{id}/update',['uses'=>'NgoSectorController@update','as'=>'ngo-sectors.update']);
    Route::get('ngo-sectors/{id}/view',['uses'=>'NgoSectorController@show','as'=>'ngo-sectors.view']);


    Route::get('ngos',['uses'=>'NgoController@index','as'=>'ngos.index']);
    Route::post('ngos/store',['uses'=>'NgoController@store','as'=>'ngos.store']);
    Route::get('ngos/{id}/edit',['uses'=>'NgoController@edit','as'=>'ngos.edit']);
    Route::put('ngos/{id}/update',['uses'=>'NgoController@update','as'=>'ngos.update']);
    Route::get('ngos/{id}/view',['uses'=>'NgoController@show','as'=>'ngos.view']);

    Route::get('self-help-groups',['uses'=>'SelfHelpGroupController@index','as'=>'self-help-groups.index']);
    Route::post('self-help-groups/store',['uses'=>'SelfHelpGroupController@store','as'=>'self-help-groups.create']);
    Route::get('self-help-groups/{id}/edit',['uses'=>'SelfHelpGroupController@edit','as'=>'self-help-groups.edit']);
    Route::put('self-help-groups/{id}/update',['uses'=>'SelfHelpGroupController@update','as'=>'self-help-groups.update']);

    Route::get('loans',['uses'=>'LoanController@index','as'=>'loans.index']);
    Route::post('loans/store',['uses'=>'LoanController@store','as'=>'loans.store']);
    Route::get('loans/{id}/view',['uses'=>'LoanController@show','as'=>'loans.view']);

    Route::get('loans/{loan_id}/repayments',['uses'=>'RepaymentController@index','as'=>'repayments.index']);
    Route::get('loans/{loan_id}/repayments/{id}/edit',['uses'=>'RepaymentController@edit','as'=>'repayments.edit']);
    Route::put('loans/{loan_id}/repayments/{id}/update',['uses'=>'RepaymentController@update','as'=>'repayments.update']);
    Route::get('loans/{loan_id}/repayments/{id}/view',['uses'=>'RepaymentController@show','as'=>'repayments.view']);


    // Charts Controller
    Route::get('charts/all-literacy-level-loan-count',['uses'=>'ChartController@all_literacy_levels_loan_count','as'=>'all_literacy_levels_loan_count']);
    Route::get('charts/all-income-level-loan-count',['uses'=>'ChartController@all_income_levels_loan_count','as'=>'all_income_levels_loan_count']);
    Route::get('charts/all-caste-loan-count',['uses'=>'ChartController@all_castes_loan_count','as'=>'all_castes_loan_count']);
    Route::get('charts/all-religion-loan-count',['uses'=>'ChartController@all_religions_loan_count','as'=>'all_religions_loan_count']);
    Route::get('charts/all-state-loan-count',['uses'=>'ChartController@all_states_loan_count','as'=>'all_states_loan_count']);

    Route::get('charts/all-literacy-level-loan-amount',['uses'=>'ChartController@all_literacy_levels_loan_amount','as'=>'all_literacy_levels_loan_amount']);
    Route::get('charts/all-income-level-loan-amount',['uses'=>'ChartController@all_income_levels_loan_amount','as'=>'all_income_levels_loan_amount']);
    Route::get('charts/all-caste-loan-amount',['uses'=>'ChartController@all_castes_loan_amount','as'=>'all_castes_loan_amount']);
    Route::get('charts/all-religion-loan-amount',['uses'=>'ChartController@all_religions_loan_amount','as'=>'all_religions_loan_amount']);
    Route::get('charts/all-state-loan-amount',['uses'=>'ChartController@all_states_loan_amount','as'=>'all_states_loan_amount']);

    Route::get('charts/all-literacy-level-repayment-status',['uses'=>'ChartController@all_literacy_levels_repayment_status','as'=>'all_literacy_levels_repayment_status']);
    Route::get('charts/all-income-level-repayment-status',['uses'=>'ChartController@all_income_levels_repayment_status','as'=>'all_income_levels_repayment_status']);
    Route::get('charts/all-caste-repayment-status',['uses'=>'ChartController@all_castes_repayment_status','as'=>'all_castes_repayment_status']);
    Route::get('charts/all-religion-repayment-status',['uses'=>'ChartController@all_religions_repayment_status','as'=>'all_religions_repayment_status']);
    Route::get('charts/all-state-repayment-status',['uses'=>'ChartController@all_states_repayment_status','as'=>'all_religions_repayment_status']);


    // For Specific State
    Route::get('charts/{state_id}/all-literacy-level-loan-count',['uses'=>'ChartForOneStateController@all_literacy_levels_loan_count_state_wise','as'=>'all_literacy_levels_loan_count_state_wise']);
    Route::get('charts/{state_id}/all-income-level-loan-count',['uses'=>'ChartForOneStateController@all_income_levels_loan_count_state_wise','as'=>'all_income_levels_loan_count_state_wise']);
    Route::get('charts/{state_id}/all-caste-loan-count',['uses'=>'ChartForOneStateController@all_castes_loan_count_state_wise','as'=>'all_castes_loan_count_state_wise']);
    Route::get('charts/{state_id}/all-religion-loan-count',['uses'=>'ChartForOneStateController@all_religions_loan_count_state_wise','as'=>'all_religions_loan_count_state_wise']);

    Route::get('charts/{state_id}/all-literacy-level-loan-amount',['uses'=>'ChartForOneStateController@all_literacy_levels_loan_amount_state_wise','as'=>'all_literacy_levels_loan_amount_state_wise']);
    Route::get('charts/{state_id}/all-income-level-loan-amount',['uses'=>'ChartForOneStateController@all_income_levels_loan_amount_state_wise','as'=>'all_income_levels_loan_amount_state_wise']);
    Route::get('charts/{state_id}/all-caste-loan-amount',['uses'=>'ChartForOneStateController@all_castes_loan_amount_state_wise','as'=>'all_castes_loan_amount_state_wise']);
    Route::get('charts/{state_id}/all-religion-loan-amount',['uses'=>'ChartForOneStateController@all_religions_loan_amount_state_wise','as'=>'all_religions_loan_amount_state_wise']);

    Route::get('charts/{state_id}/all-literacy-level-repayment-status',['uses'=>'ChartForOneStateController@all_literacy_levels_repayment_status_state_wise','as'=>'all_literacy_levels_repayment_status_state_wise']);
    Route::get('charts/{state_id}/all-income-level-repayment-status',['uses'=>'ChartForOneStateController@all_income_levels_repayment_status_state_wise','as'=>'all_income_levels_repayment_status_state_wise']);
    Route::get('charts/{state_id}/all-caste-repayment-status',['uses'=>'ChartForOneStateController@all_castes_repayment_status_state_wise','as'=>'all_castes_repayment_status_state_wise']);
    Route::get('charts/{state_id}/all-religion-repayment-status',['uses'=>'ChartForOneStateController@all_religions_repayment_status_state_wise','as'=>'all_religions_repayment_status_state_wise']);

    //For Specific District
    Route::get('charts/{district_id}/all-income-level-loan-count',['uses'=>'ChartForOneDistrictController@all_income_levels_loan_count_district_wise','as'=>'all_income_levels_loan_count_district_wise']);
    Route::get('charts/{district_id}/all-caste-loan-count',['uses'=>'ChartForOneDistrictController@all_castes_loan_count_district_wise','as'=>'all_castes_loan_count_district_wise']);
    Route::get('charts/{district_id}/all-literacy-level-loan-count',['uses'=>'ChartForOneDistrictController@all_literacy_levels_loan_count_district_wise','as'=>'all_literacy_levels_loan_count_district_wise']);
    Route::get('charts/{district_id}/all-religion-loan-count',['uses'=>'ChartForOneDistrictController@all_religions_loan_count_district_wise','as'=>'all_religions_loan_count_district_wise']);

    Route::get('charts/{district_id}/all-literacy-level-loan-amount',['uses'=>'ChartForOneDistrictController@all_literacy_levels_loan_amount_district_wise','as'=>'all_literacy_levels_loan_amount_district_wise']);
    Route::get('charts/{district_id}/all-income-level-loan-amount',['uses'=>'ChartForOneDistrictController@all_income_levels_loan_amount_district_wise','as'=>'all_income_levels_loan_amount_district_wise']);
    Route::get('charts/{district_id}/all-caste-loan-amount',['uses'=>'ChartForOneDistrictController@all_castes_loan_amount_district_wise','as'=>'all_castes_loan_amount_district_wise']);
    Route::get('charts/{district_id}/all-religion-loan-amount',['uses'=>'ChartForOneDistrictController@all_religions_loan_amount_district_wise','as'=>'all_religions_loan_amount_district_wise']);

    Route::get('charts/{district_id}/all-literacy-level-repayment-status',['uses'=>'ChartForOneDistrictController@all_literacy_levels_repayment_status_district_wise','as'=>'all_literacy_levels_repayment_status_district_wise']);
    Route::get('charts/{district_id}/all-income-level-repayment-status',['uses'=>'ChartForOneDistrictController@all_income_levels_repayment_status_district_wise','as'=>'all_income_levels_repayment_status_district_wise']);
    Route::get('charts/{district_id}/all-caste-repayment-status',['uses'=>'ChartForOneDistrictController@all_castes_repayment_status_district_wise','as'=>'all_castes_repayment_status_district_wise']);
    Route::get('charts/{district_id}/all-religion-repayment-status',['uses'=>'ChartForOneDistrictController@all_religions_repayment_status_district_wise','as'=>'all_religions_repayment_status_district_wise']);

    //For Specific Caste
    Route::get('charts/{caste_id}/all-income-level-loan-count',['uses'=>'ChartForOneCasteController@all_income_levels_loan_count_caste_wise','as'=>'all_income_levels_loan_count_caste_wise']);
    Route::get('charts/{caste_id}/all-state-loan-count',['uses'=>'ChartForOneCasteController@all_states_loan_count_caste_wise','as'=>'all_states_loan_count_caste_wise']);
    Route::get('charts/{caste_id}/all-literacy-level-loan-count',['uses'=>'ChartForOneCasteController@all_literacy_levels_loan_count_caste_wise','as'=>'all_literacy_levels_loan_count_caste_wise']);
    Route::get('charts/{caste_id}/all-religion-loan-count',['uses'=>'ChartForOneCasteController@all_religions_loan_count_caste_wise','as'=>'all_religions_loan_count_caste_wise']);

    Route::get('charts/{caste_id}/all-literacy-level-loan-amount',['uses'=>'ChartForOneCasteController@all_literacy_levels_loan_amount_caste_wise','as'=>'all_literacy_levels_loan_amount_caste_wise']);
    Route::get('charts/{caste_id}/all-income-level-loan-amount',['uses'=>'ChartForOneCasteController@all_income_levels_loan_amount_caste_wise','as'=>'all_income_levels_loan_amount_caste_wise']);
    Route::get('charts/{caste_id}/all-state-loan-amount',['uses'=>'ChartForOneCasteController@all_states_loan_amount_caste_wise','as'=>'all_states_loan_amount_caste_wise']);
    Route::get('charts/{caste_id}/all-religion-loan-amount',['uses'=>'ChartForOneCasteController@all_religions_loan_amount_caste_wise','as'=>'all_religions_loan_amount_caste_wise']);

    Route::get('charts/{caste_id}/all-literacy-level-repayment-status',['uses'=>'ChartForOneCasteController@all_literacy_levels_repayment_status_caste_wise','as'=>'all_literacy_levels_repayment_status_caste_wise']);
    Route::get('charts/{caste_id}/all-income-level-repayment-status',['uses'=>'ChartForOneCasteController@all_income_levels_repayment_status_caste_wise','as'=>'all_income_levels_repayment_status_caste_wise']);
    Route::get('charts/{caste_id}/all-state-repayment-status',['uses'=>'ChartForOneCasteController@all_states_repayment_status_caste_wise','as'=>'all_states_repayment_status_caste_wise']);
    Route::get('charts/{caste_id}/all-religion-repayment-status',['uses'=>'ChartForOneCasteController@all_religions_repayment_status_caste_wise','as'=>'all_religions_repayment_status_caste_wise']);

    // For Specific Literacy Level
    Route::get('charts/{literacy_level_id}/all-state-loan-count',['uses'=>'ChartForOneLiteracyLevelController@all_states_loan_count_literacy_level_wise','as'=>'all_states_loan_count_literacy_level_wise']);
    Route::get('charts/{literacy_level_id}/all-income-level-loan-count',['uses'=>'ChartForOneLiteracyLevelController@all_income_levels_loan_count_literacy_level_wise','as'=>'all_income_levels_loan_count_literacy_level_wise']);
    Route::get('charts/{literacy_level_id}/all-caste-loan-count',['uses'=>'ChartForOneLiteracyLevelController@all_castes_loan_count_literacy_level_wise','as'=>'all_castes_loan_count_literacy_level_wise']);
    Route::get('charts/{literacy_level_id}/all-religion-loan-count',['uses'=>'ChartForOneLiteracyLevelController@all_religions_loan_count_literacy_level_wise','as'=>'all_religions_loan_count_literacy_level_wise']);

    Route::get('charts/{literacy_level_id}/all-state-loan-amount',['uses'=>'ChartForOneLiteracyLevelController@all_states_loan_amount_literacy_level_wise','as'=>'all_states_loan_amount_literacy_level_wise']);
    Route::get('charts/{literacy_level_id}/all-income-level-loan-amount',['uses'=>'ChartForOneLiteracyLevelController@all_income_levels_loan_amount_literacy_level_wise','as'=>'all_income_levels_loan_amount_literacy_level_wise']);
    Route::get('charts/{literacy_level_id}/all-caste-loan-amount',['uses'=>'ChartForOneLiteracyLevelController@all_castes_loan_amount_literacy_level_wise','as'=>'all_castes_loan_amount_literacy_level_wise']);
    Route::get('charts/{literacy_level_id}/all-religion-loan-amount',['uses'=>'ChartForOneLiteracyLevelController@all_religions_loan_amount_literacy_level_wise','as'=>'all_religions_loan_amount_literacy_level_wise']);

    Route::get('charts/{literacy_level_id}/all-state-repayment-status',['uses'=>'ChartForOneLiteracyLevelController@all_states_repayment_status_literacy_level_wise','as'=>'all_states_repayment_status_literacy_level_wise']);
    Route::get('charts/{literacy_level_id}/all-income-level-repayment-status',['uses'=>'ChartForOneLiteracyLevelController@all_income_levels_repayment_status_literacy_level_wise','as'=>'all_income_levels_repayment_status_literacy_level_wise']);
    Route::get('charts/{literacy_level_id}/all-caste-repayment-status',['uses'=>'ChartForOneLiteracyLevelController@all_castes_repayment_status_literacy_level_wise','as'=>'all_castes_repayment_status_literacy_level_wise']);
    Route::get('charts/{literacy_level_id}/all-religion-repayment-status',['uses'=>'ChartForOneLiteracyLevelController@all_religions_repayment_status_literacy_level_wise','as'=>'all_religions_repayment_status_literacy_level_wise']);

   // For Specific Religion
    Route::get('charts/{religion_id}/all-literacy-level-loan-count',['uses'=>'ChartForOneReligionController@all_literacy_levels_loan_count_religion_wise','as'=>'all_literacy_levels_loan_count_religion_wise']);
    Route::get('charts/{religion_id}/all-income-level-loan-count',['uses'=>'ChartForOneReligionController@all_income_levels_loan_count_religion_wise','as'=>'all_income_levels_loan_count_religion_wise']);
    Route::get('charts/{religion_id}/all-caste-loan-count',['uses'=>'ChartForOneReligionController@all_castes_loan_count_religion_wise','as'=>'all_castes_loan_count_religion_wise']);
    Route::get('charts/{religion_id}/all-state-loan-count',['uses'=>'ChartForOneReligionController@all_states_loan_count_religion_wise','as'=>'all_states_loan_count_religion_wise']);

    Route::get('charts/{religion_id}/all-literacy-level-loan-amount',['uses'=>'ChartForOneReligionController@all_literacy_levels_loan_amount_religion_wise','as'=>'all_literacy_levels_loan_amount_religion_wise']);
    Route::get('charts/{religion_id}/all-income-level-loan-amount',['uses'=>'ChartForOneReligionController@all_income_levels_loan_amount_religion_wise','as'=>'all_income_levels_loan_amount_religion_wise']);
    Route::get('charts/{religion_id}/all-caste-loan-amount',['uses'=>'ChartForOneReligionController@all_castes_loan_amount_religion_wise','as'=>'all_castes_loan_amount_religion_wise']);
    Route::get('charts/{religion_id}/all-state-loan-amount',['uses'=>'ChartForOneReligionController@all_states_loan_amount_religion_wise','as'=>'all_states_loan_amount_religion_wise']);

    Route::get('charts/{religion_id}/all-literacy-level-repayment-status',['uses'=>'ChartForOneReligionController@all_literacy_levels_repayment_status_religion_wise','as'=>'all_literacy_levels_repayment_status_religion_wise']);
    Route::get('charts/{religion_id}/all-income-level-repayment-status',['uses'=>'ChartForOneReligionController@all_income_levels_repayment_status_religion_wise','as'=>'all_income_levels_repayment_status_religion_wise']);
    Route::get('charts/{religion_id}/all-caste-repayment-status',['uses'=>'ChartForOneReligionController@all_castes_repayment_status_religion_wise','as'=>'all_castes_repayment_status_religion_wise']);
    Route::get('charts/{religion_id}/all-state-repayment-status',['uses'=>'ChartForOneReligionController@all_states_repayment_status_religion_wise','as'=>'all_states_repayment_status_religion_wise']);

    // For Specific Income Level
    Route::get('charts/{income_level_id}/all-literacy-level-loan-count',['uses'=>'ChartForOneIncomeLevelController@all_literacy_levels_loan_count_income_level_wise','as'=>'all_literacy_levels_loan_count_income_level_wise']);
    Route::get('charts/{income_level_id}/all-state-loan-count',['uses'=>'ChartForOneIncomeLevelController@all_states_loan_count_income_level_wise','as'=>'all_states_loan_count_income_level_wise']);
    Route::get('charts/{income_level_id}/all-caste-loan-count',['uses'=>'ChartForOneIncomeLevelController@all_castes_loan_count_income_level_wise','as'=>'all_castes_loan_count_income_level_wise']);
    Route::get('charts/{income_level_id}/all-religion-loan-count',['uses'=>'ChartForOneIncomeLevelController@all_religions_loan_count_income_level_wise','as'=>'all_religions_loan_count_income_level_wise']);

    Route::get('charts/{income_level_id}/all-literacy-level-loan-amount',['uses'=>'ChartForOneIncomeLevelController@all_literacy_levels_loan_amount_income_level_wise','as'=>'all_literacy_levels_loan_amount_income_level_wise']);
    Route::get('charts/{income_level_id}/all-state-loan-amount',['uses'=>'ChartForOneIncomeLevelController@all_states_loan_amount_income_level_wise','as'=>'all_states_loan_amount_income_level_wise']);
    Route::get('charts/{income_level_id}/all-caste-loan-amount',['uses'=>'ChartForOneIncomeLevelController@all_castes_loan_amount_income_level_wise','as'=>'all_castes_loan_amount_income_level_wise']);
    Route::get('charts/{income_level_id}/all-religion-loan-amount',['uses'=>'ChartForOneIncomeLevelController@all_religions_loan_amount_income_level_wise','as'=>'all_religions_loan_amount_income_level_wise']);

    Route::get('charts/{income_level_id}/all-literacy-level-repayment-status',['uses'=>'ChartForOneIncomeLevelController@all_literacy_levels_repayment_status_income_level_wise','as'=>'all_literacy_levels_repayment_status_income_level_wise']);
    Route::get('charts/{income_level_id}/all-state-repayment-status',['uses'=>'ChartForOneIncomeLevelController@all_states_repayment_status_income_level_wise','as'=>'all_states_repayment_status_income_level_wise']);
    Route::get('charts/{income_level_id}/all-caste-repayment-status',['uses'=>'ChartForOneIncomeLevelController@all_castes_repayment_status_income_level_wise','as'=>'all_castes_repayment_status_income_level_wise']);
    Route::get('charts/{income_level_id}/all-religion-repayment-status',['uses'=>'ChartForOneIncomeLevelController@all_religions_repayment_status_income_level_wise','as'=>'all_religions_repayment_status_income_level_wise']);


});
