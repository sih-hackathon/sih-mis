<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Caste extends Model
{
    protected $table = 'castes';
    protected $fillable = ['title','description'];
}
