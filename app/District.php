<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
  protected $table = 'districts';
  protected $fillable = ['title','description','state_id'];

  public function districts_get_states()
  {
    return $this->belongsTo('App\State','state_id');
  }

  public function ngo_get_districts()
  {
    return $this->hasMany('App\Ngo','district_id');
  }
}
