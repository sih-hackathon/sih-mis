<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NgoSector extends Model
{
    protected $table = 'ngo_sectors';
    protected $fillable = ['id','title','description'];
}
