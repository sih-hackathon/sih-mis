<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NgoActiveSector extends Model
{
  protected $table = 'ngo_active_sectors';
  protected $fillable = ['ngo_id','sector_id'];

  public function ngo_sector()
  {
    return $this->belongsTo('App\Ngo','ngo_id');
  }
}
