<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SelfHelpGroup extends Model
{
  protected $table = 'self_help_groups';
  protected $fillable = ['name','ngo_id','state_id','district_id','admin_id','address'];

  public function ngo_self_help_group()
  {
    return $this->belongsTo('App\Ngo','ngo_id');
  }
}
