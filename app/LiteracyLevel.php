<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiteracyLevel extends Model
{
  protected $table = 'literacy_levels';
  protected $fillable = ['title','description'];
}
