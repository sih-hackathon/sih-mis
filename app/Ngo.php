<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ngo extends Model
{
  protected $table = 'ngos';
  protected $fillable = ['name','state_id','district_id','admin_id','darpan_unique_id','achievements'];

  public function ngo_get_states()
  {
    return $this->belongsTo('App\State','state_id');
  }

  public function ngo_get_districts()
  {
    return $this->belongsTo('App\District','district_id');
  }

  public function ngo_get_admins()
  {
    return $this->belongsTo('App\User','admin_id');
  }

  public function ngo_sector()
  {
    return $this->hasMany('App\NgoActiveSector','ngo_id');
  }

  public function ngo_self_help_group()
  {
    return $this->hasMany('App\SelfHelpGroup','ngo_id');
  }

  public function repayment_get_ngo()
  {
    return $this->hasMany('App\Repayment','ngo_id');
  }
}
