<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\State;
use App\Loan;
use App\Ngo;
use App\Repayment;
use DB;
use App\UserDetail;
use App\Religion;
use App\IncomeLevel;
use App\Caste;
use App\LiteracyLevel;
use App\LoanScheme;
class ChartController extends Controller
{
  public function get_ngo_performance_state_wise($state_id)
  {
    $ngos = Ngo::where('state_id',$state_id)->pluck('id');

    // Get No. Of Installments paid on Time
    $onTime = Repayment::whereIn('ngo_id',$ngos)->where('repayment_status',3)->get()->map(function($item, $index) {
      return [
        "ngo" => $item->repayment_get_ngo["name"],
      ];
    })->countBy()->all();
    $labels = array_keys($onTime);
    $onTimeData = array_values($onTime);

    // Get No. Of Installments paid before Time
    $beforeTime = Repayment::whereIn('ngo_id',$ngos)->where('repayment_status',4)->get()->map(function($item, $index) {
      return [
        "ngo" => $item->repayment_get_ngo["name"],
      ];
    })->countBy()->all();
    $beforeTimeData = array_values($beforeTime);

    // Get No. Of Installments paid late
    $paidLate = Repayment::whereIn('ngo_id',$ngos)->where('repayment_status',2)->get()->map(function($item, $index) {
      return [
        "ngo" => $item->repayment_get_ngo["name"],
      ];
    })->countBy()->all();
    $paidLateData = array_values($paidLate);

    // Get No. of Installments Not Paid At all
    $notPaid = Repayment::whereIn('ngo_id',$ngos)->where('repayment_status',1)->get()->map(function($item, $index) {
      return [
        "ngo" => $item->repayment_get_ngo["name"],
      ];
    })->countBy()->all();
    $notPaidData = array_values($notPaid);

    return response()->json([
      'labels' =>$labels,
      'onTimeData' => $onTimeData,
      'beforeTimeData' => $beforeTimeData,
      'paidLateData' => $paidLateData,
      'notPaidData' => $notPaidData,
    ]);
  }

  public function get_ngo_performance_district_wise($district_id)
  {
    $ngos = Ngo::where('district_id',$district_id)->pluck('id');

    // Get No. Of Installments paid on Time
    $onTime = Repayment::whereIn('ngo_id',$ngos)->where('repayment_status',3)->get()->map(function($item, $index) {
      return [
        "ngo" => $item->repayment_get_ngo["name"],
      ];
    })->countBy()->all();
    $labels = array_keys($onTime);
    $onTimeData = array_values($onTime);

    // Get No. Of Installments paid before Time
    $beforeTime = Repayment::whereIn('ngo_id',$ngos)->where('repayment_status',4)->get()->map(function($item, $index) {
      return [
        "ngo" => $item->repayment_get_ngo["name"],
      ];
    })->countBy()->all();
    $beforeTimeData = array_values($beforeTime);

    // Get No. Of Installments paid late
    $paidLate = Repayment::whereIn('ngo_id',$ngos)->where('repayment_status',2)->get()->map(function($item, $index) {
      return [
        "ngo" => $item->repayment_get_ngo["name"],
      ];
    })->countBy()->all();
    $paidLateData = array_values($paidLate);

    // Get No. of Installments Not Paid At all
    $notPaid = Repayment::whereIn('ngo_id',$ngos)->where('repayment_status',1)->get()->map(function($item, $index) {
      return [
        "ngo" => $item->repayment_get_ngo["name"],
      ];
    })->countBy()->all();
    $notPaidData = array_values($notPaid);

    return response()->json([
      'labels' =>$labels,
      'onTimeData' => $onTimeData,
      'beforeTimeData' => $beforeTimeData,
      'paidLateData' => $paidLateData,
      'notPaidData' => $notPaidData,
    ]);
  }


  public function get_loan_scheme_performance_state_wise($state_id)
  {
    $stateUsers = UserDetail::where([['state_id',$state_id]])->pluck('user_id');
    $firstLoanIds = Loan::whereIn('user_id',$stateUsers)->where('loan_scheme_id',1)->pluck('id');
    $firstLoanScheme = Repayment::whereIn('loan_id',$firstLoanIds)->get()->map(function($item, $index) {
      return [
        "repayment_status" => $item["repayment_status"],
      ];
    })->countBy()->all();
    //return $firstLoanScheme;

    $secondLoanIds = Loan::whereIn('user_id',$stateUsers)->where('loan_scheme_id',2)->pluck('id');
    $secondLoanScheme = Repayment::whereIn('loan_id',$secondLoanIds)->get()->map(function($item, $index) {
      return [
        "repayment_status" => $item["repayment_status"],
      ];
    })->countBy()->all();
    //return $secondLoanScheme;

    $thirdLoanIds = Loan::whereIn('user_id',$stateUsers)->where('loan_scheme_id',3)->pluck('id');
    $thirdLoanScheme = Repayment::whereIn('loan_id',$thirdLoanIds)->get()->map(function($item, $index) {
      return [
        "repayment_status" => $item["repayment_status"],
      ];
    })->countBy()->all();
    //return $thirdLoanScheme;

    $fourthLoanIds = Loan::whereIn('user_id',$stateUsers)->where('loan_scheme_id',4)->pluck('id');
    $fourthLoanScheme = Repayment::whereIn('loan_id',$fourthLoanIds)->get()->map(function($item, $index) {
      return [
        "repayment_status" => $item["repayment_status"],
      ];
    })->countBy()->all();
    //return $fourthLoanScheme;

    $fifthLoanIds = Loan::whereIn('user_id',$stateUsers)->where('loan_scheme_id',5)->pluck('id');
    $fifthLoanScheme = Repayment::whereIn('loan_id',$fifthLoanIds)->get()->map(function($item, $index) {
      return [
        "repayment_status" => $item["repayment_status"],
      ];
    })->countBy()->all();
    //return $fifthLoanScheme;

    $sixthLoanIds = Loan::whereIn('user_id',$stateUsers)->where('loan_scheme_id',6)->pluck('id');
    $sixthLoanScheme = Repayment::whereIn('loan_id',$sixthLoanIds)->get()->map(function($item, $index) {
      return [
        "repayment_status" => $item["repayment_status"],
      ];
    })->countBy()->all();
    //return $sixthLoanScheme;

    $new_array = array($firstLoanScheme,$secondLoanScheme,$thirdLoanScheme,$fourthLoanScheme,$fifthLoanScheme,$sixthLoanScheme);
    $on_time = array_column($new_array, '3');
    $before_time = array_column($new_array, '4');
    $late = array_column($new_array, '2');
    $not_paid = array_column($new_array, '1');
    $labels = LoanScheme::all()->pluck('title')->toArray();
    return response()->json([
      'labels'=> $labels,
      'onTimeData'=> $on_time,
      'beforeTimeData' =>$before_time,
      'paidLateData'=> $late,
      'notPaidData'=> $not_paid,
    ]);
  }

  public function all_literacy_levels_loan_count()
  {
    $literacy_level = LiteracyLevel::all();
    $labels = $literacy_level->pluck('title');
    $count = $literacy_level->count();
    $data = [];
    /*$users = UserDetail::where('literacy_level_id',8)->pluck('user_id');
    $loans = Loan::whereIn('user_id',$users)->count();
    return $loans;*/
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where('literacy_level_id',$i+1)->pluck('user_id');
      $loans = Loan::whereIn('user_id',$users)->count();
      array_push($data,$loans);
    }
    return response()->json([
      'labels' => $labels,
      'data'=>$data,
    ]);
  }

  public function all_income_levels_loan_count()
  {
    $income_level = IncomeLevel::all();
    $labels = $income_level->pluck('title');
    $count = $income_level->count();
    $data = [];
    /*$users = UserDetail::where('literacy_level_id',8)->pluck('user_id');
    $loans = Loan::whereIn('user_id',$users)->count();
    return $loans;*/
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where('income_level_id',$i+1)->pluck('user_id');
      $loans = Loan::whereIn('user_id',$users)->count();
      array_push($data,$loans);
    }
    return response()->json([
      'labels' => $labels,
      'data'=>$data,
    ]);
  }

  public function all_castes_loan_count()
  {
    $caste = Caste::all();
    $labels = $caste->pluck('title');
    $count = $caste->count();
    $data = [];
    /*$users = UserDetail::where('literacy_level_id',8)->pluck('user_id');
    $loans = Loan::whereIn('user_id',$users)->count();
    return $loans;*/
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where('caste_id',$i+1)->pluck('user_id');
      $loans = Loan::whereIn('user_id',$users)->count();
      array_push($data,$loans);
    }
    return response()->json([
      'labels' => $labels,
      'data'=>$data,
    ]);
  }

  public function all_religions_loan_count()
  {
    $religions = Religion::all();
    $labels = $religions->pluck('title');
    $count = $religions->count();
    $data = [];
    /*$users = UserDetail::where('literacy_level_id',8)->pluck('user_id');
    $loans = Loan::whereIn('user_id',$users)->count();
    return $loans;*/
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where('religion_id',$i+1)->pluck('user_id');
      $loans = Loan::whereIn('user_id',$users)->count();
      array_push($data,$loans);
    }
    return response()->json([
      'labels' => $labels,
      'data'=>$data,
    ]);
  }
  public function all_states_loan_count()
  {
    $states = State::all();
    $labels = $states->pluck('title')->toArray();
    $count = $states->count();
    $data = [];
    /*$users = UserDetail::where('literacy_level_id',8)->pluck('user_id');
    $loans = Loan::whereIn('user_id',$users)->count();
    return $loans;*/
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where('state_id',$i+1)->pluck('user_id');
      $loans = Loan::whereIn('user_id',$users)->count();
      array_push($data,$loans);
    }
    $loan_data = array_values($data);
    //var_dump($labels);
    $state_data = array_values($labels);
    //var_dump($data);
    return response()->json([
      'labels' => $labels,
      'data'=>$data,
    ]);
  }

  public function all_literacy_levels_loan_amount()
  {
    $literacy_level = LiteracyLevel::all();
    $labels = $literacy_level->pluck('title');
    $count = $literacy_level->count();
    $data = [];
    /*$users = UserDetail::where('literacy_level_id',8)->pluck('user_id');
    $loans = Loan::whereIn('user_id',$users)->sum('loan_amount');
    return $loans;*/
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where('literacy_level_id',$i+1)->pluck('user_id');
      $loans = Loan::whereIn('user_id',$users)->sum('loan_amount');
      array_push($data,$loans);
    }
    return response()->json([
      'labels' => $labels,
      'data'=>$data,
    ]);
  }

  public function all_income_levels_loan_amount()
  {
    $income_level = IncomeLevel::all();
    $labels = $income_level->pluck('title');
    $count = $income_level->count();
    $data = [];
    /*$users = UserDetail::where('literacy_level_id',8)->pluck('user_id');
    $loans = Loan::whereIn('user_id',$users)->count();
    return $loans;*/
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where('income_level_id',$i+1)->pluck('user_id');
      $loans = Loan::whereIn('user_id',$users)->sum('loan_amount');
      array_push($data,$loans);
    }
    return response()->json([
      'labels' => $labels,
      'data'=>$data,
    ]);
  }

  public function all_castes_loan_amount()
  {
    $caste = Caste::all();
    $labels = $caste->pluck('title');
    $count = $caste->count();
    $data = [];
    /*$users = UserDetail::where('literacy_level_id',8)->pluck('user_id');
    $loans = Loan::whereIn('user_id',$users)->count();
    return $loans;*/
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where('caste_id',$i+1)->pluck('user_id');
      $loans = Loan::whereIn('user_id',$users)->sum('loan_amount');
      array_push($data,$loans);
    }
    return response()->json([
      'labels' => $labels,
      'data'=>$data,
    ]);
  }

  public function all_religions_loan_amount()
  {
    $religions = Religion::all();
    $labels = $religions->pluck('title');
    $count = $religions->count();
    $data = [];
    /*$users = UserDetail::where('literacy_level_id',8)->pluck('user_id');
    $loans = Loan::whereIn('user_id',$users)->count();
    return $loans;*/
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where('religion_id',$i+1)->pluck('user_id');
      $loans = Loan::whereIn('user_id',$users)->sum('loan_amount');
      array_push($data,$loans);
    }
    return response()->json([
      'labels' => $labels,
      'data'=>$data,
    ]);
  }
  public function all_states_loan_amount()
  {
    $states = State::all();
    $labels = $states->pluck('title')->toArray();
    $count = $states->count();
    $data = [];
    /*$users = UserDetail::where('literacy_level_id',8)->pluck('user_id');
    $loans = Loan::whereIn('user_id',$users)->count();
    return $loans;*/
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where('state_id',$i+1)->pluck('user_id');
      $loans = Loan::whereIn('user_id',$users)->sum('loan_amount');
      array_push($data,$loans);
    }
    $loan_data = array_values($data);
    $state_data = array_values($labels);
    //var_dump($data);
    return response()->json([
      'labels' => $labels,
      'data'=>$data,
    ]);
  }

  public function all_literacy_levels_repayment_status()
  {
    $literacy_level = LiteracyLevel::all();
    $labels = $literacy_level->pluck('title');
    $count = $literacy_level->count();
    $beforeTimeData = [];
    $onTimeData = [];
    $paidLateData = [];
    $notPaidData = [];
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where('literacy_level_id',$i+1)->pluck('user_id');
      //$loans = ;
      $beforeTime = Repayment::whereIn('user_id',$users)->where('repayment_status',4)->count();
      $onTime = Repayment::whereIn('user_id',$users)->where('repayment_status',3)->count();
      $paidLate = Repayment::whereIn('user_id',$users)->where('repayment_status',2)->count();
      $notPaid = Repayment::whereIn('user_id',$users)->where('repayment_status',1)->count();
      array_push($beforeTimeData,$beforeTime);
      array_push($onTimeData,$onTime);
      array_push($paidLateData,$paidLate);
      array_push($notPaidData,$notPaid);
    }
    return response()->json([
      'labels'=> $labels,
      'onTimeData' => $onTimeData,
      'beforeTimeData' => $beforeTimeData,
      'paidLateData' => $paidLateData,
      'notPaidData' => $notPaidData,
    ]);
  }

  public function all_income_levels_repayment_status()
  {
    $income_level = IncomeLevel::all();
    $labels = $income_level->pluck('title');
    $count = $income_level->count();
    $beforeTimeData = [];
    $onTimeData = [];
    $paidLateData = [];
    $notPaidData = [];
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where('income_level_id',$i+1)->pluck('user_id');
      //$loans = ;
      $beforeTime = Repayment::whereIn('user_id',$users)->where('repayment_status',4)->count();
      $onTime = Repayment::whereIn('user_id',$users)->where('repayment_status',3)->count();
      $paidLate = Repayment::whereIn('user_id',$users)->where('repayment_status',2)->count();
      $notPaid = Repayment::whereIn('user_id',$users)->where('repayment_status',1)->count();
      array_push($beforeTimeData,$beforeTime);
      array_push($onTimeData,$onTime);
      array_push($paidLateData,$paidLate);
      array_push($notPaidData,$notPaid);
    }
    return response()->json([
      'labels'=> $labels,
      'onTimeData' => $onTimeData,
      'beforeTimeData' => $beforeTimeData,
      'paidLateData' => $paidLateData,
      'notPaidData' => $notPaidData,
    ]);
  }

  public function all_castes_repayment_status()
  {
    $caste = Caste::all();
    $labels = $caste->pluck('title');
    $count = $caste->count();
    $beforeTimeData = [];
    $onTimeData = [];
    $paidLateData = [];
    $notPaidData = [];
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where('caste_id',$i+1)->pluck('user_id');
      //$loans = ;
      $beforeTime = Repayment::whereIn('user_id',$users)->where('repayment_status',4)->count();
      $onTime = Repayment::whereIn('user_id',$users)->where('repayment_status',3)->count();
      $paidLate = Repayment::whereIn('user_id',$users)->where('repayment_status',2)->count();
      $notPaid = Repayment::whereIn('user_id',$users)->where('repayment_status',1)->count();
      array_push($beforeTimeData,$beforeTime);
      array_push($onTimeData,$onTime);
      array_push($paidLateData,$paidLate);
      array_push($notPaidData,$notPaid);
    }
    return response()->json([
      'labels'=> $labels,
      'onTimeData' => $onTimeData,
      'beforeTimeData' => $beforeTimeData,
      'paidLateData' => $paidLateData,
      'notPaidData' => $notPaidData,
    ]);
  }

  public function all_religions_repayment_status()
  {
    $religions = Religion::all();
    $labels = $religions->pluck('title');
    $count = $caste->count();
    $beforeTimeData = [];
    $onTimeData = [];
    $paidLateData = [];
    $notPaidData = [];
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where('religion_id',$i+1)->pluck('user_id');
      //$loans = ;
      $beforeTime = Repayment::whereIn('user_id',$users)->where('repayment_status',4)->count();
      $onTime = Repayment::whereIn('user_id',$users)->where('repayment_status',3)->count();
      $paidLate = Repayment::whereIn('user_id',$users)->where('repayment_status',2)->count();
      $notPaid = Repayment::whereIn('user_id',$users)->where('repayment_status',1)->count();
      array_push($beforeTimeData,$beforeTime);
      array_push($onTimeData,$onTime);
      array_push($paidLateData,$paidLate);
      array_push($notPaidData,$notPaid);
    }
    return response()->json([
      'labels'=> $labels,
      'onTimeData' => $onTimeData,
      'beforeTimeData' => $beforeTimeData,
      'paidLateData' => $paidLateData,
      'notPaidData' => $notPaidData,
    ]);
  }

  public function all_states_repayment_status()
  {
    $states = State::all();
    $labels = $states->pluck('title')->toArray();
    $count = $states->count();
    $beforeTimeData = [];
    $onTimeData = [];
    $paidLateData = [];
    $notPaidData = [];
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where('state_id',$i+1)->pluck('user_id');
      //$loans = ;
      $beforeTime = Repayment::whereIn('user_id',$users)->where('repayment_status',4)->count();
      $onTime = Repayment::whereIn('user_id',$users)->where('repayment_status',3)->count();
      $paidLate = Repayment::whereIn('user_id',$users)->where('repayment_status',2)->count();
      $notPaid = Repayment::whereIn('user_id',$users)->where('repayment_status',1)->count();
      array_push($beforeTimeData,$beforeTime);
      array_push($onTimeData,$onTime);
      array_push($paidLateData,$paidLate);
      array_push($notPaidData,$notPaid);
    }
    return response()->json([
      'labels'=> $labels,
      'onTimeData' => $onTimeData,
      'beforeTimeData' => $beforeTimeData,
      'paidLateData' => $paidLateData,
      'notPaidData' => $notPaidData,
    ]);
  }

}
