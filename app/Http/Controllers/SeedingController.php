<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ngo;
use App\State;
use App\District;
use App\SelfHelpGroup;
use App\UserDetail;
use App\Loan;
use App\LoanScheme;
use Carbon\Carbon;
use App\Repayment;
class SeedingController extends Controller
{
  public function fixNgoDistrict()
  {
    $ngos = Ngo::all();
    foreach($ngos as $ngo)
    {
      $ngo = Ngo::find($ngo->id);
      $districts = District::where('state_id',$ngo->state_id)->pluck('id');
      $first_element = $districts->first();
      $last_element = $districts->last();
      //return $last_element;
      $ngo->district_id = rand($first_element,$last_element);
      $ngo->update();
    }
  }
  public function fixSelfHelpGroupDistrict()
  {
    $groups = SelfHelpGroup::all();
    foreach($groups as $group)
    {
      $group = SelfHelpGroup::find($group->id);
      $districts = District::where('state_id',$group->state_id)->pluck('id');
      $first_element = $districts->first();
      $last_element = $districts->last();
      $group->district_id = rand($first_element,$last_element);
      $group->update();
    }
  }
  public function fixUserDetailDistrict()
  {
    $users = UserDetail::all();
    foreach($users as $user)
    {
      $user = UserDetail::find($user->id);
      $districts = District::where('state_id',$user->state_id)->pluck('id');
      $first_element = $districts->first();
      $last_element = $districts->last();
      $user->district_id = rand($first_element,$last_element);
      $user->update();
    }
  }

  public function fixUserDetailShg()
  {
    $users = UserDetail::all();
    foreach($users as $user)
    {
      $user = UserDetail::find($user->id);
      $shgs = SelfHelpGroup::where('ngo_id',$user->ngo_id)->pluck('id');
      $user->self_help_group_id = $shgs->random();
      $user->update();
    }
  }

  public function fixLoans()
  {
    $loans = Loan::all();
    foreach($loans as $loan)
    {
      $loan = Loan::find($loan->id);
      $user = UserDetail::where('user_id',$loan->user_id)->first();
      $loan->ngo_id = $user->ngo_id;
      $loan->self_help_group_id = $user->self_help_group_id;
      $loan->update();
    }
  }

  public function createRepayments()
  {
    $loans = Loan::all();
    ini_set('max_execution_time', 0);
    foreach($loans as $loan)
    {
      $loan_scheme = LoanScheme::where('id',$loan->loan_scheme_id)->first();
      $no_of_installments = $loan_scheme->no_of_installments;
      $gestation_period = $loan_scheme->gestation_period_in_months;
      $start_date = Carbon::parse($loan->date_of_disbursement)->addMonthsNoOverflow($gestation_period);
      $total_amount = $loan->loan_amount;
      $loan->pre_gestation_loan_amount = $total_amount;
      $interest = 0;
      $interest_array = [];
      // Calculate the Total amount increased due to the gestation period
      for($i = 1; $i <= $gestation_period;$i++)
      {
        $interest = (($total_amount*($loan->interest_rate/100))/12);
        $total_amount = $total_amount + $interest;
      }
      $loan->post_gestation_loan_amount = $total_amount;
      $loan->update();
      $repayment = [];
      $new_interest = 0;
      $new_total_amount = $total_amount;
      $P = $total_amount;
      $R = (($loan->interest_rate/100)/12);
      $N = $no_of_installments;
      $first_comp =(1+$R);
      $first_calculation = pow($first_comp,$N);
      $second_comp = (1+$R);
      $second_calculation = pow($second_comp,$N);
      $remaining = $first_calculation/(($second_calculation)-1);
      $monthly_amount = $P * $R * $remaining;
      $first_interest = (($total_amount*($loan->interest_rate/100))/12);
      $first_principal = $monthly_amount - $first_interest;
      $new_total_amount = $total_amount;
      $reducing_array = [];
      for($i = 1; $i <= $no_of_installments;$i++)
      {
        if($i == 1)
        {
          $interest = (($new_total_amount * ($loan->interest_rate/100))/12);
          $principal = $monthly_amount - $interest;
          $repayment = new Repayment;
          $repayment->loan_id = $loan->id;
          $repayment->user_id = $loan->user_id;
          $repayment->title = "Installment No. ".$i;
          $repayment->installment_no = $i;
          $repayment->principal = $first_principal;
          $repayment->interest = $first_interest;
          $repayment->amount = $first_principal + $first_interest;
          $repayment->repayment_status = rand(1,4);
          $repayment->due_date = Carbon::parse($loan->date_of_disbursement)->addMonthsNoOverflow($i+$gestation_period);
          $repayment->save();
          $principal = $repayment->principal;
          $due_date = $repayment->due_date;
        }
        elseif($i != 1)
        {
          $new_total_amount = $new_total_amount - $principal;
          $interest = (($new_total_amount * ($loan->interest_rate/100))/12);
          $principal = $monthly_amount - $interest;
          $repayment = new Repayment;
          $repayment->loan_id = $loan->id;
          $repayment->user_id = $loan->user_id;
          $repayment->title = "Installment No. ".$i;
          $repayment->installment_no = $i;
          $repayment->principal = $principal;
          $repayment->interest = $interest;
          $repayment->amount = $principal + $interest;
          $repayment->repayment_status = rand(1,4);
          $repayment->due_date = Carbon::parse($due_date)->addMonthsNoOverflow(1);
          $repayment->save();
          $due_date = $repayment->due_date;
        }
      }
    }
  }
}
