<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SelfHelpGroup;
use Auth;
use App\Ngo;
class SelfHelpGroupController extends Controller
{
  public function index()
  {
    if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
    {
      $groups = SelfHelpGroup::all();
    }
    elseif(Auth::user()->role_id == 3)
    {
      $ngo_id = Ngo::where('admin_id',Auth::id())->pluck('id')->first();
      $groups = SelfHelpGroup::where('ngo_id',$ngo_id)->get();
    }
    else
    {
      $groups = '';
    }
    return $groups;
  }

  public function store(Request $request)
  {
    $group = new SelfHelpGroup;
    if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
    {
      $group->ngo_id = $request->ngo_id;
    }
    else
    {
      $ngo_id = Ngo::where('admin_id',Auth::id())->pluck('id')->first();
      $group->ngo_id = $ngo_id;
    }
    $group->name = $request->name;
    $group->state_id = $request->state_id;
    $group->district_id = $request->district_id;
    $group->admin_id = $request->admin_id;
    $group->address = $request->address;
    $group->save();
  }

  public function edit($id)
  {
    $group = SelfHelpGroup::find($id);
    return $group;
  }

  public function update(Request $request,$id)
  {
    $group = SelfHelpGroup::find($id);
    if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
    {
      $group->ngo_id = $request->ngo_id;
    }
    else
    {
      $ngo_id = Ngo::where('admin_id',Auth::id())->pluck('id')->first();
      $group->ngo_id = $ngo_id;
    }
    $group->name = $request->name;
    $group->state_id = $request->state_id;
    $group->district_id = $request->district_id;
    $group->admin_id = $request->admin_id;
    $group->address = $request->address;
    $group->update();
  }

  public function getShgsForAdmin($ngo_id)
  {
    if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
    {
      $groups = SelfHelpGroup::where('ngo_id',$ngo_id)->get();
      return $groups;
    }
    else
    {
      return response()->json([
        'errors' =>'Not Allowed'
      ]);
    }
  }

  public function getShgsForNgoAdmins()
  {
    if(Auth::user()->role_id == 3)
    {
      $ngo_id = Ngo::where('admin_id',Auth::id())->pluck('id')->first();
      $groups = SelfHelpGroup::where('ngo_id',$ngo_id)->get();
      return $groups;
    }
    else
    {
      return response()->json([
        'errors' =>'Not Allowed'
      ]);
    }
  }
}
