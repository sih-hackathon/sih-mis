<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\State;
use App\Loan;
use App\Ngo;
use App\Repayment;
use DB;
use App\UserDetail;
use App\Religion;
use App\IncomeLevel;
use App\Caste;
use App\LiteracyLevel;
use App\LoanScheme;
use App\District;
class ReportController extends Controller
{
  public function all_states_all_time()
  {
    $states = State::all();
    $labels = $states->pluck('title')->toArray();
    $count = $states->count();
    $data = [];
    //return $labels;
    for($i = 0; $i < $count; $i++)
    {
      $users = UserDetail::where([['state_id',$i+1]])->pluck('user_id');
      $loans = Loan::whereIn('user_id',$users);
      //$ids = $loans->pluck('id')->toArray();
      //$repaid_amount = Repayment::whereIn('loan_id',$ids)->where('repayment_status',4)->orWhere('repayment_status',3)->sum('amount');
      $loan_count = $loans->count();
      $amount = Loan::whereIn('user_id',$users)->sum('loan_amount');
      $repayments =
      $object = new \stdClass();
      $object->title = $labels[$i];
      $object->loan_count = $loan_count;
      $object->loan_amount = $amount;
      //$object->recovered_amount = $repaid_amount;
      array_push($data,$object);
    }
    //return $ids;
    return $data;
  }

  public function all_districts_particular_state_all_time($state_id)
  {
    $districts = District::where('state_id',$state_id);
    $labels = $districts->pluck('title')->toArray();
    $ids = District::where('state_id',$state_id)->pluck('id')->toArray();
    $count = $districts->count();
    $data = [];
    for($i = 0; $i < $count; $i++)
    {
      $users = UserDetail::where([['district_id',$ids[$i]]])->pluck('user_id');
      $loans = Loan::whereIn('user_id',$users)->count();
      $amount = Loan::whereIn('user_id',$users)->sum('loan_amount');
      $object = new \stdClass();
      $object->title = $labels[$i];
      $object->loan_count = $loans;
      $object->loan_amount = $amount;
      array_push($data,$object);
    }
    return $data;
  }

  public function all_religions_all_time()
  {
    $religions = Religion::all();
    $labels = $religions->pluck('title')->toArray();
    $count = $religions->count();
    $data = [];
    for($i = 0; $i < $count; $i++)
    {
      $users = UserDetail::where([['religion_id',$i+1]])->pluck('user_id');
      $loans = Loan::whereIn('user_id',$users)->count();
      $amount = Loan::whereIn('user_id',$users)->sum('loan_amount');
      $object = new \stdClass();
      $object->title = $labels[$i];
      $object->loan_count = $loans;
      $object->loan_amount = $amount;
      array_push($data,$object);
    }
    return $data;
  }

  public function all_castes_all_time()
  {
    $castes = Caste::all();
    $labels = $castes->pluck('title')->toArray();
    $count = $castes->count();
    $data = [];
    for($i = 0; $i < $count; $i++)
    {
      $users = UserDetail::where([['caste_id',$i+1]])->pluck('user_id');
      $loans = Loan::whereIn('user_id',$users)->count();
      $amount = Loan::whereIn('user_id',$users)->sum('loan_amount');
      $object = new \stdClass();
      $object->title = $labels[$i];
      $object->loan_count = $loans;
      $object->loan_amount = $amount;
      array_push($data,$object);
    }
    return $data;
  }

  public function all_income_levels_all_time()
  {
    $income_levels = IncomeLevel::all();
    $labels = $income_levels->pluck('title')->toArray();
    $count = $income_levels->count();
    $data = [];
    for($i = 0; $i < $count; $i++)
    {
      $users = UserDetail::where([['income_level_id',$i+1]])->pluck('user_id');
      $loans = Loan::whereIn('user_id',$users)->count();
      $amount = Loan::whereIn('user_id',$users)->sum('loan_amount');
      $object = new \stdClass();
      $object->title = $labels[$i];
      $object->loan_count = $loans;
      $object->loan_amount = $amount;
      array_push($data,$object);
    }
    return $data;
  }

  public function all_literacy_levels_all_time()
  {
    $literacy_levels = LiteracyLevel::all();
    $labels = $literacy_levels->pluck('title')->toArray();
    $count = $literacy_levels->count();
    $data = [];
    for($i = 0; $i < $count; $i++)
    {
      $users = UserDetail::where([['literacy_level_id',$i+1]])->pluck('user_id');
      $loans = Loan::whereIn('user_id',$users)->count();
      $amount = Loan::whereIn('user_id',$users)->sum('loan_amount');
      $object = new \stdClass();
      $object->title = $labels[$i];
      $object->loan_count = $loans;
      $object->loan_amount = $amount;
      array_push($data,$object);
    }
    return $data;
  }

  public function all_ngos_particular_state_all_time($state_id)
  {
    $ngos = Ngo::where('state_id',$state_id);
    $labels = $ngos->pluck('name')->toArray();
    $ids = Ngo::where('state_id',$state_id)->pluck('id')->toArray();
    $count = $ngos->count();
    $data = [];
    for($i = 0; $i < $count; $i++)
    {
      $users = UserDetail::where([['ngo_id',$ids[$i]]])->pluck('user_id');
      $loans = Loan::whereIn('user_id',$users)->count();
      $amount = Loan::whereIn('user_id',$users)->sum('loan_amount');
      $object = new \stdClass();
      $object->title = $labels[$i];
      $object->loan_count = $loans;
      $object->loan_amount = $amount;
      array_push($data,$object);
    }
    return $data;
  }

}
