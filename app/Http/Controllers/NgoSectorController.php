<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NgoSector;

class NgoSectorController extends Controller
{
  public function index()
  {
    $ngoSectors = NgoSector::all();
    return $ngoSectors;
  }

  public function store(Request $request)
  {
    $ngoSector = new NgoSector;
    $ngoSector->title = $request->title;
    $ngoSector->description = $request->description;
    $ngoSector->save();
  }

  public function edit($id)
  {
    $ngoSector = NgoSector::find($id);
    return $ngoSector;
  }

  public function update(Request $request, $id)
  {
    $ngoSector = NgoSector::find($id);
    $ngoSector->title = $request->title;
    $ngoSector->description = $request->description;
    $ngoSector->update();
  }

  public function show($id)
  {
    $ngoSector = NgoSector::where('id',$id)->first();
    return $ngoSector;
  }
}
