<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LoanScheme;

class LoanSchemeController extends Controller
{
    public function index()
    {
      $loanSchemes = LoanScheme::all();
      return $loanSchemes;
    }

    public function store(Request $request)
    {
      $loanScheme = new LoanScheme;
      $loanScheme->title = $request->title;
      $loanScheme->description = $request->description;
      $loanScheme->save();
    }

    public function edit($id)
    {
      $loanScheme = LoanScheme::find($id);
      return $loanScheme;
    }

    public function update(Request $request, $id)
    {
      $loanScheme = LoanScheme::find($id);
      $loanScheme->title = $request->title;
      $loanScheme->description = $request->description;
      $loanScheme->update();
    }

    public function show($id)
    {
      $loanScheme = LoanScheme::where('id',$id)->first();
      return $loanScheme;
    }
}
