<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Loan;
use Auth;
use App\Repayment;
use App\Ngo;
use App\SelfHelpGroup;
use App\LoanScheme;
use Carbon\Carbon;
class LoanController extends Controller
{
  public function index()
  {
    if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
    {
      $loans = Loan::all();
      return $loans;
    }
    elseif(Auth::user()->role_id == 3)
    {
      $ngo_id = Ngo::where('admin_id',Auth::id())->pluck('id')->first();
      $loans = Loan::where('ngo_id',$ngo_id)->get();
      return $loans;
    }
    elseif(Auth::user()->role_id == 4)
    {
      $shg_id = SelfHelpGroup::where('admin_id',Auth::id())->pluck('id')->first();
      $loans = Loan::where('self_help_group_id',$shg_id)->get();
      return $loans;
    }
    elseif(Auth::user()->role_id == 5)
    {
      $loans = Loan::where('user_id',Auth::id())->get();
      return $loans;
    }
  }

  public function store(Request $request)
  {
    if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
    {
      $loan = new Loan;
      $loan->ngo_id = $request->ngo_id;
      $loan->self_help_group_id = $request->self_help_group_id;
      $loan->loan_scheme_id = $request->loan_scheme_id;
      $loan->user_id = $request->user_id;
      $loan_scheme = LoanScheme::where('id',$request->loan_scheme_id)->first();
      $loan->loan_amount = $request->loan_amount;
      $loan->interest_rate = $request->interest_rate;
      $loan->date_of_disbursement = $request->date_of_disbursement;
      $loan->save();
      $no_of_installments = $loan_scheme->no_of_installments;
      $gestation_period = $loan_scheme->gestation_period_in_months;
      $start_date = Carbon::parse($loan->date_of_disbursement)->addMonthsNoOverflow($gestation_period);
      $total_amount = $loan->loan_amount;
      $loan->pre_gestation_loan_amount = $total_amount;
      $interest = 0;
      $interest_array = [];
      // Calculate the Total amount increased due to the gestation period
      for($i = 1; $i <= $gestation_period;$i++)
      {
        $interest = (($total_amount*($loan->interest_rate/100))/12);
        $total_amount = $total_amount + $interest;
      }
      $loan->post_gestation_loan_amount = $total_amount;
      $loan->update();
      $repayment = [];
      $new_interest = 0;
      $new_total_amount = $total_amount;
      $P = $total_amount;
      $R = (($loan->interest_rate/100)/12);
      $N = $no_of_installments;
      $first_comp =(1+$R);
      $first_calculation = pow($first_comp,$N);
      $second_comp = (1+$R);
      $second_calculation = pow($second_comp,$N);
      $remaining = $first_calculation/(($second_calculation)-1);
      $monthly_amount = $P * $R * $remaining;
      $first_interest = (($total_amount*($loan->interest_rate/100))/12);
      $first_principal = $monthly_amount - $first_interest;
      $new_total_amount = $total_amount;
      $reducing_array = [];
      for($i = 1; $i <= $no_of_installments;$i++)
      {
        if($i == 1)
        {
          $interest = (($new_total_amount * ($loan->interest_rate/100))/12);
          $principal = $monthly_amount - $interest;
          $repayment = new Repayment;
          $repayment->loan_id = $loan->id;
          $repayment->user_id = $loan->user_id;
          $repayment->ngo_id = $loan->ngo_id;
          $repayment->self_help_group_id = $loan->self_help_group_id;
          $repayment->title = "Installment No. ".$i;
          $repayment->installment_no = $i;
          $repayment->principal = $first_principal;
          $repayment->interest = $first_interest;
          $repayment->amount = $first_principal + $first_interest;
          $repayment->repayment_status = rand(1,4);
          $repayment->due_date = Carbon::parse($loan->date_of_disbursement)->addMonthsNoOverflow($i+$gestation_period);
          $repayment->save();
          $principal = $repayment->principal;
          $due_date = $repayment->due_date;
        }
        elseif($i != 1)
        {
          $new_total_amount = $new_total_amount - $principal;
          $interest = (($new_total_amount * ($loan->interest_rate/100))/12);
          $principal = $monthly_amount - $interest;
          $repayment = new Repayment;
          $repayment->loan_id = $loan->id;
          $repayment->user_id = $loan->user_id;
          $repayment->ngo_id = $loan->ngo_id;
          $repayment->self_help_group_id = $loan->self_help_group_id;
          $repayment->title = "Installment No. ".$i;
          $repayment->installment_no = $i;
          $repayment->principal = $principal;
          $repayment->interest = $interest;
          $repayment->amount = $principal + $interest;
          $repayment->repayment_status = rand(1,4);
          $repayment->due_date = Carbon::parse($due_date)->addMonthsNoOverflow(1);
          $repayment->save();
          $due_date = $repayment->due_date;
        }
      }
    }
    elseif(Auth::user()->role_id == 3)
    {
      $loan = new Loan;
      $ngo_id = Ngo::where('admin_id',Auth::id())->pluck('id')->first();
      $loan->ngo_id = $ngo_id;
      $loan->self_help_group_id = $request->self_help_group_id;
      $loan->loan_scheme_id = $request->loan_scheme_id;
      $loan->user_id = $request->user_id;
      $loan_scheme = LoanScheme::where('id',$request->loan_scheme_id)->first();
      $loan->loan_amount = $request->loan_amount;
      $loan->interest_rate = $request->interest_rate;
      $loan->date_of_disbursement = $request->date_of_disbursement;
      $loan->save();
      $no_of_installments = $loan_scheme->no_of_installments;
      $gestation_period = $loan_scheme->gestation_period_in_months;
      $start_date = Carbon::parse($loan->date_of_disbursement)->addMonthsNoOverflow($gestation_period);
      $total_amount = $loan->loan_amount;
      $loan->pre_gestation_loan_amount = $total_amount;
      $interest = 0;
      $interest_array = [];
      // Calculate the Total amount increased due to the gestation period
      for($i = 1; $i <= $gestation_period;$i++)
      {
        $interest = (($total_amount*($loan->interest_rate/100))/12);
        $total_amount = $total_amount + $interest;
      }
      $loan->post_gestation_loan_amount = $total_amount;
      $loan->update();
      $repayment = [];
      $new_interest = 0;
      $new_total_amount = $total_amount;
      $P = $total_amount;
      $R = (($loan->interest_rate/100)/12);
      $N = $no_of_installments;
      $first_comp =(1+$R);
      $first_calculation = pow($first_comp,$N);
      $second_comp = (1+$R);
      $second_calculation = pow($second_comp,$N);
      $remaining = $first_calculation/(($second_calculation)-1);
      $monthly_amount = $P * $R * $remaining;
      $first_interest = (($total_amount*($loan->interest_rate/100))/12);
      $first_principal = $monthly_amount - $first_interest;
      $new_total_amount = $total_amount;
      $reducing_array = [];
      for($i = 1; $i <= $no_of_installments;$i++)
      {
        if($i == 1)
        {
          $interest = (($new_total_amount * ($loan->interest_rate/100))/12);
          $principal = $monthly_amount - $interest;
          $repayment = new Repayment;
          $repayment->loan_id = $loan->id;
          $repayment->user_id = $loan->user_id;
          $repayment->ngo_id = $loan->ngo_id;
          $repayment->self_help_group_id = $loan->self_help_group_id;
          $repayment->title = "Installment No. ".$i;
          $repayment->installment_no = $i;
          $repayment->principal = $first_principal;
          $repayment->interest = $first_interest;
          $repayment->amount = $first_principal + $first_interest;
          $repayment->repayment_status = rand(1,4);
          $repayment->due_date = Carbon::parse($loan->date_of_disbursement)->addMonthsNoOverflow($i+$gestation_period);
          $repayment->save();
          $principal = $repayment->principal;
          $due_date = $repayment->due_date;
        }
        elseif($i != 1)
        {
          $new_total_amount = $new_total_amount - $principal;
          $interest = (($new_total_amount * ($loan->interest_rate/100))/12);
          $principal = $monthly_amount - $interest;
          $repayment = new Repayment;
          $repayment->loan_id = $loan->id;
          $repayment->user_id = $loan->user_id;
          $repayment->ngo_id = $loan->ngo_id;
          $repayment->self_help_group_id = $loan->self_help_group_id;
          $repayment->title = "Installment No. ".$i;
          $repayment->installment_no = $i;
          $repayment->principal = $principal;
          $repayment->interest = $interest;
          $repayment->amount = $principal + $interest;
          $repayment->repayment_status = rand(1,4);
          $repayment->due_date = Carbon::parse($due_date)->addMonthsNoOverflow(1);
          $repayment->save();
          $due_date = $repayment->due_date;
        }
      }
    }
  }
}
