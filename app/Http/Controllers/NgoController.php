<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ngo;
use App\NgoActiveSector;
class NgoController extends Controller
{
  public function index()
  {
    $ngos = Ngo::with('ngo_get_states','ngo_get_districts','ngo_get_admins')->get();
    return $ngos;
  }

  public function store(Request $request)
  {
    $ngo = Ngo::create($request->all());
    for($i=0;$i<count($request->sectors);$i++)
    {
      $NgoActiveSector = new NgoActiveSector;
      $NgoActiveSector->ngo_id = $ngo->id;
      $NgoActiveSector->sector_id = $request->sectors[$i];
      $NgoActiveSector->save();
    }
    return $ngo;
  }

  public function edit($id)
  {
    $ngo = Ngo::find($id);
    return $ngo;
  }

  public function update(Request $request,$id)
  {
    $ngos = Ngo::find($id);
    $ngos->update($request->all());
    if(count($request->sector_ids) > 0 || count($request->sectors) > 0)
    {
      $array = [];
      $sectors = [];
      // Execute this when Sector IDs are more than the sectors
      if(count($request->sector_ids) > count($request->sectors))
      {
        for($i = 0;$i<count($request->sector_ids);$i++)
        {
          if(isset($request->sector_ids[$i]))
          {
            $array[$i] = (int) $request->sector_ids[$i];
          }
          if(isset($request->sectors[$i]))
          {
            $sectors[$i] = $request->sectors[$i];
          }
        }
        $sector_ids ="Sector IDs More than Sectors";
        //return $sector_ids;
      }
      // Execute this when Sectors are more than the Sector IDs
      elseif(count($request->sector_ids) < count($request->sectors))
      {
        for($i = 0;$i<count($request->sectors);$i++)
        {
          if(isset($request->sector_ids[$i]))
          {
            $array[$i] = (int) $request->sector_ids[$i];
          }
          if(isset($request->sectors[$i]))
          {
            $sectors[$i] = $request->sectors[$i];
          }
        }
        $sector_ids ="Sectors More than Sector IDs";
        //return $sector_ids;
      }
      // Execute this when Sector IDs are equal to sectors
      else
      {
        for($i = 0;$i<count($request->sectors);$i++)
        {
            $array[$i] = (int) $request->sector_ids[$i];
            $sectors[$i] = $request->sectors[$i];
        }
          $sector_ids ="Sectors are equal to Sector IDs";
          //return $sector_ids;
      }
      // Execute when Sector IDs will be more than Sectors
      if(count($array)>count($sectors))
      {
        for($i=0;$i<count($array);$i++)
        {
          if(!empty($array[$i]))
          {
            $check = NgoActiveSector::find($array[$i]);

            if(isset($sectors[$i]))
            {
              $check->sector_id = $sectors[$i];
              $check->update();
            }
            elseif(!isset($sectors[$i]))
            {
              $check->delete();
            }
          }
          elseif(empty($array[$i]))
          {
            $new_sector = new NgoActiveSector;
            $new_sector->ngo_id = $ngos->id;
            $new_sector->sector_id = $sectors[$i];
            $new_sector->save();
          }
        }
      }
      // Execute when Sectors will be more than Sector IDs
      elseif(count($array)<count($sectors))
      {
        for($i=0;$i<count($sectors);$i++)
        {
          if(!empty($array[$i]))
          {
            $check = NgoActiveSector::find($array[$i]);
            $check->sector_id = $sectors[$i];
            if(!empty($sectors[$i]))
            {
              $check->update();
            }
            elseif(empty($sectors[$i]))
            {
              $check->delete();
            }
          }
          elseif(empty($array[$i]))
          {
            $new_sector = new NgoActiveSector;
            $new_sector->ngo_id = $ngos->id;
            $new_sector->sector_id = $sectors[$i];
            $new_sector->save();
          }
        }
      }
      // Execute when Sector IDs will be equal to Sectors
      elseif(count($array) == count($sectors))
      {
        for($i=0;$i<count($sectors);$i++)
        {
            $check = NgoActiveSector::find($array[$i]);
            $check->sector_id = $sectors[$i];
            $check->update();
        }
      }

    }
    return $ngos;
  }

  public function show($id)
  {
    $ngo = Ngo::find($id);
    return $ngo;
  }
}
