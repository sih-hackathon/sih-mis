<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\District;
class DistrictController extends Controller
{
  public function index()
  {
    $districts = District::with('districts_get_states:id,title')->get();
    return $districts;
  }

  public function store(Request $request)
  {
    $district = new District;
    $district->title = $request->title;
    $district->description = $request->description;
    $district->state_id = $request->state_id;
    $district->save();
  }

  public function edit($id)
  {
    $district = District::find($id);
    return $district;
  }

  public function update(Request $request, $id)
  {
    $district = District::find($id);
    $district->title = $request->title;
    $district->description = $request->description;
    $district->update();
  }

  public function show($id)
  {
    $district = District::where('id',$id)->with('districts_get_states:id,title')->first();
    return $district;
  }

  public function get_districts($state_id)
  {
    $districts = District::where('state_id',$state_id)->get();
    return $districts;
  }
}
