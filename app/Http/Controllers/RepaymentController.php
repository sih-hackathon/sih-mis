<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repayment;
use Auth;
use App\Ngo;
use App\UserDetail;
use App\SelfHelpGroup;
class RepaymentController extends Controller
{
  public function index($loan_id)
  {
    if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
    {
      $repayments = Repayment::where('loan_id',$loan_id)->get();
      return $repayments;
    }
    elseif(Auth::user()->role_id == 3)
    {
      $repayments = Repayment::where('loan_id',$loan_id);
      $ngo_id = $repayments->pluck('ngo_id')->first();
      $user_ngo_id = Ngo::where('admin_id',Auth::id())->pluck('id')->first();
      if($ngo_id == $user_ngo_id)
      {
        $repayments = $repayments->get();
        return $repayments;
      }
      else
      {
        return response()->json([
          'errors' => 'Not Allowed'
        ]);
      }
    }
    elseif(Auth::user()->role_id == 4)
    {
      $repayments = Repayment::where('loan_id',$loan_id);
      $shg_id = $repayments->pluck('self_help_group_id')->first();
      $user_shg_id = SelfHelpGroup::where('admin_id',Auth::id())->pluck('id')->first();
      if($shg_id == $user_shg_id)
      {
        $repayments = $repayments->get();
        return $repayments;
      }
      else
      {
        return response()->json([
          'errors' => 'Not Allowed'
        ]);
      }
    }
    elseif(Auth::user()->role_id == 5)
    {
      $repayments = Repayment::where('loan_id',$loan_id);
      $user_id = $repayments->pluck('user_id')->first();
      if(Auth::id() == $user_shg_id)
      {
        $repayments = $repayments->get();
        return $repayments;
      }
      else
      {
        return response()->json([
          'errors' => 'Not Allowed'
        ]);
      }
    }
  }

  public function edit($loan_id,$id)
  {
    if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
    {
      $repayment = Repayment::find($id);
      return $repayment;
    }
    elseif(Auth::user()->role_id == 3)
    {
      $repayment = Repayment::find($id);
      $ngo_id = $repayment->ngo_id;
      $user_ngo_id = Ngo::where('admin_id',Auth::id())->pluck('id')->first();
      if($ngo_id == $user_ngo_id)
      {
        return $repayment;
      }
      else
      {
        return response()->json([
          'errors' => 'Not Allowed'
        ]);
      }
    }
  }

  public function update(Request $request,$id)
  {
    if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
    {
      $repayment = Repayment::find($id);
      $repayment->update($request->all());
    }
    elseif(Auth::user()->role_id == 3)
    {
      $repayment = Repayment::find($id);
      $ngo_id = $repayment->ngo_id;
      $user_ngo_id = Ngo::where('admin_id',Auth::id())->pluck('id')->first();
      if($ngo_id == $user_ngo_id)
      {
        $repayment->update($request->all());
      }
      else
      {
        return response()->json([
          'errors' => 'Not Allowed'
        ]);
      }
    }
  }

  public function show($id)
  {
    if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
    {
      $repayment = Repayment::find($id);
      return $repayment;
    }
    elseif(Auth::user()->role_id == 3)
    {
      $repayment = Repayment::find($id);
      $ngo_id = $repayment->ngo_id;
      $user_ngo_id = Ngo::where('admin_id',Auth::id())->pluck('id')->first();
      if($ngo_id == $user_ngo_id)
      {
        return $repayment;
      }
      else
      {
        return response()->json([
          'errors' => 'Not Allowed'
        ]);
      }
    }
    elseif(Auth::user()->role_id == 4)
    {
      $repayment = Repayment::find($id);
      $self_help_group_id = $repayment->self_help_group_id;
      $user_shg_id = SelfHelpGroup::where('admin_id',Auth::id())->pluck('id')->first();
      if($self_help_group_id == $user_shg_id)
      {
        return $repayment;
      }
      else
      {
        return response()->json([
          'errors' => 'Not Allowed'
        ]);
      }
    }
  }
}
