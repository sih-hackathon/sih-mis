<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NgoActiveSector;
class NgoActiveSectorController extends Controller
{
  public function index()
  {
    $NgoActiveSectors = NgoActiveSector::all();
    return $NgoActiveSectors;
  }

  public function store(Request $request)
  {
    $NgoActiveSectors = new NgoActiveSector;
    $NgoActiveSectors->ngo_id = $request->ngo_id;
    $NgoActiveSectors->sector_id = $request->sector_id;
    $NgoActiveSectors->save();
  }

  public function edit($id)
  {
    $NgoActiveSectors = NgoActiveSector::find($id);
    return $NgoActiveSectors;
  }

  public function update(Request $request,$id)
  {
    $NgoActiveSectors = NgoActiveSector::find($id);
    $NgoActiveSectors->ngo_id = $request->ngo_id;
    $NgoActiveSectors->sector_id = $request->sector_id;
    $NgoActiveSectors->update();
  }

  public function getNgoSectors($ngo_id)
  {
    $NgoActiveSectors = NgoActiveSector::where('ngo_id',$ngo_id)->pluck('sector_id','id')->toArray();
    return json_encode($NgoActiveSectors);
  }
}
