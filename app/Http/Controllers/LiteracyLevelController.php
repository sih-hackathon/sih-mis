<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LiteracyLevel;

class LiteracyLevelController extends Controller
{
    public function index()
    {
      $literacyLevels = LiteracyLevel::all();
      return $literacyLevels;
    }

    public function store(Request $request)
    {
      $literacyLevel = new LiteracyLevel;
      $literacyLevel->title = $request->title;
      $literacyLevel->description = $request->description;
      $literacyLevel->save();
    }

    public function edit($id)
    {
      $literacyLevel = LiteracyLevel::find($id);
      return $literacyLevel;
    }

    public function update(Request $request, $id)
    {
      $literacyLevel = LiteracyLevel::find($id);
      $literacyLevel->title = $request->title;
      $literacyLevel->description = $request->description;
      $literacyLevel->update();
    }

    public  function show($id)
    {
      $literacyLevel = LiteracyLevel::where('id',$id)->first();
      return $literacyLevel;
    }
}
