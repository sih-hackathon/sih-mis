<?php

namespace App\Http\Controllers;
use App\State;
use Illuminate\Http\Request;

class StateController extends Controller
{
    public function index()
    {
      $states = State::all();
      return $states;
    }

    public function store(Request $request)
    {
      $states = new State;
      $states->title = $request->title;
      $states->description = $request->description;
      $states->save();
    }

    public function edit($id)
    {
      $state = State::find($id);
      return $state;
    }

    public function update(Request $request,$id)
    {
      $states = State::find($id);
      $states->title = $request->title;
      $states->description = $request->description;
      $states->update();
    }

    public function show($id)
    {
      $state = State::where('id',$id)->first();
      return $state;
    }
}
