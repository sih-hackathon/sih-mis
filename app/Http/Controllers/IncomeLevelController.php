<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\IncomeLevel;

class IncomeLevelController extends Controller
{
  public function index()
  {
    $IncomeLevels = IncomeLevel::all();
    return $IncomeLevels;
  }

  public function store(Request $request)
  {
    $IncomeLevels = new IncomeLevel;
    $IncomeLevels->title = $request->title;
    $IncomeLevels->description = $request->description;
    $IncomeLevels->save();
  }

  public function edit($id)
  {
    $IncomeLevel = IncomeLevel::find($id);
    return $IncomeLevel;
  }

  public function update(Request $request,$id)
  {
    $IncomeLevels = IncomeLevel::find($id);
    $IncomeLevels->title = $request->title;
    $IncomeLevels->description = $request->description;
    $IncomeLevels->update();
  }

  public function show($id)
  {
    $IncomeLevel = IncomeLevel::where('id',$id)->first();
    return $IncomeLevel;
  }
}
