<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Religion;

class ReligionController extends Controller
{
  public function index()
  {
    $religion = Religion::all();
    return $religion;
  }

  public function store(Request $request)
  {
    $religion = new Religion;
    $religion->title = $request->title;
    $religion->description = $request->description;
    $religion->save();
  }

  public function edit($id)
  {
    $religion = Religion::find($id);
    return $religion;
  }

  public function update(Request $request, $id)
  {
    $religion = Religion::find($id);
    $religion->title = $request->title;
    $religion->description = $request->description;
    $religion->update();
  }

  public function show($id)
  {
    $religion = Religion::where('id',$id)->first();
    return $religion;
  }
}
