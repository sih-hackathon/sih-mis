<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\State;
use App\Loan;
use App\Ngo;
use App\Repayment;
use DB;
use App\UserDetail;
use App\Religion;
use App\IncomeLevel;
use App\Caste;
use App\LiteracyLevel;
use App\LoanScheme;
class ChartForOneDistrictController extends Controller
{
  public function all_literacy_levels_loan_count_district_wise($district_id)
  {
    $literacy_level = LiteracyLevel::all();
    $labels = $literacy_level->pluck('title');
    $count = $literacy_level->count();
    $data = [];
    /*$users = UserDetail::where('literacy_level_id',8)->pluck('user_id');
    $loans = Loan::whereIn('user_id',$users)->count();
    return $loans;*/
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where([['district_id',$district_id],['literacy_level_id',$i+1]])->pluck('user_id');
      $loans = Loan::whereIn('user_id',$users)->count();
      array_push($data,$loans);
    }
    return response()->json([
      'labels' => $labels,
      'data'=>$data,
    ]);
  }

  public function all_income_levels_loan_count_district_wise($district_id)
  {
    $income_level = IncomeLevel::all();
    $labels = $income_level->pluck('title');
    $count = $income_level->count();
    $data = [];
    /*$users = UserDetail::where('literacy_level_id',8)->pluck('user_id');
    $loans = Loan::whereIn('user_id',$users)->count();
    return $loans;*/
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where([['district_id',$district_id],['income_level_id',$i+1]])->pluck('user_id');
      $loans = Loan::whereIn('user_id',$users)->count();
      array_push($data,$loans);
    }
    return response()->json([
      'labels' => $labels,
      'data'=>$data,
    ]);
  }

  public function all_castes_loan_count_district_wise($district_id)
  {
    $caste = Caste::all();
    $labels = $caste->pluck('title');
    $count = $caste->count();
    $data = [];
    /*$users = UserDetail::where('literacy_level_id',8)->pluck('user_id');
    $loans = Loan::whereIn('user_id',$users)->count();
    return $loans;*/
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where([['district_id',$district_id],['caste_id',$i+1]])->pluck('user_id');
      $loans = Loan::whereIn('user_id',$users)->count();
      array_push($data,$loans);
    }
    return response()->json([
      'labels' => $labels,
      'data'=>$data,
    ]);
  }

  public function all_religions_loan_count_district_wise($district_id)
  {
    $religions = Religion::all();
    $labels = $religions->pluck('title');
    $count = $religions->count();
    $data = [];
    /*$users = UserDetail::where('literacy_level_id',8)->pluck('user_id');
    $loans = Loan::whereIn('user_id',$users)->count();
    return $loans;*/
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where([['district_id',$district_id],['religion_id',$i+1]])->pluck('user_id');
      $loans = Loan::whereIn('user_id',$users)->count();
      array_push($data,$loans);
    }
    return response()->json([
      'labels' => $labels,
      'data'=>$data,
    ]);
  }

  public function all_literacy_levels_loan_amount_district_wise($district_id)
  {
    $literacy_level = LiteracyLevel::all();
    $labels = $literacy_level->pluck('title');
    $count = $literacy_level->count();
    $data = [];
    /*$users = UserDetail::where('literacy_level_id',8)->pluck('user_id');
    $loans = Loan::whereIn('user_id',$users)->sum('loan_amount');
    return $loans;*/
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where([['district_id',$district_id],['literacy_level_id',$i+1]])->pluck('user_id');
      $loans = Loan::whereIn('user_id',$users)->sum('loan_amount');
      array_push($data,$loans);
    }
    return response()->json([
      'labels' => $labels,
      'data'=>$data,
    ]);
  }

  public function all_income_levels_loan_amount_district_wise($district_id)
  {
    $income_level = IncomeLevel::all();
    $labels = $income_level->pluck('title');
    $count = $income_level->count();
    $data = [];
    /*$users = UserDetail::where('literacy_level_id',8)->pluck('user_id');
    $loans = Loan::whereIn('user_id',$users)->count();
    return $loans;*/
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where([['district_id',$district_id],['income_level_id',$i+1]])->pluck('user_id');
      $loans = Loan::whereIn('user_id',$users)->sum('loan_amount');
      array_push($data,$loans);
    }
    return response()->json([
      'labels' => $labels,
      'data'=>$data,
    ]);
  }

  public function all_castes_loan_amount_district_wise($district_id)
  {
    $caste = Caste::all();
    $labels = $caste->pluck('title');
    $count = $caste->count();
    $data = [];
    /*$users = UserDetail::where('literacy_level_id',8)->pluck('user_id');
    $loans = Loan::whereIn('user_id',$users)->count();
    return $loans;*/
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where([['district_id',$district_id],['caste_id',$i+1]])->pluck('user_id');
      $loans = Loan::whereIn('user_id',$users)->sum('loan_amount');
      array_push($data,$loans);
    }
    return response()->json([
      'labels' => $labels,
      'data'=>$data,
    ]);
  }

  public function all_religions_loan_amount_district_wise($district_id)
  {
    $religions = Religion::all();
    $labels = $religions->pluck('title');
    $count = $religions->count();
    $data = [];
    /*$users = UserDetail::where('literacy_level_id',8)->pluck('user_id');
    $loans = Loan::whereIn('user_id',$users)->count();
    return $loans;*/
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where([['district_id',$district_id],['religion_id',$i+1]])->pluck('user_id');
      $loans = Loan::whereIn('user_id',$users)->sum('loan_amount');
      array_push($data,$loans);
    }
    return response()->json([
      'labels' => $labels,
      'data'=>$data,
    ]);
  }

  public function all_literacy_levels_repayment_status_district_wise($district_id)
  {
    $literacy_level = LiteracyLevel::all();
    $labels = $literacy_level->pluck('title');
    $count = $literacy_level->count();
    $beforeTimeData = [];
    $onTimeData = [];
    $paidLateData = [];
    $notPaidData = [];
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where([['district_id',$district_id],['literacy_level_id',$i+1]])->pluck('user_id');
      //$loans = ;
      $beforeTime = Repayment::whereIn('user_id',$users)->where('repayment_status',4)->count();
      $onTime = Repayment::whereIn('user_id',$users)->where('repayment_status',3)->count();
      $paidLate = Repayment::whereIn('user_id',$users)->where('repayment_status',2)->count();
      $notPaid = Repayment::whereIn('user_id',$users)->where('repayment_status',1)->count();
      array_push($beforeTimeData,$beforeTime);
      array_push($onTimeData,$onTime);
      array_push($paidLateData,$paidLate);
      array_push($notPaidData,$notPaid);
    }
    return response()->json([
      'labels'=> $labels,
      'onTimeData' => $onTimeData,
      'beforeTimeData' => $beforeTimeData,
      'paidLateData' => $paidLateData,
      'notPaidData' => $notPaidData,
    ]);
  }

  public function all_income_levels_repayment_status_district_wise($district_id)
  {
    $income_level = IncomeLevel::all();
    $labels = $income_level->pluck('title');
    $count = $income_level->count();
    $beforeTimeData = [];
    $onTimeData = [];
    $paidLateData = [];
    $notPaidData = [];
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where([['district_id',$district_id],['income_level_id',$i+1]])->pluck('user_id');
      //$loans = ;
      $beforeTime = Repayment::whereIn('user_id',$users)->where('repayment_status',4)->count();
      $onTime = Repayment::whereIn('user_id',$users)->where('repayment_status',3)->count();
      $paidLate = Repayment::whereIn('user_id',$users)->where('repayment_status',2)->count();
      $notPaid = Repayment::whereIn('user_id',$users)->where('repayment_status',1)->count();
      array_push($beforeTimeData,$beforeTime);
      array_push($onTimeData,$onTime);
      array_push($paidLateData,$paidLate);
      array_push($notPaidData,$notPaid);
    }
    return response()->json([
      'labels'=> $labels,
      'onTimeData' => $onTimeData,
      'beforeTimeData' => $beforeTimeData,
      'paidLateData' => $paidLateData,
      'notPaidData' => $notPaidData,
    ]);
  }

  public function all_castes_repayment_status_district_wise($district_id)
  {
    $caste = Caste::all();
    $labels = $caste->pluck('title');
    $count = $caste->count();
    $beforeTimeData = [];
    $onTimeData = [];
    $paidLateData = [];
    $notPaidData = [];
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where([['district_id',$district_id],['caste_id',$i+1]])->pluck('user_id');
      //$loans = ;
      $beforeTime = Repayment::whereIn('user_id',$users)->where('repayment_status',4)->count();
      $onTime = Repayment::whereIn('user_id',$users)->where('repayment_status',3)->count();
      $paidLate = Repayment::whereIn('user_id',$users)->where('repayment_status',2)->count();
      $notPaid = Repayment::whereIn('user_id',$users)->where('repayment_status',1)->count();
      array_push($beforeTimeData,$beforeTime);
      array_push($onTimeData,$onTime);
      array_push($paidLateData,$paidLate);
      array_push($notPaidData,$notPaid);
    }
    return response()->json([
      'labels'=> $labels,
      'onTimeData' => $onTimeData,
      'beforeTimeData' => $beforeTimeData,
      'paidLateData' => $paidLateData,
      'notPaidData' => $notPaidData,
    ]);
  }

  public function all_religions_repayment_status_district_wise($district_id)
  {
    $religions = Religion::all();
    $labels = $religions->pluck('title');
    $count = $caste->count();
    $beforeTimeData = [];
    $onTimeData = [];
    $paidLateData = [];
    $notPaidData = [];
    for($i = 0; $i < $count ; $i++)
    {
      $users = UserDetail::where([['district_id',$district_id],['religion_id',$i+1]])->pluck('user_id');
      //$loans = ;
      $beforeTime = Repayment::whereIn('user_id',$users)->where('repayment_status',4)->count();
      $onTime = Repayment::whereIn('user_id',$users)->where('repayment_status',3)->count();
      $paidLate = Repayment::whereIn('user_id',$users)->where('repayment_status',2)->count();
      $notPaid = Repayment::whereIn('user_id',$users)->where('repayment_status',1)->count();
      array_push($beforeTimeData,$beforeTime);
      array_push($onTimeData,$onTime);
      array_push($paidLateData,$paidLate);
      array_push($notPaidData,$notPaid);
    }
    return response()->json([
      'labels'=> $labels,
      'onTimeData' => $onTimeData,
      'beforeTimeData' => $beforeTimeData,
      'paidLateData' => $paidLateData,
      'notPaidData' => $notPaidData,
    ]);
  }
}
