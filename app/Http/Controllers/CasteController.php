<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Caste;

class CasteController extends Controller
{
    public function index()
    {
      $castes = Caste::all();
      return $castes;
    }

    public function store(Request $request)
    {
      $caste = new Caste;
      $caste->title = $request->title;
      $caste->description = $request->description;
      $caste->save();
    }

    public function edit($id)
    {
      $caste = Caste::find($id);
      return $caste;
    }

    public function update(Request $request, $id)
    {
      $caste = Caste::find($id);
      $caste->title = $request->title;
      $caste->description = $request->description;
      $caste->update();
    }

    public function show($id)
    {
      $caste = Caste::where('id',$id)->first();
      return $caste;
    }
}
