<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\UserDetail;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
class UserController extends Controller
{
    public function index()
    {
        $users = User::with('users_get_roles')->get();
        return $users;
    }


    public function store(Request $request)
    {
      $user = new User;
      $user->name = $request->name;
      $user->email = $request->email;
      $user->password = Hash::make($request->password);
      $user->phone_number = $request->phone_number;
      $user->role_id = $request->role_id;
      $user->save();
    }

    public function edit($id)
    {
      $user = User::find($id);
      return $user;
    }

    public function update(Request $request,$id)
    {
      $user = User::find($id);
      $user->name = $request->name;
      $user->email = $request->email;
      $user->phone_number = $request->phone_number;
      $user->role_id = $request->role_id;
      $user->update();
    }

    public function show($id)
    {
      $user = User::with('users_get_roles:id,title')->where('id',$id)->first();
      return $user;
    }

    public function get_ngo_user()
    {
      $users = User::where('role_id',3)->get();
      return $users;
    }

    public function get_shg_user()
    {
      $users = User::where('role_id',4)->get();
      return $users;
    }

    public function getEntrepreneursForShg($shg_id)
    {
      $user_details = UserDetail::where('self_help_group_id',$shg_id)->pluck('user_id');
      $users = User::whereIn('id',$user_details)->get();
      return $users;
    }

    public function import(Request $request)
    {
      //return $request->file;
         /*$request->validate([
            //'import_file' => 'required|file|mimes:xlsx,xls'
        ]);*/

        $path = $request->file('import_file');
        $data = Excel::import(new UsersImport, $path);

        return response()->json(['message' => 'uploaded successfully'], 200);
    }
}
