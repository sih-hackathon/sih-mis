<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table ='states';
    protected $fillable = ['title','description'];

    public function districts_get_states()
    {
      return $this->hasMany('App\District','state_id');
    }

    public function ngo_get_states()
    {
      return $this->hasMany('App\Ngo','state_id');
    }
}
