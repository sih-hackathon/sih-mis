<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncomeLevel extends Model
{
    protected $table ='income_levels';
    protected $fillable = ['title','description'];
}
