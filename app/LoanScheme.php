<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanScheme extends Model
{
  protected $table = 'loan_schemes';
  protected $fillable = ['title','description','gestation_period_in_months','no_of_installments'];
}
