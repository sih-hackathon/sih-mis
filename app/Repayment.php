<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Repayment extends Model
{
  protected $table = 'repayments';
  protected $fillable = ['loan_id','ngo_id','user_id','self_help_group_id','title','installment_no','amount','principal','interest','paid_amount','due_date','paid_on','repayment_status','method_of_payment','payment_file_pdf_url','transaction_no'];


  public function repayment_get_ngo()
  {
    return $this->belongsTo('App\Ngo','ngo_id');
  }
}
