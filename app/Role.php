<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
  protected $table = 'roles';
  protected $fillable = ['title','description'];

  public function users_get_roles()
  {
    return $this->hasMany('App\User','role_id');
  }
}
