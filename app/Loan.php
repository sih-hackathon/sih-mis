<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
  protected $table = 'loans';
  protected $fillable = ['ngo_id','self_help_group_id','loan_scheme_id','user_id','loan_amount','interest_rate','date_of_disbursement','repayment_from_date','total_repayment_amount','total_repaid_amount','due_date'];
}
