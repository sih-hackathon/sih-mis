<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
  protected $table = 'user_details';
  protected $fillable = ['user_id','ngo_id','self_help_group_id','state_id','district_id','income_level_id','literacy_level_id','religion_id','caste_id','economic_activity','aadhar_card_no','address','bank_account_no','ifsc_code','age','savings','no_of_family_members','mobile_number'];
}
