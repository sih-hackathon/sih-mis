<?php

namespace App\Imports;

use App\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Hash;
class UsersImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new User([
          'name'  => $row[0],
          'email' => $row[1],
          'password' => Hash::make($row[2]),
          'role_id' => $row[3],
          'phone_number'=> $row[4],
        ]);
    }
}
