import VueRouter from 'vue-router'

// Pages
import Home from './Home'
import Register from './auth/Register'
import Login from './auth/Login'
import Dashboard from './pages/user/Dashboard'
import AdminDashboard from './pages/admin/Dashboard'

import RolesIndex from './roles/Index'
import RolesCreate from './roles/Create'
import RolesEdit from './roles/Edit'
import RolesView from './roles/View'

import UsersIndex from './users/Index'
import UsersCreate from './users/Create'
import UsersEdit from './users/Edit'
import UsersView from './users/View'
import UsersImport from './users/Import'

import StatesIndex from './states/Index'
import StatesCreate from './states/Create'
import StatesEdit from './states/Edit'
import StatesView from './states/View'

import IncomeLevelsIndex from './income-level/Index'
import IncomeLevelsCreate from './income-level/Create'
import IncomeLevelsEdit from './income-level/Edit'
import IncomeLevelsView from './income-level/View'

import LoanSchemesIndex from './loan-schemes/Index'
import LoanSchemesCreate from './loan-schemes/Create'
import LoanSchemesEdit from './loan-schemes/Edit'
import LoanSchemesView from './loan-schemes/View'

import ReligionsIndex from './religions/Index'
import ReligionsCreate from './religions/Create'
import ReligionsEdit from './religions/Edit'
import ReligionsView from './Religions/View'

import CastesIndex from './castes/Index'
import CastesCreate from './castes/Create'
import CastesEdit from './castes/Edit'
import CastesView from './castes/View'

import LiteracyLevelsIndex from './literacy-levels/Index'
import LiteracyLevelsCreate from './literacy-levels/Create'
import LiteracyLevelsEdit from './literacy-levels/Edit'
import LiteracyLevelsView from './literacy-levels/View'

import DistrictsIndex from './districts/Index'
import DistrictsCreate from './districts/Create'
import DistrictsEdit from './districts/Edit'
import DistrictsView from './districts/View'

import NgoSectorsIndex from './ngo-sectors/Index'
import NgoSectorsCreate from './ngo-sectors/Create'
import NgoSectorsEdit from './ngo-sectors/Edit'
import NgoSectorsView from './ngo-sectors/View'

import NgosIndex from './ngos/Index'
import NgosCreate from './ngos/Create'
import NgosEdit from './ngos/Edit'
import NgosView from './ngos/View'

import SelfHelpGroupsIndex from './self-help-groups/Index'
import SelfHelpGroupsCreate from './self-help-groups/Create'
import SelfHelpGroupsEdit from './self-help-groups/Edit'

import LoansIndex from './loans/Index'
import LoansCreate from './loans/Create'
import LoansView from './loans/View'

import RepaymentsIndex from './repayments/Index'
import RepaymentsEdit from './repayments/Edit'
import RepaymentsView from './repayments/View'

import NgoStateWisePerformance from './charts/NgoStateWisePerformance'
import ReligionStateWisePerformance from './charts/ReligionStateWisePerformance'
import CasteStateWisePerformance from './charts/CasteStateWisePerformance'
import IncomeLevelStateWisePerformance from './charts/IncomeLevelStateWisePerformance'
import LiteracyLevelStateWisePerformance from './charts/LiteracyLevelStateWisePerformance'
import NoSpecificInputChart from './charts/NoSpecificInputChart'
import SpecificStateChart from './charts/SpecificStateChart'
import SpecificCasteChart from './charts/SpecificCasteChart'
import SpecificReligionChart from './charts/SpecificReligionChart'
import SpecificIncomeLevelChart from './charts/SpecificIncomeLevelChart'
import SpecificLiteracyLevelChart from './charts/SpecificLiteracyLevelChart'

// Routes
const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      auth: undefined
    }
  },
  {
    path: '/register',
    name: 'register',
    component: Register,
    meta: {
      auth: false
    }
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {
      auth: false
    }
  },
  // USER ROUTES
  {
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard,
    meta: {
      auth: true
    }
  },
  // ADMIN ROUTES
  {
    path: '/roles',
    name: 'roles.index',
    component: RolesIndex,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  // Roles Routes
  {
    path: '/roles/create',
    name: 'roles.create',
    component: RolesCreate,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/roles/:id/edit',
    name: 'roles.edit',
    component: RolesEdit,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/roles/:id/view',
    name: 'roles.view',
    component: RolesView,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },

  // Users Routes
  {
    path: '/users',
    name: 'users.index',
    component: UsersIndex,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/users/create',
    name: 'users.create',
    component: UsersCreate,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/users/:id/edit',
    name: 'users.edit',
    component: UsersEdit,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/users/:id/view',
    name: 'users.view',
    component: UsersView,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/users/import',
    name: 'users.import',
    component: UsersImport,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  // States Route
  {
    path: '/states',
    name: 'states.index',
    component: StatesIndex,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/states/create',
    name: 'states.create',
    component: StatesCreate,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/states/:id/edit',
    name: 'states.edit',
    component: StatesEdit,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/states/:id/view',
    name: 'states.view',
    component: StatesView,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/income-levels',
    name: 'income-levels.index',
    component: IncomeLevelsIndex,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/income-levels/create',
    name: 'income-levels.create',
    component: IncomeLevelsCreate,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/income-levels/:id/edit',
    name: 'income-levels.edit',
    component: IncomeLevelsEdit,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/income-levels/:id/view',
    name: 'income-levels.view',
    component: IncomeLevelsView,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/loan-schemes',
    name: 'loan-schemes.index',
    component: LoanSchemesIndex,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/loan-schemes/create',
    name: 'loan-schemes.create',
    component: LoanSchemesCreate,
    meta:{
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/loan-schemes/:id/edit',
    name: 'loan-schemes.edit',
    component: LoanSchemesEdit,
    meta:{
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/loan-schemes/:id/view',
    name: 'loan-schemes.view',
    component: LoanSchemesView,
    meta:{
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },

  // Religions routes
  {
    path: '/religions',
    name: 'religions.index',
    component: ReligionsIndex,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/religions/create',
    name: 'religions.create',
    component: ReligionsCreate,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/religions/:id/edit',
    name: 'religions.edit',
    component: ReligionsEdit,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/religions/:id/view',
    name: 'religions.view',
    component: ReligionsView,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },

  // Literacy levels

  {
    path: '/literacy-levels',
    name: 'literacy-levels.index',
    component: LiteracyLevelsIndex,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/literacy-levels/create',
    name: 'literacy-levels.create',
    component: LiteracyLevelsCreate,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/literacy-levels/:id/edit',
    name: 'literacy-levels.edit',
    component: LiteracyLevelsEdit,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/literacy-levels/:id/view',
    name: 'literacy-levels.view',
    component: LiteracyLevelsView,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },

  //Castes

  {
    path: '/castes',
    name: 'castes.index',
    component: CastesIndex,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/castes/create',
    name: 'castes.create',
    component: CastesCreate,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/castes/:id/edit',
    name: 'castes.edit',
    component: CastesEdit,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/castes/:id/view',
    name: 'castes.view',
    component: CastesView,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },

  // Districts
  {
    path: '/districts',
    name: 'districts.index',
    component: DistrictsIndex,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/districts/create',
    name: 'districts.create',
    component: DistrictsCreate,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/districts/:id/edit',
    name: 'districts.edit',
    component: DistrictsEdit,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/districts/:id/view',
    name: 'districts.view',
    component: DistrictsView,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },

  //Ngo sectors
  {
    path: '/ngo-sectors',
    name: 'ngo-sectors.index',
    component: NgoSectorsIndex,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/ngo-sectors/create',
    name: 'ngo-sectors.create',
    component: NgoSectorsCreate,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/ngo-sectors/:id/edit',
    name: 'ngo-sectors.edit',
    component: NgoSectorsEdit,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/ngo-sectors/:id/view',
    name: 'ngo-sectors.view',
    component: NgoSectorsView,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/ngos',
    name: 'ngos.index',
    component: NgosIndex,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/ngos/create',
    name: 'ngos.create',
    component: NgosCreate,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/ngos/:id/edit',
    name: 'ngos.edit',
    component: NgosEdit,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/ngos/:id/view',
    name: 'ngos.view',
    component: NgosView,
    meta: {
      auth: {roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/self-help-groups',
    name: 'self-help-groups.index',
    component: SelfHelpGroupsIndex,
    meta: {
      auth: {roles: [1,2,3], redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/self-help-groups/create',
    name: 'self-help-groups.create',
    component: SelfHelpGroupsCreate,
    meta: {
      auth: {roles: [1,2,3], redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/self-help-groups/:id/edit',
    name: 'self-help-groups.edit',
    component: SelfHelpGroupsEdit,
    meta: {
      auth: {roles: [1,2,3], redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/loans',
    name: 'loans.index',
    component: LoansIndex,
    meta: {
      auth: {roles: [1,2,3,4,5], redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/loans/create',
    name: 'loans.create',
    component: LoansCreate,
    meta: {
      auth: {roles: [1,2,3], redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/loans/:id/view',
    name: 'loans.view',
    component: LoansView,
    meta: {
      auth: {roles: [1,2,3,4,5], redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/loans/:loan_id/repayments',
    name: 'repayments.index',
    component: RepaymentsIndex,
    meta: {
      auth: {roles: [1,2,3,4,5], redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/loans/:loan_id/repayments/:id/edit',
    name: 'repayments.edit',
    component: RepaymentsEdit,
    meta: {
      auth: {roles: [1,2,3], redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/loans/:loan_id/repayments/:id/view',
    name: 'repayments.view',
    component: RepaymentsView,
    meta: {
      auth: {roles: [1,2,3,4,5], redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/ngos/state-wise-performance',
    name: 'ngos.state_wise_performance',
    component: NgoStateWisePerformance,
    meta: {
      auth: {roles: [1,2], redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/ngos/religion-state-wise-performance',
    name: 'ngos.religion_state_wise_performance',
    component: ReligionStateWisePerformance,
    meta: {
      auth: {roles: [1,2], redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/ngos/caste-performance-state-wise',
    name: 'ngos.caste_performance_state_wise',
    component: CasteStateWisePerformance,
    meta: {
      auth: {roles: [1,2], redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/ngos/income-level-performance-state-wise',
    name: 'ngos.income_level_performance_state_wise',
    component: IncomeLevelStateWisePerformance,
    meta: {
      auth: {roles: [1,2], redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/ngos/literacy-level-performance-state-wise',
    name: 'ngos.literacy_level_performance_state_wise',
    component: LiteracyLevelStateWisePerformance,
    meta: {
      auth: {roles: [1,2], redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/charts/all-inputs',
    name: 'charts.all-inputs',
    component: NoSpecificInputChart,
    meta: {
      auth: {roles: [1,2], redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/charts/specific-state-all-inputs',
    name: 'charts.specific-state-all-inputs',
    component: SpecificStateChart,
    meta: {
      auth: {roles: [1,2], redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/charts/specific-caste-all-inputs',
    name: 'charts.specific-caste-all-inputs',
    component: SpecificCasteChart,
    meta: {
      auth: {roles: [1,2], redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/charts/specific-religion-all-inputs',
    name: 'charts.specific-religion-all-inputs',
    component: SpecificReligionChart,
    meta: {
      auth: {roles: [1,2], redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/charts/specific-income-level-all-inputs',
    name: 'charts.specific-income-level-all-inputs',
    component: SpecificIncomeLevelChart,
    meta: {
      auth: {roles: [1,2], redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },
  {
    path: '/charts/specific-literacy-level-all-inputs',
    name: 'charts.specific-literacy-level-all-inputs',
    component: SpecificLiteracyLevelChart,
    meta: {
      auth: {roles: [1,2], redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    }
  },

]


const router = new VueRouter({
  history: true,
  mode: 'history',
  routes,
})

export default router
